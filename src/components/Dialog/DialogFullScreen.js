import React, { useState , useEffect } from "react";
import {
  Grid, 
  Dialog,
  DialogActions,
  DialogContent,
  Typography,
  DialogTitle,
  Button,
} from "@material-ui/core";

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Slide from '@material-ui/core/Slide';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(theme => ({
    appBar: {
      position: 'relative',
      backgroundColor:'red',
      color:'white'
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
      color:'white'
    },
  }));
  
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function DialogFullScreen(props) {
    const classes = useStyles();
  return (
    <>
       <Dialog fullScreen open={props.open} onClose={props.close} >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={props.cancel} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {props.title}
            </Typography>
          </Toolbar>
        </AppBar>
        <DialogContent>
            {props.content}
        </DialogContent>
        <DialogActions>
          <Button onClick={props.cancel}>
            <Typography style={{textTransform:'none'}}>{props.valueCancel === ""? "Batal" : props.valueCancel}</Typography>
          </Button>
          {
            props.confirm ?
            <Button onClick={props.confirm} variant="contained" style={{backgroundColor:`${props.colorButtonConfirm}`}}>
              <Typography style={{textTransform:'none',color:'#ffffff'}}>{props.valueConfirm}</Typography>
            </Button> : null
          }
          
        </DialogActions>
      </Dialog>
    </>
  );
}

import React, { useState, useEffect } from "react";
import { Drawer, IconButton, List } from "@material-ui/core";
import {
  Home as HomeIcon,
  NotificationsNone as NotificationsIcon,
  FormatSize as TypographyIcon,
  FilterNone as UIElementsIcon,
  BorderAll as TableIcon,
  LocationCity,
  Sports,
  AccountBox as AccountBoxIcon,
  QuestionAnswer as SupportIcon,
  LibraryBooks as LibraryIcon,
  HelpOutline as FAQIcon,
  ArrowBack as ArrowBackIcon,
} from "@material-ui/icons";
import LiveHelpIcon from "@material-ui/icons/LiveHelp";
import { useTheme } from "@material-ui/styles";
import { withRouter } from "react-router-dom";
import classNames from "classnames";

// styles
import useStyles from "./styles";

// components
import SidebarLink from "./components/SidebarLink/SidebarLink";
import Dot from "./components/Dot";

// context
import {
  useLayoutState,
  useLayoutDispatch,
  toggleSidebar,
} from "../../context/LayoutContext";

function Sidebar({ location }) {
  const structure = [];
  if (localStorage.getItem("user_access") === "admin") {
    structure.push(
      {
        id: 0,
        label: "Dashboard",
        link: "/app/dashboard",
        icon: <HomeIcon />,
        toolTip: "Dashboard",
      },
      {
        id: 1,
        label: "Kontingen",
        link: "/app/contingents",
        icon: <TableIcon />,
      },
      { id: 2, label: "Disiplin", link: "/app/disciplines", icon: <Sports /> },
      { id: 3, label: "Venue", link: "/app/venues", icon: <LocationCity /> },
      {
        id: 4,
        label: "Participant",
        link: "/app/participants",
        icon: <LocationCity />,
      },
      {
        id: 5,
        label: "User",
        link: "/app/users",
        icon: <AccountBoxIcon />,
        children: [
          { label: "User", link: "/app/users" },
          { label: "User Keabsahan", link: "/app/accreditation" },
        ],
      },
      {
        id: 6,
        label: "Report",
        link: "/app/entries",
        icon: <AccountBoxIcon />,
        children: [
          { label: "Pra PON", link: "/app/entries" },
          { label: "Tahap 1", link: "/app/report/tahap1" },
          { label: "Tahap 2", link: "/app/report/tahap2" },
        ],
      },
      {
        id: 7,
        label: "Helpdesk",
        link: "/app/helpdesk/tahap1",
        icon: <LiveHelpIcon />,
        children: [
          { label: "Tahap 1", link: "/app/helpdesk/tahap1" },
          { label: "Tahap 2", link: "/app/helpdesk/tahap2" },
        ],
      },
    );
  } else if (localStorage.getItem("user_access") === "Sekretariat PB PON") {
    structure.push(
      {
        id: 0,
        label: "Dashboard",
        link: "/app/dashboard",
        icon: <HomeIcon />,
        toolTip: "Dashboard",
      },
      { id: 3, label: "Venue", link: "/app/venues", icon: <LocationCity /> },
      {
        id: 4,
        label: "User",
        link: "/app/users",
        icon: <AccountBoxIcon />,
        children: [
          { label: "User", link: "/app/users" },
          { label: "User Keabsahan", link: "/app/accreditation" },
        ],
      },
    );
  } else if (localStorage.getItem("user_access") === "Dukcapil") {
    structure.push(
      {
        id: 0,
        label: "Dashboard",
        link: "/app/dashboard",
        icon: <HomeIcon />,
        toolTip: "Dashboard",
      },
      {
        id: 1,
        label: "Participant",
        link: "/app/participants",
        icon: <LocationCity />,
      },
      {
        id: 2,
        label: "Report",
        link: "/app/entries",
        icon: <AccountBoxIcon />,
        children: [
          { label: "Pra PON", link: "/app/entries" },
          { label: "Tahap 1", link: "/app/report/tahap1" },
          { label: "Tahap 2", link: "/app/report/tahap2" },
        ],
      },
    );
  } else if (localStorage.getItem("user_access") === "adminkominfo") {
    structure.push(
      {
        id: 0,
        label: "Dashboard",
        link: "/app/dashboard",
        icon: <HomeIcon />,
        toolTip: "Dashboard",
      },
      {
        id: 1,
        label: "Tahap 1",
        link: "/app/report/tahap1",
        icon: <AccountBoxIcon />,
        toolTip: "Tahap 1",
      },
    );
  } else if (localStorage.getItem("user_access") === "Bidang Pertandingan") {
    structure.push(
      {
        id: 0,
        label: "Dashboard",
        link: "/app/dashboard",
        icon: <HomeIcon />,
        toolTip: "Dashboard",
      },
      {
        id: 1,
        label: "Tahap 1",
        link: "/app/report/tahap1",
        icon: <AccountBoxIcon />,
        toolTip: "Tahap 1",
      },
    );
  }
  var classes = useStyles();
  var theme = useTheme();

  // global
  var { isSidebarOpened } = useLayoutState();
  var layoutDispatch = useLayoutDispatch();

  // local
  var [isPermanent, setPermanent] = useState(true);

  useEffect(function () {
    window.addEventListener("resize", handleWindowWidthChange);
    handleWindowWidthChange();
    return function cleanup() {
      window.removeEventListener("resize", handleWindowWidthChange);
    };
  });

  return (
    <div class="no-print">
      <Drawer
        variant={isPermanent ? "permanent" : "temporary"}
        className={classNames(classes.drawer, {
          [classes.drawerOpen]: isSidebarOpened,
          [classes.drawerClose]: !isSidebarOpened,
        })}
        classes={{
          paper: classNames({
            [classes.drawerOpen]: isSidebarOpened,
            [classes.drawerClose]: !isSidebarOpened,
          }),
        }}
        open={isSidebarOpened}
      >
        <div className={classes.toolbar} />
        <div className={classes.mobileBackButton}>
          <IconButton onClick={() => toggleSidebar(layoutDispatch)}>
            <ArrowBackIcon
              classes={{
                root: classNames(
                  classes.headerIcon,
                  classes.headerIconCollapse,
                ),
              }}
            />
          </IconButton>
        </div>
        <List className={classes.sidebarList}>
          {structure.map((link) => (
            <SidebarLink
              key={link.id}
              location={location}
              isSidebarOpened={isSidebarOpened}
              {...link}
            />
          ))}
        </List>
      </Drawer>
    </div>
  );

  // ##################################################################
  function handleWindowWidthChange() {
    var windowWidth = window.innerWidth;
    var breakpointWidth = theme.breakpoints.values.md;
    var isSmallScreen = windowWidth < breakpointWidth;

    if (isSmallScreen && isPermanent) {
      setPermanent(false);
    } else if (!isSmallScreen && !isPermanent) {
      setPermanent(true);
    }
  }
}

export default withRouter(Sidebar);

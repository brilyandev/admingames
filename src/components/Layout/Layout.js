import React from "react";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import classnames from "classnames";

// styles
import useStyles from "./styles";

// components
import Header from "../Header";
import Sidebar from "../Sidebar";

// pages
import Dashboard from "../../pages/dashboard";
import Typography from "../../pages/typography";
import Notifications from "../../pages/notifications";
import Maps from "../../pages/maps";
import Tables from "../../pages/tables";
import Icons from "../../pages/icons";
import Charts from "../../pages/charts";
import Users from "../../pages/users/Users";
import Contingents from "../../pages/contingents/Contingents";
import Disciplines from "../../pages/disciplines/Disciplines";
import ViewDisciplines from "../../pages/disciplines/ViewDisciplines";
import Venues from "../../pages/venues/Venues";
// context
import { useLayoutState } from "../../context/LayoutContext";
import Accreditation from "../../pages/users/Accreditation";
import Prapon from "../../pages/disciplines/PraPon";
import Entries from "../../pages/disciplines/Entries";
import Participants from "../../pages/participants/participants";
import Tahap1 from "../../pages/report/tahap1";
import Tahap2 from "../../pages/report/tahap2";

import Helpdesk from "../../pages/helpdesk/Helpdesk";
import Helpdesk2 from "../../pages/helpdesk/HelpdeskTahap2";

function Layout(props) {
  var classes = useStyles();

  // global
  var layoutState = useLayoutState();

  return (
    <div className={classes.root}>
      <>
        <Header history={props.history} />
        <Sidebar />
        <div
          className={classnames(classes.content, {
            [classes.contentShift]: layoutState.isSidebarOpened,
          })}
        >
          <div className={classes.fakeToolbar} />
          <Switch>
            <Route path="/app/prapon/:id/:idKelas" component={Prapon} />
            <Route path="/app/dashboard" component={Dashboard} />
            <Route path="/app/entries" component={Entries} />
            <Route path="/app/typography" component={Typography} />
            <Route path="/app/tables" component={Tables} />
            <Route path="/app/notifications" component={Notifications} />
            <Route path="/app/users" component={Users} />
            <Route path="/app/contingents" component={Contingents} />
            <Route path="/app/disciplines" component={Disciplines} />
            <Route
              path="/app/viewdisciplines/:id"
              component={ViewDisciplines}
            />
            <Route path="/app/venues" component={Venues} />
            <Route path="/app/accreditation" component={Accreditation} />
            <Route path="/app/participants" component={Participants} />
            <Route path="/app/helpdesk/tahap1" component={Helpdesk} />
            <Route path="/app/helpdesk/tahap2" component={Helpdesk2} />
            <Route path="/app/report/tahap1" component={Tahap1} />
            <Route path="/app/report/tahap2" component={Tahap2} />
            <Route
              exact
              path="/app/ui"
              render={() => <Redirect to="/app/ui/icons" />}
            />
            <Route path="/app/ui/maps" component={Maps} />
            <Route path="/app/ui/icons" component={Icons} />
            <Route path="/app/ui/charts" component={Charts} />
          </Switch>
        </div>
      </>
    </div>
  );
}

export default withRouter(Layout);

import axios from 'axios';
import StaticVar from '../Config/StaticVar';

// ===> api create 
const api = axios.create({
  baseURL: StaticVar.Base_Url,
  // timeout: 10000,
  headers:{}
});

// ===> api interceptors 
api.interceptors.request.use(function (config) {
    // set headers after authentication
    config.headers['x-access-token'] = localStorage.getItem("token");
    return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});

// ===> api list function request
const getEvents = () => api.get('/private/events');
const getAllEvents = () => api.get('/private/events/get_all_event');
const getEventData = (id) => api.get('/private/events/get_event_data/'+id);
const getEventCategoryData = (id) => api.get('/private/events/get_categories_data/'+id);

const getQuota = (event_id) => api.get('/private/event_quotas/event/'+event_id);
const createQuota = (data) => api.post('/private/event_quotas/create', data);
const addQuota = (_id, data) => api.put('/private/event_quotas/addQuota/'+_id, data);
const deleteQuota = (_id, contingent_id) => api.put('/private/event_quotas/deleteQuota/'+_id+'/'+contingent_id);
const updateQuota = (_id, contingent_id, data) => api.put('/private/event_quotas/setQuota/'+_id+'/'+contingent_id, data);

const getEventQualification = (id) => api.get('/private/event_qualifications/'+id);
const getEventParticipantQuota = (id) => api.get('/private/event_qualifications/participant/get_participantByQuota/'+id);
const getEventParticipantCategory = (id, categoryid) => api.get('/private/event_qualifications/participant/get_participant/'+id+'/'+categoryid);

const getEventById = (id) => api.get('/private/events/'+id);
const postEvents = (data) => api.post('/private/events/create', data);
const postUploadEvents = (data) => api.post('/private/events/upload', data);
const postEventQualification = (data) => api.post('/private/event_qualifications/create', data);
const putEvents = (id, data) => api.put('/private/events/'+id, data);
const putEventAddParticipant = (id, data) => api.put('/private/event_qualifications/participant/add/'+id, data);
const putEventUpdateParticipant = (categoryId, id, data) => api.put('/private/event_qualifications/participant/update/'+categoryId+'/'+id, data);
const putEventAddQuota = (id, data) => api.put('/private/event_qualifications/participant/addByQuota/'+id, data);
const putEventUpdateQuota = (idQuota, id, data) => api.put('/private/event_qualifications/participant/updateByQuota/'+idQuota+'/'+id, data);

const putNonEvents = (id, data) => api.put('/private/events/non/'+id, data);
const updatecategory = (id, data) => api.put('/private/events/updatecategory/'+id, data);
const deleteEvents = (id) => api.delete('/private/events/'+id);
const deleteEventQuota = (idQuota, id) => api.delete('/private/event_qualifications/participant/deleteByQuota/'+idQuota+'/'+id);
const deleteEventParticipantQuota = (categoryId, id) => api.delete('/private/event_qualifications/participant/delete/'+categoryId+'/'+id);

const getContingents = () => api.get('/private/contingents');
const getContingentById = (id) => api.get('/private/contingents/'+id);
const postContingents = (data) => api.post('/private/contingents/create', data);
const putContingents = (id, data) => api.put('/private/contingents/'+id, data);
const deleteContingents = (id) => api.delete('/private/contingents/'+id);

const getUsers = () => api.get('/private/users');
const getVenues = () => api.get('/private/venues');
const getEntrieSchedule = () => api.get('/entrie_schedules/PON XX 2020 Papua');
const putEntrieSchedule = (id, data) => api.put('/entrie_schedules/'+ id, data);
const postEntrieQualification = () => api.post('/private/event_qualifications/entrie_qualification');

const getCheckParticipant = (data) => api.post('/private/participants/dukcapil', data);
const getSyncDukcapil = (data) => api.post('/private/participants/syncdukcapil', data);
const getParticipantTahap2 = (participant_type, contingent_id) => {
  if(contingent_id !== "0"){
    if(participant_type === "VIP" || participant_type === "CdM" || participant_type === "KONI"){
      return(api.get('/private/participants?contingent_id='+contingent_id+'&participant_type='+participant_type));
    }
    else{
      return(api.get('/private/participants?contingent_id='+contingent_id+'&entrie_by_name_status=true&participant_type='+participant_type));
    }
  }  
  else {return(api.get('/private/participants?entrie_by_name_status=true&participant_type='+participant_type));}
  
}

const getParticipant = (contingen_id,event_id) => api.get('/private/participants/'+contingen_id+'/'+event_id+'/Athlete');
const editParticipantCategoryRequire = (participant_id,category_require_id) => api.put('/private/participants/'+participant_id+'/'+category_require_id+'/Athlete');
const addParticipantCategoryRequire = (participant_id,data) => api.put('/private/participants/add_participant_category_require/'+participant_id,data);
const deleteParticipantCategoryRequire = (participant_id,category_require_id) => api.put('/private/participants/delete_participant_category_require/'+participant_id+'/'+category_require_id);


export const apis = {
  getEvents,
  getAllEvents,
  getEventData,
  getEventCategoryData,
  getEventQualification,
  getEventParticipantQuota,
  getEventParticipantCategory,

  getEventById,
  postEvents,
  postUploadEvents,
  postEventQualification,
  putEvents,
  putEventAddParticipant,
  putEventUpdateParticipant,
  putEventAddQuota,
  putEventUpdateQuota,

  putNonEvents,
  updatecategory,
  deleteEvents,
  deleteEventQuota,
  deleteEventParticipantQuota,

  getContingents,
  getContingentById,
  postContingents,
  putContingents,
  deleteContingents,

  getUsers,
  getVenues,
  getEntrieSchedule,
  putEntrieSchedule,

  postEntrieQualification,

  getCheckParticipant,
  getSyncDukcapil,
  getParticipantTahap2,

  getQuota,
  createQuota,
  addQuota,
  deleteQuota,
  updateQuota,

  getParticipant,

  editParticipantCategoryRequire,
  addParticipantCategoryRequire,
  deleteParticipantCategoryRequire 
}

export default apis
import React, { useState,useEffect } from "react";
import {
  Grid, 
  Paper,
  Avatar, 
  Typography,
  Button,
  TextField,
  InputLabel, 
  Select,
  MenuItem, 
  CircularProgress
} from "@material-ui/core";
import {useLocation} from "react-router-dom";
 
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';  

// styles
//import useStyles from "./styles";
import MUIDataTable from "mui-datatables";

import PageTitle from "../Components/PageTitle/PageTitle";
import axios from "axios";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import _ from 'lodash';
import DialogFullScreen from "../../components/Dialog/DialogFullScreen";
import moment from 'moment';
import ObjectID  from 'bson-objectid'
import logo from "../../Assets/Images/logo-2021.png";
import maskot1 from "../../Assets/Images/kangpo.png";
import maskot2 from "../../Assets/Images/maskot_drawa_1.png";
import Printer, { print } from 'react-pdf-print'
import {PDFViewer, Page, Text, View, Document, StyleSheet  } from '@react-pdf/renderer';
import Tahap2BySport from "./tahap2bysport";
import "./css.css"
import Tahap1ByCluster from "./tahap1bycluster";
import Tahap2SummaryDiscipline from "./tahap2summarydiscipline";
import Tahap1SummaryCategory from "./tahap1summarycategory"
import Tahap1CheckDukcapil from "./tahap1checkdukcapil"
import StaticVar from "../../Config/StaticVar"
import Api from "../../Services/Api"
import Tahap1DistributionLonglist from "./tahap1DistributionLonglist";
import Tahap2SummaryContingent from "./tahap2summarycontingent";
import Tahap2SummaryCategory from "./tahap2summarycategory";

export default function Tahap2WebView(props) {
  // global
  var userDispatch = useUserDispatch();  
  //var classes = useStyles();
  //"http://localhost:300"
  const url = StaticVar.Base_Url;//"http://localhost:300";
  const search = useLocation().search;
  //const token = localStorage.getItem("token");
  const token = new URLSearchParams(search).get('token');
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token 
  };

  const [event, setevent]= useState([])
  const [dataevent, setdataevent]= useState([])
  const [disciplines, setdisciplines]= useState([])
  const [contingents, setcontingents]= useState([])
  const [choosecontingent, setchoosecontingent]= useState({_id:"0", name:"", logo:""})
  const [choosedisciplines, setchoosedisciplines]= useState({_id:"0", event_name:"", event_icon:""})
  const [participant, setparticipant] = useState([])
  const [summarycategory, setsummarycategory] = useState([])
  const [loading, setloading] = useState(false)
  const [cluster, setcluster]= useState("0")

  const [datacontingent, setdatacontingent]= useState([])
  const [dataparticipant, setdataparticipant]= useState([])

  const [pagenumber, setpagenumber] = useState(0)
  const [printPDF, setprintPDF] = useState(false)
  const [documentPDF, setdocumentPDF] = useState("")

  const [datasummarycontingent, setdatasummarycontingent]= useState([])

  const [datasummarycontingent_byname, setdatasummarycontingent_byname]= useState([])

  const [datasummarydiscipline, setdatasummarydiscipline]= useState([])
  const [dataentriequalification, setdataentriequalification]= useState([])
  const [filterentries, setfilterentries] = useState("0")
  const [entrie_individual_athletes, setentrie_individual_athletes] = useState([])
  const [entrie_group_athletes, setentrie_group_athletes] = useState([])
  const [entries, setentries] = useState([])
  const [entries_by_sport, setentries_by_sport] = useState([])
  const [entrie_individual_athletes_by_sport, setentrie_individual_athletes_by_sport] = useState([])
  const [entrie_group_athletes_by_sport, setentrie_group_athletes_by_sport] = useState([])
  const [loadingSummary, setLoadingSummary] = useState(false);

  const [entrie_individual, setentrie_individual] = useState([])
  const [entrie_group, setentrie_group] = useState([])
  const [limit_participant, setlimit_participant] = useState(0)


  const ids = ['1']
  function loadData(){
      let contingent_id = choosecontingent._id
      let event_id = choosedisciplines._id
        setloading(true)
        setLoadingSummary(true)    
        if(event_id === "0"){
            axios.get(url+"/private/contingents/"+contingent_id,{headers}).then(
                resdata=>{
                    setchoosecontingent(resdata.data[0])
                    console.log('profile', new Date())
                }
            );  
            axios.get(url+"/private/entrie_by_numbers/contingent_by_name/"+contingent_id,{headers}).then(
                resSummaryEntries=>{
                    setentries(resSummaryEntries.data)
                    console.log('summary', new Date())
                    setLoadingSummary(false)
                }
            );   
            axios.get(url+"/private/entrie_by_numbers/contingent2/"+contingent_id,{headers}).then(
                resDaftarCabor=>{
                    setevent(resDaftarCabor.data)
                    setloading(false)
                    console.log('cabor', new Date())
                }
            ); 
            axios.get(url+"/private/entrie_athletes/participant/contingent/"+contingent_id,{headers}).then(
                resIndividualEntries=>{
                    setentrie_individual_athletes(resIndividualEntries.data)
                    console.log('individual', new Date())
                }
            ); 
            axios.get(url+"/private/entrie_groups/participant/contingentonly/"+contingent_id,{headers}).then(
                resGroupEntries=>{
                    setentrie_group_athletes(resGroupEntries.data)
                    console.log('group', new Date())
                }
            );
        }    
        else{
            axios.get(url+"/private/contingents/"+contingent_id+"?event_id="+event_id,{headers}).then(
                resdata=>{
                    setchoosecontingent(resdata.data[0])
                    console.log('profile', new Date())
                }
            );  
            axios.get(url+"/private/entrie_by_numbers/contingent_by_name/"+contingent_id+"?event_id="+event_id,{headers}).then(
                resSummaryEntries=>{
                    setentries(resSummaryEntries.data)
                    console.log('summary', new Date())
                    setLoadingSummary(false)
                }
            );   
            axios.get(url+"/private/entrie_by_numbers/contingent2/"+contingent_id+"?event_id="+event_id,{headers}).then(
                resDaftarCabor=>{
                    setevent(resDaftarCabor.data)
                    setloading(false)
                    console.log('cabor', new Date())
                }
            ); 
            axios.get(url+"/private/entrie_athletes/participant/contingent/"+contingent_id+"?event_id="+event_id,{headers}).then(
                resIndividualEntries=>{
                    setentrie_individual_athletes(resIndividualEntries.data)
                    console.log('individual', new Date())
                }
            ); 
            axios.get(url+"/private/entrie_groups/participant/contingentonly/"+contingent_id+"?event_id="+event_id,{headers}).then(
                resGroupEntries=>{
                    setentrie_group_athletes(resGroupEntries.data)
                    console.log('group', new Date())
                }
            );
        }
  }

  function loadDataCluster(data){
    axios.get(url+"/private/events?cluster="+data,{headers}).then((response)=>{
        console.log('dataevent', response.data)
        var eventlist = response.data.map(x=> x._id)
        setdataevent(response.data)
        axios.get(url+"/private/participants?entrie_by_name_status=true&event_list="+JSON.stringify(eventlist),{headers}).then((responsedataparticipant)=>{
            console.log('responsedataparticipant 2',responsedataparticipant.data)
            setdataparticipant(responsedataparticipant.data)
        })
        
    })
  }

  function loadDataEvent(_id){
    setloading(true)
    const req1 = axios.get(url+"/private/entrie_by_numbers/event/"+_id,{headers}); 
    const req2 = axios.get(url+"/private/participants?entrie_by_name_status=true&event_id="+_id,{headers}); 
    //const req2 = axios.post(url+"/private/participants/participant_byname", {event_id:_id}, {headers})
    const req3 = axios.get(url+"/private/entrie_athletes/participant/event/"+_id,{headers}); 
    const req4 = axios.get(url+"/private/entrie_groups/participant/eventonly/"+_id,{headers}); 
    const req5 = axios.get(url+"/private/entrie_by_numbers/event_by_name/"+_id,{headers}); 
    const req6 = axios.get(url+"/private/entrie_by_numbers/event_by_name_contingent_list/"+_id,{headers});
    axios.all([req1, req2, req3, req4, req5, req6]).then(axios.spread((...responses) => {
        const response1 = responses[0]  
        const response2 = responses[1] 
        const response3 = responses[2] 
        const response4 = responses[3]  
        const response5 = responses[4] 
        const response6 = responses[5]  
        let _datacontingent = []
            response1.data.forEach(x=>{
                let count = response6.data.filter(y=>y.contingent_id === x.contingent_id)
                if(count.length>0){
                    _datacontingent.push(x)
                }
            })
        setdatacontingent(_.orderBy(_datacontingent, ["contingent"], ["asc"]))
        setdataparticipant(response2.data)
        console.log('entries_by_sport', response5.data)
        setentrie_individual_athletes_by_sport(response3.data)
        setentrie_group_athletes_by_sport(response4.data)
        setentries_by_sport(response5.data)
        setloading(false)
    })).catch(errors => {
        console.log('error',errors)
    })

  }

  function loadDataCategory(_id){
    axios.get(url+"/private/events/"+_id, {headers}).then(async res=>{
        var datacategory = res.data;
        console.log('/private/events/category_by_name/',  res.data)
        console.log('datacategory', datacategory[0].event_categories)
        setsummarycategory(
            datacategory[0].event_categories.map(
                x=>{
                return(
                    {
                    ...x, event_id:datacategory[0]._id,  event_name:datacategory[0].event_name 
                    })
            })
        )

        axios.get(url+"/private/events/individual_category/"+_id, {headers}).then(async res=>{
            console.log('/private/events/individual_category/',  res.data)
            setentrie_individual(res.data)
        })
    
        axios.get(url+"/private/events/group_category/"+_id, {headers}).then(async res=>{
            console.log('/private/events/group_category/',  res.data)
            setentrie_group(res.data)
        })
    })

    
  }

  function loadDataCategoryLimit(limit){
      
    axios.get(url+"/private/events", {headers}).then(async res=>{
        var summary = []
        res.data.forEach(itemevent=>{
            var datacategory = itemevent.event_categories.map(x=>{
                return(
                    {
                    ...x, event_id:itemevent._id,  event_name:itemevent.event_name 
                    })
            })
            console.log('datacategory', datacategory)
            summary = [...summary, ...datacategory]
        })
        
        console.log('summary', res.data)
        setsummarycategory(summary)

        axios.get(url+"/private/events/individual_category/0", {headers}).then(async res=>{
            console.log('/private/events/individual_category/',  res.data)
            setentrie_individual(res.data)
        })
    
        axios.get(url+"/private/events/group_category/0", {headers}).then(async res=>{
            console.log('/private/events/group_category/',  res.data)
            setentrie_group(res.data)
        })
    })
    
  }

  const [datafootercontingent, setdatafootercontingent] = useState([])

  const [datafooterdiscipline, setdatafooterdiscipline] = useState([])
  const [loadingdata, setloadingdata] = useState(false)

  function loadDataSummaryContingent(_id){
    const req1 = axios.get(url+"/private/contingents/by_name_recapitulation",{headers}); 
    axios.all([req1]).then(axios.spread((...responses) => {
        const response1 = responses[0]  
        console.log('setdatasummarycontingent_byname', response1.data)
        setdatasummarycontingent_byname(_.orderBy(response1.data, ["name"], ["asc"]))
        let datasummary = {
            event_tahap_1:0,
        	category_tahap_1:0,
            event_tahap_2:0,
            category_tahap_2:0,
            longlist_athlete:0,
            longlist_official:0,
            longlist_officialkontingen:0,
            athlete:0,
            official:0,
            officialkontingen:0
        }
        response1.data.forEach(x=>{
            datasummary.event_tahap_1 = datasummary.event_tahap_1 + x.event_tahap_1
        	datasummary.category_tahap_1 =datasummary.category_tahap_1 + x.category_tahap_1
            datasummary.event_tahap_2 = datasummary.event_tahap_2 + x.event_tahap_2
            datasummary.category_tahap_2 = datasummary.category_tahap_2 + x.category_tahap_2
            datasummary.longlist_athlete = datasummary.longlist_athlete + x.longlist_athlete
            datasummary.longlist_official = datasummary.longlist_official + x.longlist_official
            datasummary.longlist_officialkontingen = datasummary.longlist_officialkontingen + x.longlist_officialkontingen
            datasummary.athlete = datasummary.athlete + x.athlete
            datasummary.official = datasummary.official + x.official
            datasummary.officialkontingen = datasummary.officialkontingen + x.officialkontingen
        })
        console.log('datasummary', datasummary)
       setdatafootercontingent([datasummary]);
    })).catch(errors => {
        console.log('error',errors)
    })

  }

  function loadDataSummaryDiscipline(){
      var urlget = ""
      if(choosecontingent._id === "0"){
        urlget = url+'/private/events/by_name_recapitulation'
      }
      else{
        urlget = url+'/private/events/by_name_recapitulation?contingent_id='+choosecontingent._id
      }
      setloadingdata(true)
      axios.get(urlget, {headers}).then(res=>{
        console.log('res.data', res.data)
        setdatasummarydiscipline(_.orderBy(res.data, ["event_name"], ["asc"]))
        let datasummary = [
            {
                contingent_tahap_1:_.sumBy(res.data,"contingent_tahap_1"),
                category_tahap_1:_.sumBy(res.data,"category_tahap_1"),
                contingent_tahap_2:_.sumBy(res.data,"contingent_tahap_2"),
                category_tahap_2:_.sumBy(res.data,"category_tahap_2"),
                longlist_athlete:_.sumBy(res.data,"longlist_athlete"),
                longlist_official:_.sumBy(res.data,"longlist_official"),
                athlete:_.sumBy(res.data,"athlete"),
                official:_.sumBy(res.data,"official")
            }
        ];
        setdatafooterdiscipline(datasummary);
        setloadingdata(false)
      })

  }

  useEffect(()=>{
    const req1 = axios.get(url+"/private/contingents",{headers});  
    const req2 = axios.get(url+"/private/events/get_all_event",{headers});    
    axios.all([req1, req2]).then(axios.spread((...responses) => {
        const response1 = responses[0]
        const response2 = responses[1]
        setcontingents(response1.data)
        setdisciplines(response2.data)
    })).catch(errors => {
        console.log('error',errors)
    })

    if(localStorage.user_access === "Bidang Pertandingan"){
        setpagenumber(2); loadDataSummaryContingent()
    }
  },[])

  const [datalonglistdistribution, setdatalonglistdistribution] = useState([])

  function loadLonglistDistribution(){
    axios.post(url+"/private/participants/longlistdistribution",{},{headers}).then(res=>{
        console.log('loadLonglistDistribution', res.data)
        setdatalonglistdistribution(res.data)
    })
  }


  return (
    <>
      <div  class="no-print">
          {
              localStorage.user_access === "Bidang Pertandingan" ? 
              <>
                <Button variant="contained" color={pagenumber==2?"primary":"default"} style={{marginRight:5}} onClick={()=>{setpagenumber(2); loadDataSummaryContingent()}}>Rekap Kontingen</Button>
                <Button variant="contained" color={pagenumber==3?"primary":"default"} onClick={()=>{setpagenumber(3); loadDataSummaryDiscipline()}}>Rekap Cabor/Disiplin</Button>
              </> : 
              <>
                <Button variant="contained" color={pagenumber==0?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>setpagenumber(0)}>By Kontingen</Button>
                <Button variant="contained" color={pagenumber==1?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>setpagenumber(1)}>By Cabor/Disiplin</Button>
                <Button variant="contained" color={pagenumber==2?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>{setpagenumber(2); loadDataSummaryContingent()}}>Rekap Kontingen</Button>
                <Button variant="contained" color={pagenumber==3?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>{setpagenumber(3)}}>Rekap Cabor/Disiplin</Button>
                <Button variant="contained" color={pagenumber==5?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>{setpagenumber(5)}}>Rekap No.Pertandingan</Button>
                <Button variant="contained" color={pagenumber==6?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>{setpagenumber(6)}}>No.Pertandingan</Button>
                {/* <Button variant="contained" color={pagenumber==7?"primary":"default"} style={{fontSize:11}} onClick={()=>{setpagenumber(7); loadDataSummaryDiscipline()}}>Cek Dukcapil</Button>
                <Button variant="contained" color={pagenumber==8?"primary":"default"} style={{fontSize:11}} onClick={()=>{setpagenumber(8); loadLonglistDistribution()}}>Distribusi Longlist</Button> */}
              </>
          }
          
          {
              pagenumber==0 ? 
              <p>Kontingen : 
                <Select value={choosecontingent._id} 
                    onChange={(event)=>{
                        if(event.target.value != "0"){
                            var data = contingents.filter(x=>x._id=== event.target.value);
                            setchoosecontingent(data[0]);
                            
                        }
                        else{
                            setchoosecontingent({_id:"", name:"", logo:""});
                        }
                    }}
                >
                    <MenuItem value={"0"}>Pilih Kontingen</MenuItem>
                    {
                        contingents.map((item,index)=>{
                            return(
                                <MenuItem value={item._id}>{item.name}</MenuItem>
                            )
                        })
                    }
                </Select>
                Disipin : 
                <Select value={choosedisciplines._id} 
                    onChange={(event)=>{
                        if(event.target.value != "0"){
                            var data = disciplines.filter(x=>x._id=== event.target.value);
                            setchoosedisciplines(data[0]);
                        }
                        else{
                            setchoosedisciplines({_id:"0", event_name:"", event_icon:"", event_qualifications:""});
                        }
                    }}
                >
                    <MenuItem value={"0"}>Semua Disiplin</MenuItem>
                    {
                        disciplines.map((item,index)=>{
                            return(
                                <MenuItem value={item._id}>{item.event_name}</MenuItem>
                            )
                        })
                    }
                </Select>
                {
                    choosecontingent._id ? 
                    choosecontingent._id !== "0" ? 
                    <Button style={{marginLeft:5}} variant="contained" color="secondary" onClick={()=>loadData()}>Load Data</Button> : 
                    null : null
                }
                
              </p>
              : 
              pagenumber==1 || pagenumber ==5 ? 
              <p>Disiplin : 
                <Select value={choosedisciplines._id} 
                    onChange={(event)=>{
                        if(event.target.value != "0"){
                            var data = disciplines.filter(x=>x._id=== event.target.value);
                            setchoosedisciplines(data[0]);
                            if(pagenumber== 1){
                                loadDataEvent(event.target.value);
                            }
                            else{
                                // loadDataEvent(event.target.value);
                                loadDataCategory(event.target.value)
                            }
                        }
                        else{
                            setchoosedisciplines({_id:"", event_name:"", event_icon:"", event_qualifications:""});
                        }
                    }}
                >
                    <MenuItem value={"0"}>Pilih Disiplin</MenuItem>
                    {
                        disciplines.map((item,index)=>{
                            return(
                                <MenuItem value={item._id}>{item.event_name}</MenuItem>
                            )
                        })
                    }
                </Select>
                </p>
              :
              pagenumber == 4 ? 
              <p>Kluster : 
                <Select value={cluster} onChange={(event)=>{setcluster(event.target.value); loadDataCluster(event.target.value)}}>
                    <MenuItem value={"0"}>Pilih Kluster</MenuItem>
                    <MenuItem value={"Kota Jayapura"}>Kota Jayapura</MenuItem>
                    <MenuItem value={"Kabupaten Jayapura"}>Kabupaten Jayapura</MenuItem>
                    <MenuItem value={"Kabupaten Merauke"}>Kabupaten Merauke</MenuItem>
                    <MenuItem value={"Kabupaten Mimika"}>Kabupaten Mimika</MenuItem>
                </Select>
              </p> :
              pagenumber == 6 ?
              <p>Jumlah Peserta : 
              <Select value={filterentries} onChange={(event)=>{console.log('filterentries', event.target.value); setfilterentries(event.target.value); 
                loadDataCategoryLimit(event.target.value)}}>
                  <MenuItem value={"0"}>Pilih Batas </MenuItem>
                  <MenuItem value={"7"}>7 peserta</MenuItem>
                  <MenuItem value={"6"}>6 peserta</MenuItem>
                  <MenuItem value={"5"}>5 peserta</MenuItem>
                  <MenuItem value={"4"}>4 peserta</MenuItem>
              </Select>
              </p> :
              pagenumber == 3 ?
              <p>Kontingen : 
                <Select value={choosecontingent._id} 
                    onChange={(event)=>{
                        if(event.target.value != "0"){
                            var data = contingents.filter(x=>x._id=== event.target.value);
                            setchoosecontingent(data[0]);
                            
                        }
                        else{
                            setchoosecontingent({_id:"0", name:"", logo:""});
                        }
                    }}
                >
                    <MenuItem value={"0"}>Semua Kontingen</MenuItem>
                    {
                        contingents.map((item,index)=>{
                            return(
                                <MenuItem value={item._id}>{item.name}</MenuItem>
                            )
                        })
                    }
                </Select>
                
                <Button style={{marginLeft:5}} variant="contained" color="secondary" onClick={()=>loadDataSummaryDiscipline()}>Load Data</Button> : 
                   
              </p>:
              
              <div style={{marginTop:5,marginBottom:5}}></div>
        }
          
          
      </div>
      <div style={{backgroundColor:'white', padding:10}}>
        
        <div class="header">
            <table style={{width:'100%', marginTop:10}}>
                <tr>
                    <td style={{textAlign:'right'}}>
                        <img src={logo} style={{height:100}}/>
                    </td>
                    {
                        pagenumber < 2 ?
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                            Laporan Pendaftaran Tahap #2
                            <br/> Cabor/Disiplin, Nomor Pertandingan, Atlit & Official
                            <br/>
                            PON XX 2021 Papua
                        </td> : 
                        pagenumber == 2 ? 
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                            Rekap Pendaftaran Tahap #2
                            <br/> Jumlah Cabor/Disiplin, Atlit & Official
                            <br/>
                            PON XX 2021 Papua
                        </td> : 
                        pagenumber == 3 ?
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                            Rekap Pendaftaran Tahap #2
                            <br/> Jumlah Kontingen, Atlit & Official
                            <br/>
                            PON XX 2021 Papua
                        </td>  :
                        pagenumber == 4 ?
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                            Laporan Pendaftaran Tahap #2
                            <br/> Cabor/Disiplin & Longlist By Kluster
                            <br/>
                            PON XX 2021 Papua
                        </td> :
                        pagenumber == 7 ? 
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                             Pendaftaran Tahap #2
                            <br/> Pengecekan Identitas Atlit
                            <br/>
                            PON XX 2021 Papua
                        </td> :
                        pagenumber == 8 ? 
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                             Pendaftaran Tahap #2
                            <br/> Distribusi Longlist
                            <br/>
                            PON XX 2021 Papua
                        </td> : 
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                            Laporan Pendaftaran Tahap #2
                            <br/> Berdasarkan Nomor Pertandingan
                            <br/>
                            PON XX 2021 Papua
                        </td>

                    }
                    
                    <td style={{textAlign:'left'}}>
                        <img src={maskot1} style={{height:100}}/>
                        <img src={maskot2} style={{height:100}}/>
                    </td>
                </tr>
            </table>
            <hr/>
        </div>
        
        {
                    loading ? <center class="no-print" style={{paddingTop:50, paddingBottom:100}}><CircularProgress/><Typography>Sedang memuat data...</Typography></center> :
        <table  style={{width:'100%'}}>
            <thead><tr><td>
                <div class="header-space">&nbsp;</div>
            </td></tr></thead>
            <tbody><tr><td>
            <div class="content">  
                {
                    pagenumber == 0 ?
                    <table style={{float:'left'}}>
                        <tr>
                            <td style={{padding:10}}>
                                <img src={url+"/repo/contingent/"+choosecontingent.logo} style={{height:80}}/>
                            </td>
                            <td>
                                <span style={{fontSize:20, fontWeight:'bold'}}>{"Provinsi "+choosecontingent.name}</span>
                            </td>
                        </tr>
                    </table> : 
                    pagenumber == 1 || pagenumber == 5 ?
                     <table style={{float:'left'}}>
                        <tr>
                            <td style={{padding:10}}>
                                <img src={url+"/repo/sport/"+choosedisciplines.event_icon} style={{height:80}}/>
                            </td>
                            <td>
                                <span style={{fontSize:20, fontWeight:'bold'}}>{choosedisciplines.event_name}</span>
                            </td>
                        </tr>
                    </table> : 
                    pagenumber == 4 ? 
                    <table style={{float:'left'}}>
                        <tr>
                            {/* <td style={{padding:10}}>
                                <img src={url+"/repo/sport/"+choosedisciplines.event_icon} style={{height:80}}/>
                            </td> */}
                            <td style={{marginTop:20}}>
                                <span style={{fontSize:20, fontWeight:'bold'}}>Kluster {cluster}</span>
                            </td>
                        </tr>
                    </table> : null
                }          
               
               {
                   pagenumber == 0 ?
                   <div style={{float:'right', padding:10, marginRight:10}}>
                        <table>
                            <tr>
                                <td>Jumlah Disiplin</td>
                                <td>:</td>
                                <td>{event.length} Disiplin</td>
                            </tr>
                            <tr>
                                <td>Jumlah Atlit</td>
                                <td>:</td>
                                <td>{
                                    _.sumBy(event.map(x=>x.data_participants.filter(y=>y.participant_type==="Athlete").length))
                                } orang</td>
                            </tr>
                            <tr>
                                <td>Jumlah Official</td>
                                <td>:</td>
                                <td>{_.sumBy(event.map(x=>x.data_participants.filter(y=>y.participant_type==="Official").length))} orang</td>
                            </tr>
                            
                        </table>
                    </div> :
                   pagenumber == 1 || pagenumber == 5?
                    <div style={{float:'right', padding:10, marginRight:10}}>
                        <table>
                            <tr>
                                <td>Jumlah Kontingen</td>
                                <td>:</td>
                                <td>{datacontingent.length} Kontingen</td>
                            </tr>
                            <tr>
                                <td>Jumlah Atlit</td>
                                <td>:</td>
                                <td>{dataparticipant.filter(x=>x.participant_type === "Athlete").length} orang</td>
                            </tr>
                            <tr>
                                <td>Jumlah Official</td>
                                <td>:</td>
                                <td>{dataparticipant.filter(x=>x.participant_type === "Official").length} orang</td>
                            </tr>
                            <tr>
                                <td>Babak Kualifikasi</td>
                                <td>:</td>
                                <td>{choosedisciplines.event_qualifications}</td>
                            </tr>
                        </table>
                    </div>
                   : 
                   pagenumber == 4 ? 
                   <div style={{float:'right', padding:10, marginRight:10}}>
                        <table>
                            <tr>
                                <td>Jumlah Disiplin</td>
                                <td>:</td>
                                <td>{dataevent.length} Disiplin</td>
                            </tr>
                            <tr>
                                <td>Jumlah Atlit</td>
                                <td>:</td>
                                <td>{dataparticipant.filter(x=>x.participant_type === "Athlete").length} orang</td>
                            </tr>
                            <tr>
                                <td>Jumlah Official</td>
                                <td>:</td>
                                <td>{dataparticipant.filter(x=>x.participant_type === "Official").length} orang</td>
                            </tr>
                            
                        </table>
                    </div>
                   : null
                }
                
                <p style={{clear:'both'}}>Update Terakhir : {moment(new Date()).format("DD-MM-YYYY HH:mm:ss")}</p>
                {
                    pagenumber==0 ? 
                        event.map((item,index)=>{
                            var dataparticipant = item.data_participants.filter(x=>
                                {
                                    return(
                                        x.event_id === item.event_id && x.participant_type === "Athlete"
                                    )
                                }                                                
                            )
                            var dataparticipantofficial = item.data_participants.filter(x=>
                                {
                                    return(
                                        x.event_id === item.event_id && x.participant_type === "Official"
                                    )
                                }
                            )
                            return(
                                <div style={{  marginBottom:20}}>
                                    <table style={{ width:'100%', backgroundColor:'#EFEFEF'}}>
                                        <tr>
                                            <td style={{width:50}}><img src={url+"/repo/sport/"+item.event_icon} style={{width:50}}/></td>
                                            <td style={{fontSize:18, fontWeight:'bold', textAlign:'left'}}>{item.event_name}</td>
                                            <td style={{textAlign:'right'}}>
                                                <h3 style={{marginRight:10}}>Prakualifikasi : {item.event_qualifications}</h3>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style={{padding:10}}>
                                    <p>1. Nomor Pertandingan</p>
                                    <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                                        <thead style={{background:'#bf272b', color:'white'}}>
                                            <tr>
                                                <td style={{width:20,padding:8, textAlign:'center'}}>No</td>
                                                <td style={{padding:8}}>Nomor Pertandingan</td>
                                                <td style={{width:200,padding:8, textAlign:'center'}}>Jumlah Entries</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                _.orderBy(item.category,["category_gender"],["asc"]).map((itemcategory, indexcategory)=>{
                                                    return(
                                                        <tr>
                                                            <td style={{padding:8, textAlign:'center'}}>{indexcategory+1}</td>
                                                            <td style={{padding:8}}>{itemcategory.category_name}</td>
                                                            <td style={{padding:8, textAlign:'center'}}>{
                                                                loadingSummary ? 
                                                                <center class="no-print"><CircularProgress size={14}/><Typography style={{fontSize:'small'}}>memuat data</Typography></center> : 
                                                                entries.filter(x=>x.category_id === itemcategory._id).length>0 ? entries.filter(x=>x.category_id === itemcategory._id)[0].individual_entries + entries.filter(x=>x.category_id === itemcategory._id)[0].group_entries:"-"
                                                            }</td>
                                                        </tr>
                                                    )
                                                }) 
                                            }
                                        </tbody>
                                    </table>
                                    <p>2. Atlit</p>
                                    {
                                        dataparticipant.length > 0 ? 
                                        <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                                            <thead style={{background:'#bf272b', color:'white'}}>
                                                <tr>
                                                    <th style={{width:20,padding:8}}>No</th>
                                                    <th style={{textAlign:'left',padding:8}}>Nama Peserta</th>
                                                    <th style={{width:80,padding:8}}>Jenis Kelamin</th>
                                                    <th style={{width:150,padding:8, textAlign:'left'}}>Tanggal Lahir</th>
                                                    <th style={{width:250,padding:8, textAlign:'left'}}>Alamat</th>
                                                    <th style={{width:350,padding:8, textAlign:'left'}}>Nomor Pertandingan</th>
                                                    <th style={{width:150,padding:8, textAlign:'left'}}>Tgl Daftar</th>
                                                    {/* <th style={{width:150,padding:8}}>SKM</th>
                                                    <th style={{width:80,padding:8}}>Sesuai/Tidak</th>
                                                    <th style={{width:100,padding:8}}>Catatan</th> */}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    _.orderBy(dataparticipant, ["gender"], ["asc"]).map((itemparticipant,indexparticipant)=>{
                                                        return(
                                                            <tr>
                                                                <td style={{width:20,padding:8}}>{indexparticipant+1}</td>
                                                                <td style={{textAlign:'left',padding:8}}>
                                                                    <a href={url+'/repo/participant/'+ itemparticipant.photo} style={{float:'left', marginRight:10}} target="_blank">
                                                                        <img
                                                                        style={{ width: 30}}
                                                                        src={url+'/repo/participant/'+ itemparticipant.photo}
                                                                        />
                                                                    </a>
                                                                    <div>
                                                                        <b style={{fontSize:14}}>{itemparticipant.full_name}</b>
                                                                        <br/>
                                                                        <span style={{fontSize:10}}>NIK : {itemparticipant.ID_Number}</span>
                                                                        <br/>
                                                                        <span style={{fontSize:10}}>KK : {itemparticipant.Family_Number}</span>
                                                                    </div>
                                                                </td>
                                                                <td style={{padding:8}}>{itemparticipant.gender === "L" ? "Laki-laki" : "Perempuan"}</td>
                                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.place_of_birth+', '+moment(itemparticipant.date_of_birth).format("DD-MM-YYYY")}</td>
                                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.address}</td>
                                                                <td style={{padding:8, textAlign:'left'}}>
                                                                    {
                                                                    entrie_individual_athletes.filter(x=>x.participant_id === itemparticipant._id).length>0 ? entrie_individual_athletes.filter(x=>x.participant_id=== itemparticipant._id)[0].categories.map(y=>{
                                                                        return(
                                                                            <p>{y.category_name}</p>
                                                                        )
                                                                    }):                                                                     
                                                                    null
                                                                }
                                                                {
                                                                    entrie_group_athletes.filter(x=> x.participant._id === itemparticipant._id).length>0 ?
                                                                    entrie_group_athletes.filter(x=> x.participant._id === itemparticipant._id).map(y=>{
                                                                        return(
                                                                            <p>{y.category.event_categories}</p>
                                                                        )
                                                                    })
                                                                    //[0].category.event_categories
                                                                    :                                                                     
                                                                    null
                                                                }
                                                                </td>
                                                                <td style={{padding:8, textAlign:'center'}}>{moment(itemparticipant.entrie_by_name_date).format("DD-MM-YYYY HH:mm:ss")}</td>
                                                                {/* <td style={{padding:8}}>
                                                                    {
                                                                        itemparticipant.SKM.status ? 
                                                                        <a href={url+'/repo/SKM/'+itemparticipant.SKM.document} style={{textDecoration:'none'}} target="_blank">
                                                                        <Typography style={{fontSize:10}}>Asal Provinsi : {itemparticipant.SKM.name_province}</Typography>
                                                                        <Typography style={{fontSize:10}}>Tgl Terbit : {itemparticipant.SKM.date_issued ? moment(itemparticipant.SKM.date_issued).format("DD-MM-YYYY") : ""}</Typography>
                                                                        <Typography style={{fontSize:10}}>Diterbikan oleh : {itemparticipant.SKM.organization_issued}</Typography>
                                                                        </a>
                                                                        : <center>-</center>
                                                                    }
                                                                </td>
                                                                <td style={{padding:8}}></td>
                                                                <td style={{padding:8}}></td> */}
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                
                                            </tbody>
                                        </table> : <p>Belum isi Data</p>
                                    }
                                    <p>3. Official</p>
                                    {
                                        dataparticipant.length > 0 ? 
                                        <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                                            <thead style={{background:'#bf272b', color:'white'}}>
                                                <tr>
                                                    <th style={{width:20,padding:8}}>No</th>
                                                    <th style={{textAlign:'left',padding:8}}>Nama Peserta</th>
                                                    <th style={{width:100,padding:8}}>Jenis Kelamin</th>
                                                    <th style={{width:150,padding:8, textAlign:'left'}}>Tanggal Lahir</th>
                                                    <th style={{width:200,padding:8, textAlign:'left'}}>Alamat</th>
                                                    <th style={{width:150,padding:8, textAlign:'center'}}>Posisi</th>
                                                    <th style={{width:150,padding:8, textAlign:'center'}}>Tgl Daftar</th>
                                                    {/* <th style={{width:80,padding:8}}>Sesuai/Tidak</th>
                                                    <th style={{width:100,padding:8}}>Catatan</th> */}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    _.orderBy(dataparticipantofficial, ["gender"], ["asc"]).map((itemparticipant,indexparticipant)=>{
                                                        return(
                                                            <tr>
                                                                <td style={{width:20,padding:8}}>{indexparticipant+1}</td>
                                                                <td style={{textAlign:'left',padding:8}}>
                                                                    <a href={url+'/repo/participant/'+ itemparticipant.photo} style={{float:'left', marginRight:10}} target="_blank">
                                                                        <img
                                                                        style={{ width: 30}}
                                                                        src={url+'/repo/participant/'+ itemparticipant.photo}
                                                                        />
                                                                    </a>
                                                                    <div>
                                                                        <b style={{fontSize:14}}>{itemparticipant.full_name}</b>
                                                                        <br/>
                                                                        <span style={{fontSize:10}}>NIK : {itemparticipant.ID_Number}</span>
                                                                        <br/>
                                                                        <span style={{fontSize:10}}>KK : {itemparticipant.Family_Number}</span>
                                                                    </div>
                                                                </td>
                                                                <td style={{padding:8}}>{itemparticipant.gender === "L" ? "Laki-laki" : "Perempuan"}</td>
                                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.place_of_birth+', '+moment(itemparticipant.date_of_birth).format("DD-MM-YYYY")}</td>
                                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.address}</td>
                                                                <td style={{padding:8, textAlign:'center'}}>{itemparticipant.jobposition}</td>
                                                                <td style={{padding:8, textAlign:'center'}}>{moment(itemparticipant.entrie_by_name_date).format("DD-MM-YYYY HH:mm:ss")}</td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                
                                            </tbody>
                                        </table> : <p>Belum isi Data</p>
                                    }
                                    </div>
                                    <hr/>
                                </div>
                            )
                        })
                    : 
                    pagenumber==1 ? 
                    <Tahap2BySport entrie_individual_athletes_by_sport={entrie_individual_athletes_by_sport} entrie_group_athletes_by_sport={entrie_group_athletes_by_sport} entries_by_sport={entries_by_sport} 
                        datacontingent={datacontingent} dataparticipant={dataparticipant}/>
                    : 
                    pagenumber==2 ? 
                    <Tahap2SummaryContingent datafootercontingent={datafootercontingent} datasummarycontingent_byname={datasummarycontingent_byname} /> : 
                    pagenumber ==3 ?
                    <Tahap2SummaryDiscipline datafooterdiscipline={datafooterdiscipline} datasummarydiscipline={datasummarydiscipline} loading={loadingdata} /> : 
                    pagenumber ==4 ?
                    <Tahap1ByCluster dataevent={dataevent} dataparticipant={dataparticipant} datacontingents = {contingents}/> :
                    pagenumber ==5 ?
                    <Tahap2SummaryCategory pagenumber={pagenumber} datacontingent={contingents} entrie_individual={entrie_individual} entrie_group={entrie_group}  dataentriequalification={dataentriequalification} datacategory={summarycategory} />:
                    pagenumber ==7 ? 
                    <Tahap1CheckDukcapil /> :
                    pagenumber == 8 ?
                    <Tahap1DistributionLonglist datalonglistdistribution={datalonglistdistribution} datacontingents = {contingents} />
                    :
                    pagenumber == 6 ?
                    <Tahap2SummaryCategory pagenumber={pagenumber} datacontingent={contingents} entrie_individual={entrie_individual} entrie_group={entrie_group}  dataentriequalification={dataentriequalification} datacategory={summarycategory} filterentries={filterentries} />
                    // <center>Under Contraction</center>
                    :
                    null

                }
             
        </div>
            </td></tr></tbody>
            <tfoot><tr><td>
                <div class="footer-space">&nbsp;</div>
            </td></tr></tfoot>
        </table>
        }
        <div class="footer">
            <hr/>
            <p>Sport Entries PON XX 2021 Papua</p>
        </div>
     </div>
     
      
    </>
  );
}

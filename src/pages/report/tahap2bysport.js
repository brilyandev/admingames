import React, { useState,useEffect } from "react";
import {
  Grid, 
  Paper,
  Avatar, 
  Typography,
  Button,
  TextField,
  InputLabel, 
  Select,
  MenuItem, 
} from "@material-ui/core";
import axios from "axios";
import moment from 'moment';
import _ from 'lodash';
import StaticVar from "../../Config/StaticVar"

export default function Tahap2BySport(props) {
  // global
  //var userDispatch = useUserDispatch();  
  const url = StaticVar.Base_Url;//"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token 
  };

  useEffect(()=>{
      console.log('props.datacontingent', JSON.stringify(props.datacontingent))
      console.log('props.dataparticipant', JSON.stringify(props.dataparticipant))
  },[])
  
  return (
    <>
      {
          props.datacontingent.map((item,index)=>{
            var dataparticipantathlete = props.dataparticipant.filter(x=>
                {
                    return(
                        x.contingent_id === item.contingent_id && x.participant_type === "Athlete"
                    )
                }
            
            )
            var dataparticipantofficial = props.dataparticipant.filter(x=>
                {
                    return(
                        x.contingent_id === item.contingent_id && x.participant_type === "Official"
                    )
                }
            
            )
            return(
                <div style={{marginBottom:20, width:'100%'}}>
                    <table style={{ width:'100%', backgroundColor:'#EFEFEF'}}>
                        <tr>
                            <td style={{width:50}}><img src={url+"/repo/contingent/"+item.contingent_logo} style={{height:40, marginRight:5}}/></td>
                            <td style={{fontSize:18, fontWeight:'bold', textAlign:'left'}}>{item.contingent}</td>
                        </tr>
                    </table>
                    <div style={{padding:10}}>

                    <p>1. Nomor Pertandingan</p>
                    <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                        <thead style={{background:'#bf272b', color:'white'}}>
                            <tr>
                                <td style={{width:20,padding:8, textAlign:'center'}}>No</td>
                                <td style={{padding:8}}>Nomor Pertandingan</td>
                                <td style={{width:200,padding:8, textAlign:'center'}}>Jumlah Entries</td>
                            </tr>
                        </thead>
                        <tbody>
                            {/* {
                                JSON.stringify(item)
                            } */}
                            {
                                _.orderBy(item.category,["category_gender"],["asc"]).map((itemcategory, indexcategory)=>{
                                    return(
                                        <tr>
                                            <td style={{padding:8, textAlign:'center'}}>{indexcategory+1}</td>
                                            <td style={{padding:8}}>{itemcategory.category_name}</td>
                                            <td style={{padding:8, textAlign:'center'}}>
                                                {
                                                    props.entries_by_sport.filter(x=>x.category_id === itemcategory._id && x.contingent_id ===item.contingent_id).length>0 ? props.entries_by_sport.filter(x=>x.category_id === itemcategory._id && x.contingent_id ===item.contingent_id)[0].individual_entries + props.entries_by_sport.filter(x=>x.category_id === itemcategory._id && x.contingent_id ===item.contingent_id)[0].group_entries:"-"
                                                }
                                            </td>
                                        </tr>
                                    )
                                }) 
                            }
                        </tbody>
                    </table>
                    {/* <table style={{clear:'both'}}>
                        <tr>
                            <td style={{verticalAlign:'top'}}>1. Nomor Pertandingan</td>
                            <td style={{verticalAlign:'top'}}>:</td>
                            <td>
                                <ol style={{paddingTop:0, marginTop:0}}>
                                    {
                                        _.orderBy(item.category, ["category_gender"],["asc"]).map((itemcategory, indexcategory)=>{
                                            return(
                                                <li>{itemcategory.category_name}</li>
                                            )
                                        })
                                    }
                                </ol>
                                
                            </td>
                        </tr>
                    </table> */}
                    <p>2. Atlit</p>
                    {
                        dataparticipantathlete.length > 0 ? 
                        <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                            <thead style={{background:'#bf272b', color:'white'}}>
                                <tr>
                                    <th style={{width:20,padding:8}}>No</th>
                                    <th style={{textAlign:'left',padding:8}}>Nama Peserta</th>
                                    <th style={{width:80,padding:8}}>Jenis Kelamin</th>
                                    <th style={{width:150,padding:8, textAlign:'left'}}>Tanggal Lahir</th>
                                    <th style={{width:250,padding:8, textAlign:'left'}}>Alamat</th>
                                    <th style={{width:350,padding:8, textAlign:'left'}}>Nomor Pertandingan</th>
                                    <th style={{width:150,padding:8, textAlign:'left'}}>Tgl Daftar</th>
                                    {/* <th style={{width:80,padding:8}}>Sesuai/Tidak</th>
                                    <th style={{width:100,padding:8}}>Catatan</th> */}
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    _.orderBy(dataparticipantathlete, ['gender'], ['asc']).map((itemparticipant,indexparticipant)=>{
                                        return(
                                            <tr>
                                                <td style={{width:20,padding:8}}>{indexparticipant+1}</td>
                                                <td style={{textAlign:'left',padding:8}}>
                                                    <a href={url+'/repo/participant/'+ itemparticipant.photo} style={{float:'left', marginRight:10}} target="_blank">
                                                        <img
                                                        style={{ width: 30}}
                                                        src={url+'/repo/participant/'+ itemparticipant.photo}
                                                        />
                                                    </a>
                                                    <div>
                                                        <b style={{fontSize:14}}>{itemparticipant.full_name}</b>
                                                        <br/>
                                                        <span style={{fontSize:10}}>NIK : {itemparticipant.ID_Number}</span>
                                                        <br/>
                                                        <span style={{fontSize:10}}>KK : {itemparticipant.Family_Number}</span>
                                                    </div>
                                                </td>
                                                <td style={{padding:8}}>{itemparticipant.gender === "L" ? "Laki-laki" : "Perempuan"}</td>
                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.place_of_birth+', '+moment(itemparticipant.date_of_birth).format("DD-MM-YYYY")}</td>
                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.address}</td>
                                                {/* <td style={{padding:8}}>
                                                    {
                                                        itemparticipant.SKM.status ? 
                                                        <a href={url+'/repo/SKM/'+itemparticipant.SKM.document} style={{textDecoration:'none'}} target="_blank">
                                                        <Typography style={{fontSize:10}}>Asal Provinsi : {itemparticipant.SKM.name_province}</Typography>
                                                        <Typography style={{fontSize:10}}>Tgl Terbit : {itemparticipant.SKM.date_issued ? moment(itemparticipant.SKM.date_issued).format("DD-MM-YYYY") : ""}</Typography>
                                                        <Typography style={{fontSize:10}}>Diterbikan oleh : {itemparticipant.SKM.organization_issued}</Typography>
                                                        </a>
                                                        : <center>-</center>
                                                    }
                                                </td> */}
                                                <td style={{padding:8, textAlign:'left'}}>
                                                    {
                                                    props.entrie_individual_athletes_by_sport.filter(x=>x.participant_id === itemparticipant._id).length>0 ? props.entrie_individual_athletes_by_sport.filter(x=>x.participant_id=== itemparticipant._id)[0].categories.map(y=>{
                                                        return(
                                                            <p>{y.category_name}</p>
                                                        )
                                                    }):                                                                     
                                                    null
                                                }
                                                {
                                                    props.entrie_group_athletes_by_sport.filter(x=> x.participant._id === itemparticipant._id).length>0 ?
                                                    props.entrie_group_athletes_by_sport.filter(x=> x.participant._id === itemparticipant._id).map(y=>{
                                                        return(
                                                            <p>{y.category.event_categories}</p>
                                                        )
                                                    }):                                                                     
                                                    null
                                                }
                                                </td>
                                                <td style={{padding:8, textAlign:'center'}}>{moment(itemparticipant.entrie_by_name_date).format("DD-MM-YYYY HH:mm:ss")}</td>
                                            </tr>
                                        )
                                    })
                                }
                                
                            </tbody>
                        </table> : <p>Belum isi Data</p>
                    }
                    <p>3. Official</p>
                    {
                        dataparticipantofficial.length > 0 ? 
                        <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                            <thead style={{background:'#bf272b', color:'white'}}>
                                <tr>
                                    <th style={{width:20,padding:8}}>No</th>
                                    <th style={{textAlign:'left',padding:8}}>Nama Peserta</th>
                                    <th style={{width:100,padding:8}}>Jenis Kelamin</th>
                                    <th style={{width:150,padding:8, textAlign:'left'}}>Tanggal Lahir</th>
                                    <th style={{width:200,padding:8, textAlign:'left'}}>Alamat</th>
                                    <th style={{width:150,padding:8, textAlign:'center'}}>Posisi</th>
                                    <th style={{width:150,padding:8, textAlign:'center'}}>Tgl Daftar</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    _.orderBy(dataparticipantofficial, ['gender'], ['asc']).map((itemparticipant,indexparticipant)=>{
                                        return(
                                            <tr>
                                                <td style={{width:20,padding:8}}>{indexparticipant+1}</td>
                                                <td style={{textAlign:'left',padding:8}}>
                                                    <a href={url+'/repo/participant/'+ itemparticipant.photo} style={{float:'left', marginRight:10}} target="_blank">
                                                        <img
                                                        style={{ width: 30}}
                                                        src={url+'/repo/participant/'+ itemparticipant.photo}
                                                        />
                                                    </a>
                                                    <div>
                                                        <b style={{fontSize:14}}>{itemparticipant.full_name}</b>
                                                        <br/>
                                                        <span style={{fontSize:10}}>NIK : {itemparticipant.ID_Number}</span>
                                                        <br/>
                                                        <span style={{fontSize:10}}>KK : {itemparticipant.Family_Number}</span>
                                                    </div>
                                                </td>
                                                <td style={{padding:8}}>{itemparticipant.gender === "L" ? "Laki-laki" : "Perempuan"}</td>
                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.place_of_birth+', '+moment(itemparticipant.date_of_birth).format("DD-MM-YYYY")}</td>
                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.address}</td>
                                                <td style={{padding:8, textAlign:'center'}}>{itemparticipant.jobposition}</td>
                                                <td style={{padding:8, textAlign:'center'}}>{moment(itemparticipant.entrie_by_name_date).format("DD-MM-YYYY HH:mm:ss")}</td>
                                            </tr>
                                        )
                                    })
                                }
                                
                            </tbody>
                        </table> : <p>Belum isi Data</p>
                    }
                    </div>
                    <hr/>
                </div>
            )
      })
      }
    </>
  );
}

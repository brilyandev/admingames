import React, { useState,useEffect } from "react";
import {
  Typography, Button,
  Grid
} from "@material-ui/core";
import moment from 'moment';
import _ from 'lodash';
import StaticVar from "../../Config/StaticVar"
import Dialog from "../Components/Dialog/Dialog"

export default function Tahap1SummaryCategory(props) {
  // global
  //var userDispatch = useUserDispatch();  
  const url = StaticVar.Base_Url;//"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token 
  };

  const [openModal, setopenModal] = useState(false)
  const [detailpeserta, setdetailpeserta] = useState([])
  const [lolosqualification, setlolosqualification] = useState([])
  const [categoryname, setcategoryname] = useState("")
  const handleClose = ()=>{
      setopenModal(false)
  }
  
  return (
    <>
        <Dialog
        open={openModal}
        close={handleClose}
        title={categoryname}
        content={
          <>
            <Grid container spacing="2">
                <Grid item md="6">
                    <Typography>Entri Tahap 1</Typography>
                    <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                        <thead style={{background:'#bf272b', color:'white'}}>
                            <tr>
                                <th style={{width:20,padding:8}}>No</th>
                                <th style={{width:420,textAlign:'left',padding:8}}>Nama Kontingen</th>
                                <th style={{width:220,padding:8, textAlign:'center'}}>Tgl Entry</th>
                            </tr>
                        </thead>
                        <tbody  class="table-stripped">
                            {
                                detailpeserta.map((item,index)=>{
                                    return(
                                        <tr>
                                            <td style={{width:20,padding:8}}>{index+1}</td>
                                            <td style={{textAlign:'left',padding:8}}>{item.contingent_name}</td>
                                            <td style={{textAlign:'center'}}>{moment(item.entrie_date).format("DD-MM-YYYY HH:mm:ss")}</td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </Grid>
                <Grid item md="6">
                    <Typography>Lolos Babak Kualifikasi</Typography>
                    <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                        <thead style={{background:'#bf272b', color:'white'}}>
                            <tr>
                                <th style={{width:20,padding:8}}>No</th>
                                <th style={{textAlign:'left',padding:8}}>Nama Kontingen</th>
                            </tr>
                        </thead>
                        <tbody  class="table-stripped">
                            {
                                lolosqualification.map((item,index)=>{
                                    return(
                                        <tr>
                                            <td style={{width:20,padding:8}}>{index+1}</td>
                                            <td style={{textAlign:'left',padding:8}}>{item.contingent_name}</td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </Grid>
            </Grid>
             
          </>
        }

        cancel={handleClose}
        valueCancel ={"Tutup"}
        // confirm={()=>postData()}
        // valueConfirm={"Simpan"}
        />
        <table style={{width:'100%', fontSize:12, marginBottom:10}}>
            <thead style={{background:'#bf272b', color:'white'}}>
                <tr>
                    <th style={{width:20,padding:8}}>No</th>
                    <th style={{textAlign:'left',padding:8}}>Nomor Pertandingan</th>
                    <th style={{width:180,padding:8}}>Jumlah Kontingen</th>
                    <th style={{width:180,padding:8}}>Jumlah Lolos</th>
                    <th style={{width:100,padding:8}}>Detail</th>
                </tr>
            </thead>
            <tbody  class="table-stripped">
                {
                    props.datacategory.map((item,index)=>{
                        return(
                            <>
                                <tr>
                                    <td style={{width:20,padding:8}}>{index+1}</td>
                                    <td style={{textAlign:'left',padding:8}}>
                                        <b style={{fontSize:14}}>
                                            {item.event_name+ " - "+ item.category_name}</b>
                                        <br/>
                                        <small>Babak Kualifikasi : {item.event_qualification}</small>
                                    </td>
                                    <td style={{padding:8, textAlign:'center'}}>{item.entrie.length}</td>
                                    <td style={{padding:8, textAlign:'center'}}>{item.qualification.length}</td>
                                    <td style={{textAlign:'center'}}>
                                        <Button variant="contained" color="secondary" onClick={()=> {setopenModal(true); setlolosqualification(item.qualification); setcategoryname(item.event_name+ " - "+ item.category_name); setdetailpeserta(_.orderBy(item.entrie,["contingent_name"],["asc"]))}}>Lihat</Button>
                                    </td>
                                </tr>
                                {
                                    props.pagenumber == 5 ?
                                    <tr>
                                        <td></td>
                                        <td colSpan="4" style={{padding:3}}>
                                            {
                                                _.orderBy(item.entrie,["contingent_name"],["asc"]).map(itemparticipant=>{
                                                    return(
                                                        <div style={{float:'left', marginRight:10, textAlign:'center'}} >
                                                            <img
                                                            style={{ height: 30}}
                                                            src={url+'/repo/contingent/'+ itemparticipant.contingent_logo}
                                                            />
                                                            <br/>
                                                            <small>
                                                                {itemparticipant.contingent_initial}
                                                            </small>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </td>
                                    </tr> : null
                                }
                                
                            </>    
                        )
                    })
                }
                
            </tbody>
        </table>
        
    </>
  );
}

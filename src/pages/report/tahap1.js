import React, { useState,useEffect } from "react";
import {
  Grid, 
  Paper,
  Avatar, 
  Typography,
  Button,
  TextField,
  InputLabel, 
  Select,
  MenuItem, 
} from "@material-ui/core";
 
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';  

// styles
//import useStyles from "./styles";
import MUIDataTable from "mui-datatables";

import PageTitle from "../Components/PageTitle/PageTitle";
import axios from "axios";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import _ from 'lodash';
import DialogFullScreen from "../../components/Dialog/DialogFullScreen";
import moment from 'moment';
import ObjectID  from 'bson-objectid'
import logo from "../../Assets/Images/logo-2021.png";
import maskot1 from "../../Assets/Images/kangpo.png";
import maskot2 from "../../Assets/Images/maskot_drawa_1.png";
import Printer, { print } from 'react-pdf-print'
import {PDFViewer, Page, Text, View, Document, StyleSheet  } from '@react-pdf/renderer';
import Tahap1BySport from "./tahap1bysport";
import "./css.css"
import Tahap1ByCluster from "./tahap1bycluster";
import Tahap1SummaryContingent from "./tahap1summarycontingent";
import Tahap1SummaryDiscipline from "./tahap1summarydiscipline";
import Tahap1SummaryCategory from "./tahap1summarycategory"
import Tahap1CheckDukcapil from "./tahap1checkdukcapil"
import StaticVar from "../../Config/StaticVar"
import Api from "../../Services/Api"
import Tahap1DistributionLonglist from "./tahap1DistributionLonglist";

export default function Tahap1(props) {
  // global
  var userDispatch = useUserDispatch();  
  //var classes = useStyles();
  //"http://localhost:300"
  const url = StaticVar.Base_Url;//"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token 
  };

  const [event, setevent]= useState([])
  const [dataevent, setdataevent]= useState([])
  const [disciplines, setdisciplines]= useState([])
  const [contingents, setcontingents]= useState([])
  const [choosecontingent, setchoosecontingent]= useState({_id:"", name:"", logo:""})
  const [choosedisciplines, setchoosedisciplines]= useState({_id:"", event_name:"", event_icon:""})
  const [participant, setparticipant] = useState([])
  const [summarycategory, setsummarycategory] = useState([])

  const [cluster, setcluster]= useState("0")

  const [datacontingent, setdatacontingent]= useState([])
  const [dataparticipant, setdataparticipant]= useState([])

  const [pagenumber, setpagenumber] = useState(0)
  const [printPDF, setprintPDF] = useState(false)
  const [documentPDF, setdocumentPDF] = useState("")

  const [datasummarycontingent, setdatasummarycontingent]= useState([])
  const [datasummarydiscipline, setdatasummarydiscipline]= useState([])
  const [dataentriequalification, setdataentriequalification]= useState([])
  const [filterentries, setfilterentries] = useState("0")


  const ids = ['1']
  function loadData(_id){
    const req1 = axios.get(url+"/private/entrie_by_numbers/contingent/"+_id,{headers}); 
    const req2 = axios.get(url+"/private/participants?contingent_id="+_id,{headers}); 
    axios.all([req1, req2]).then(axios.spread((...responses) => {
        const response1 = responses[0]  
        const response2 = responses[1]  
        setevent(response1.data)
        setparticipant(response2.data)
    })).catch(errors => {
        console.log('error',errors)
    })

  }

  function loadDataCluster(data){
    axios.get(url+"/private/events?cluster="+data,{headers}).then((response)=>{
        console.log('dataevent', response.data)
        var eventlist = response.data.map(x=> x._id)
        setdataevent(response.data)
        axios.get(url+"/private/participants?event_list="+JSON.stringify(eventlist),{headers}).then((responsedataparticipant)=>{
            console.log('responsedataparticipant',responsedataparticipant.data)
            setdataparticipant(responsedataparticipant.data)
        })
        
    })
  }

  function loadDataEvent(_id){
    const req1 = axios.get(url+"/private/entrie_by_numbers/event/"+_id,{headers}); 
    const req2 = axios.get(url+"/private/participants?event_id="+_id,{headers}); 
    axios.all([req1, req2]).then(axios.spread((...responses) => {
        const response1 = responses[0]  
        const response2 = responses[1]  
        setdatacontingent(_.orderBy(response1.data, ["contingent"], ["asc"]))
        setdataparticipant(response2.data)
    })).catch(errors => {
        console.log('error',errors)
    })

  }

  function loadDataCategory(_id){
    axios.get(url+"/private/events/category/"+_id, {headers}).then(async res=>{
        var datacategory = res.data;
        var dataqualification = await Api.postEntrieQualification().then(res=>res.data)
        datacategory.map(item=>{
            var _dqualification
            if(!(item.event_qualification === "Lolos By Quota" || item.event_qualification === "Lolos By Name")){
                _dqualification = dataqualification.filter(x=>x.event_id === item._id).map(itemqualification=>{
                    return(
                        {
                            "event_id" : itemqualification.event_id,
                            "event_name" : itemqualification.event_name,
                            "event_qualification" : itemqualification.event_qualification,
                            "participant" : itemqualification.participant.filter(x=>x.category_name === item.category_name)
                        }
                    )
                })
            }
            else{
                _dqualification = dataqualification.filter(x=>x.event_id === item._id)
            }
            item.qualification = _dqualification.length>0 ? _dqualification[0].participant : []
        })
        console.log('datacategory', datacategory)
        setsummarycategory(datacategory)
    })
    
  }

  function loadDataCategoryLimit(limit){
    axios.get(url+"/private/events/category_limit/"+limit, {headers}).then(async res=>{
        var datacategory = res.data;
        var dataqualification = await Api.postEntrieQualification().then(res=>res.data)
        datacategory.map(item=>{
            var _dqualification
            if(!(item.event_qualification === "Lolos By Quota" || item.event_qualification === "Lolos By Name")){
                _dqualification = dataqualification.filter(x=>x.event_id === item._id).map(itemqualification=>{
                    return(
                        {
                            "event_id" : itemqualification.event_id,
                            "event_name" : itemqualification.event_name,
                            "event_qualification" : itemqualification.event_qualification,
                            "participant" : itemqualification.participant.filter(x=>x.category_name === item.category_name)
                        }
                    )
                })
            }
            else{
                _dqualification = dataqualification.filter(x=>x.event_id === item._id)
            }
            item.qualification = _dqualification.length>0 ? _dqualification[0].participant : []
        })
        setsummarycategory(datacategory)
    })
    
  }

  const [datafootercontingent, setdatafootercontingent] = useState([])

  const [datafooterdiscipline, setdatafooterdiscipline] = useState([])

  function loadDataSummaryContingent(_id){
    const req1 = axios.get(url+"/private/contingents/accreditation",{headers}); 
    axios.all([req1]).then(axios.spread((...responses) => {
        const response1 = responses[0]  
        
        
        var datamanipulation = [];
        var data = response1.data;
        var entrie_by_numbers = 0
        var sum_athlete_count_male = 0
        var sum_athlete_count_female = 0
        var sum_official_count_male = 0
        var sum_official_count_female = 0
        var sum_official_kontingen_count_male = 0
        var sum_official_kontingen_count_female = 0
        var number_of_event = 0

        var datasummary = []

        data.forEach(item => {
            entrie_by_numbers = entrie_by_numbers+ item.data_entrie_by_numbers.length
            sum_athlete_count_male = sum_athlete_count_male+ item.data_longlists.filter(x=>{return x.participant_type === "Athlete" && x.gender=== "L"}).length
            sum_athlete_count_female = sum_athlete_count_female+ item.data_longlists.filter(x=>{return x.participant_type === "Athlete" && x.gender=== "P"}).length
            sum_official_count_male = sum_official_count_male+item.data_longlists.filter(x=>{return x.participant_type === "Official" && x.gender=== "L"}).length
            sum_official_count_female = sum_official_count_female+ item.data_longlists.filter(x=>{return x.participant_type === "Official" && x.gender=== "P"}).length
            sum_official_kontingen_count_male = sum_official_kontingen_count_male+ item.data_longlists.filter(x=>{return x.participant_type === "OfficialKontingen" && x.gender=== "L"}).length
            sum_official_kontingen_count_female = sum_official_kontingen_count_female+ item.data_longlists.filter(x=>{return x.participant_type === "OfficialKontingen" && x.gender=== "P"}).length
            
            var _number_of_event = 0
            item.data_entrie_by_numbers.forEach(itemevent=>{
                _number_of_event =  _number_of_event+itemevent.category.length                
            })
            number_of_event = number_of_event + _number_of_event

            var athlete_count_male = item.data_longlists.filter(x=>{
                return x.participant_type === "Athlete" && x.gender=== "L"
            })
            var athlete_count_female = item.data_longlists.filter(x=>{
                return x.participant_type === "Athlete" && x.gender=== "P"
            })
            var official_count_male = item.data_longlists.filter(x=>{
                return x.participant_type === "Official" && x.gender=== "L"
            })
            var official_count_female = item.data_longlists.filter(x=>{
                return x.participant_type === "Official" && x.gender=== "P"
            })
            var official_kontingen_count_male = item.data_longlists.filter(x=>{
                return x.participant_type === "OfficialKontingen" && x.gender=== "L"
            })
            var official_kontingen_count_female = item.data_longlists.filter(x=>{
                return x.participant_type === "OfficialKontingen" && x.gender=== "P"
            })

            datamanipulation.push({
                _id : item._id,
                name : item.name,
                logo : item.logo,
                entrie_by_number : item.data_entrie_by_numbers.length,
                number_of_event :  _number_of_event,
                // data_longlists : item.data_longlists,
                athlete_count_male : athlete_count_male.length,
                athlete_count_female : athlete_count_female.length,
                athlete_count : athlete_count_male.length+athlete_count_female.length,
                official_count_male : official_count_male.length,
                official_count_female : official_count_female.length,
                official_count : official_count_male.length+official_count_female.length,
                official_kontingen_count_male : official_kontingen_count_male.length,
                official_kontingen_count_female : official_kontingen_count_female.length,
                official_kontingen_count : official_kontingen_count_male.length+official_kontingen_count_female.length,
                longlist_male : athlete_count_male.length+official_count_male.length+official_kontingen_count_male.length,
                longlist_female : athlete_count_female.length+official_count_female.length+official_kontingen_count_female.length,
                longlist : athlete_count_male.length+official_count_male.length+official_kontingen_count_male.length+
                    athlete_count_female.length+official_count_female.length+official_kontingen_count_female.length

            })
        });
        
        datasummary.push({
            entrie_by_numbers : entrie_by_numbers,
            number_of_event : number_of_event,
            athlete_count_male : sum_athlete_count_male,
            athlete_count_female : sum_athlete_count_female,
            athlete_count : sum_athlete_count_male+sum_athlete_count_female,
            official_count_male : sum_official_count_male,
            official_count_female : sum_official_count_female,
            official_count : sum_official_count_male+sum_official_count_female,
            official_kontingen_count_male : sum_official_kontingen_count_male,
            official_kontingen_count_female : sum_official_kontingen_count_female,
            official_kontingen_count : sum_official_kontingen_count_male+sum_official_kontingen_count_female,
            longlist_male : sum_athlete_count_male+sum_official_count_male+sum_official_kontingen_count_male,
            longlist_female : sum_athlete_count_female+sum_official_count_female+sum_official_kontingen_count_female,
            longlist : sum_athlete_count_male+sum_official_count_male+sum_official_kontingen_count_male+
                sum_athlete_count_female+sum_official_count_female+sum_official_kontingen_count_female
        });

        setdatasummarycontingent(_.orderBy(datamanipulation, ["name"], ["asc"]))
        setdatafootercontingent(datasummary);
    })).catch(errors => {
        console.log('error',errors)
    })

  }

  function loadDataSummaryDiscipline(){
      axios.get(url+'/private/entrie_by_numbers/summary/event', {headers}).then(res=>{
          console.log('res.data', res.data)
        setdatasummarydiscipline(_.orderBy(res.data, ["event_name"], ["asc"]))
        let datasummary = [
            {
                entrie_by_numbers : _.sumBy(res.data,"entrie_by_number"),
                number_of_event : _.sumBy(res.data,"number_of_event"),
                athlete_count_male : _.sumBy(res.data,"athlete_count_male"),
                athlete_count_female : _.sumBy(res.data,"athlete_count_female"),
                official_count_male : _.sumBy(res.data,"official_count_male"),
                official_count_female : _.sumBy(res.data,"official_count_female"),
            }
        ];
        setdatafooterdiscipline(datasummary);
      })
    //const req1 = axios.get(url+"/private/events/summary",{headers}); 
    // axios.all([req1]).then(axios.spread((...responses) => {
    //     const response1 = responses[0]  
        
        
    //     var datamanipulation = [];
    //     var data = response1.data;
    //     var number_of_contingents = 0
    //     var sum_athlete_count_male = 0
    //     var sum_athlete_count_female = 0
    //     var sum_official_count_male = 0
    //     var sum_official_count_female = 0
    //     // var sum_official_kontingen_count_male = 0
    //     // var sum_official_kontingen_count_female = 0
    //     var number_of_event = 0

    //     var datasummary = []

    //     data.forEach(item => {
    //         number_of_contingents = number_of_contingents+ item.data_entrie_by_numbers.length
    //         number_of_event = number_of_event + item.event_category.length
    //         sum_athlete_count_male = sum_athlete_count_male+ item.data_longlists.filter(x=>{return x.participant_type === "Athlete" && x.gender=== "L"}).length
    //         sum_athlete_count_female = sum_athlete_count_female+ item.data_longlists.filter(x=>{return x.participant_type === "Athlete" && x.gender=== "P"}).length
    //         sum_official_count_male = sum_official_count_male+item.data_longlists.filter(x=>{return x.participant_type === "Official" && x.gender=== "L"}).length
    //         sum_official_count_female = sum_official_count_female+ item.data_longlists.filter(x=>{return x.participant_type === "Official" && x.gender=== "P"}).length
           
    //         var athlete_count_male = item.data_longlists.filter(x=>{
    //             return x.participant_type === "Athlete" && x.gender=== "L"
    //         })
    //         var athlete_count_female = item.data_longlists.filter(x=>{
    //             return x.participant_type === "Athlete" && x.gender=== "P"
    //         })
    //         var official_count_male = item.data_longlists.filter(x=>{
    //             return x.participant_type === "Official" && x.gender=== "L"
    //         })
    //         var official_count_female = item.data_longlists.filter(x=>{
    //             return x.participant_type === "Official" && x.gender=== "P"
    //         })

    //         datamanipulation.push({
    //             _id : item._id,
    //             event_name : item.event_name,
    //             event_icon : item.event_icon,
    //             entrie_by_number : item.data_entrie_by_numbers.length,
    //             number_of_event :  item.event_category.length,
    //             athlete_count_male : athlete_count_male.length,
    //             athlete_count_female : athlete_count_female.length,
    //             athlete_count : athlete_count_male.length+athlete_count_female.length,
    //             official_count_male : official_count_male.length,
    //             official_count_female : official_count_female.length,
    //             official_count : official_count_male.length+official_count_female.length,
    //             longlist_male : athlete_count_male.length+official_count_male.length,
    //             longlist_female : athlete_count_female.length+official_count_female.length,
    //             longlist : athlete_count_male.length+official_count_male.length+athlete_count_female.length+official_count_female.length

    //         })
    //     });
        
    //     datasummary.push({
    //         entrie_by_numbers : number_of_contingents,
    //         number_of_event : number_of_event,
    //         athlete_count_male : sum_athlete_count_male,
    //         athlete_count_female : sum_athlete_count_female,
    //         official_count_male : sum_official_count_male,
    //         official_count_female : sum_official_count_female,
    //     });

    //     setdatasummarydiscipline(_.orderBy(datamanipulation, ["event_name"], ["asc"]))
    //     setdatafooterdiscipline(datasummary);
    // })).catch(errors => {
    //     console.log('error',errors)
    // })

  }

  useEffect(()=>{
    const req1 = axios.get(url+"/private/contingents",{headers});  
    const req2 = axios.get(url+"/private/events/get_all_event",{headers});    
    axios.all([req1, req2]).then(axios.spread((...responses) => {
        const response1 = responses[0]
        const response2 = responses[1]
        setcontingents(response1.data)
        setdisciplines(response2.data)
    })).catch(errors => {
        console.log('error',errors)
    })

    if(localStorage.user_access === "Bidang Pertandingan"){
        setpagenumber(2); loadDataSummaryContingent()
    }
  },[])

  const [datalonglistdistribution, setdatalonglistdistribution] = useState([])

  function loadLonglistDistribution(){
    axios.post(url+"/private/participants/longlistdistribution",{},{headers}).then(res=>{
        console.log('loadLonglistDistribution', res.data)
        setdatalonglistdistribution(res.data)
    })
  }


  return (
    <>
      <div  class="no-print">
          <PageTitle title="Pendaftaran Tahap #1"/>
          {
              localStorage.user_access === "Bidang Pertandingan" ? 
              <>
                <Button variant="contained" color={pagenumber==2?"primary":"default"} style={{marginRight:5}} onClick={()=>{setpagenumber(2); loadDataSummaryContingent()}}>Rekap Kontingen</Button>
                <Button variant="contained" color={pagenumber==3?"primary":"default"} onClick={()=>{setpagenumber(3); loadDataSummaryDiscipline()}}>Rekap Cabor/Disiplin</Button>
              </> : 
              <>
                <Button variant="contained" color={pagenumber==0?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>setpagenumber(0)}>By Kontingen</Button>
                <Button variant="contained" color={pagenumber==1?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>setpagenumber(1)}>By Cabor/Disiplin</Button>
                <Button variant="contained" color={pagenumber==4?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>setpagenumber(4)}>By Cluster</Button>
                <Button variant="contained" color={pagenumber==2?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>{setpagenumber(2); loadDataSummaryContingent()}}>Rekap Kontingen</Button>
                <Button variant="contained" color={pagenumber==3?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>{setpagenumber(3); loadDataSummaryDiscipline()}}>Rekap Cabor/Disiplin</Button>
                <Button variant="contained" color={pagenumber==5?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>{setpagenumber(5); loadDataSummaryDiscipline()}}>Rekap No.Pertandingan</Button>
                <Button variant="contained" color={pagenumber==6?"primary":"default"} style={{marginRight:5, fontSize:11}} onClick={()=>{setpagenumber(6); loadDataSummaryDiscipline()}}>No.Pertandingan</Button>
                <Button variant="contained" color={pagenumber==7?"primary":"default"} style={{fontSize:11}} onClick={()=>{setpagenumber(7); loadDataSummaryDiscipline()}}>Cek Dukcapil</Button>
                <Button variant="contained" color={pagenumber==8?"primary":"default"} style={{fontSize:11}} onClick={()=>{setpagenumber(8); loadLonglistDistribution()}}>Distribusi Longlist</Button>
              </>
          }
          
          {
              pagenumber==0 ? 
              <p>Kontingen : 
                <Select value={choosecontingent._id} 
                    onChange={(event)=>{
                        if(event.target.value != "0"){
                            var data = contingents.filter(x=>x._id=== event.target.value);
                            setchoosecontingent(data[0]);
                            loadData(event.target.value);
                        }
                        else{
                            setchoosecontingent({_id:"", name:"", logo:""});
                        }
                    }}
                >
                    <MenuItem value={"0"}>Pilih Kontingen</MenuItem>
                    {
                        contingents.map((item,index)=>{
                            return(
                                <MenuItem value={item._id}>{item.name}</MenuItem>
                            )
                        })
                    }
                </Select>
                
              </p>
              : 
              pagenumber==1 || pagenumber ==5 ? 
              <p>Disiplin : 
                <Select value={choosedisciplines._id} 
                    onChange={(event)=>{
                        if(event.target.value != "0"){
                            var data = disciplines.filter(x=>x._id=== event.target.value);
                            setchoosedisciplines(data[0]);
                            if(pagenumber== 1){
                                loadDataEvent(event.target.value);
                            }
                            else{
                                loadDataEvent(event.target.value);
                                loadDataCategory(event.target.value)
                            }
                        }
                        else{
                            setchoosedisciplines({_id:"", event_name:"", event_icon:"", event_qualifications:""});
                        }
                    }}
                >
                    <MenuItem value={"0"}>Pilih Disiplin</MenuItem>
                    {
                        disciplines.map((item,index)=>{
                            return(
                                <MenuItem value={item._id}>{item.event_name}</MenuItem>
                            )
                        })
                    }
                </Select>
                </p>
              :
              pagenumber == 4 ? 
              <p>Kluster : 
                <Select value={cluster} onChange={(event)=>{setcluster(event.target.value); loadDataCluster(event.target.value)}}>
                    <MenuItem value={"0"}>Pilih Kluster</MenuItem>
                    <MenuItem value={"Kota Jayapura"}>Kota Jayapura</MenuItem>
                    <MenuItem value={"Kabupaten Jayapura"}>Kabupaten Jayapura</MenuItem>
                    <MenuItem value={"Kabupaten Merauke"}>Kabupaten Merauke</MenuItem>
                    <MenuItem value={"Kabupaten Mimika"}>Kabupaten Mimika</MenuItem>
                </Select>
              </p> :
              pagenumber == 6 ?
              <p>Jumlah Peserta : 
              <Select value={filterentries} onChange={(event)=>{setfilterentries(event.target.value); 
                loadDataCategoryLimit(event.target.value)}}>
                  <MenuItem value={"0"}>Pilih Batas </MenuItem>
                  <MenuItem value={"7"}>7 peserta</MenuItem>
                  <MenuItem value={"6"}>6 peserta</MenuItem>
                  <MenuItem value={"5"}>5 peserta</MenuItem>
                  <MenuItem value={"4"}>4 peserta</MenuItem>
              </Select>
              </p> :
              <div style={{marginTop:5,marginBottom:5}}></div>
        }
          
          
      </div>
      <div style={{backgroundColor:'white', padding:10}}>
        
        <div class="header">
            <table style={{width:'100%', marginTop:10}}>
                <tr>
                    <td style={{textAlign:'right'}}>
                        <img src={logo} style={{height:100}}/>
                    </td>
                    {
                        pagenumber < 2 ?
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                            Laporan Pendaftaran Tahap #1 
                            <br/> Cabor/Disiplin, Nomor Pertandingan & Longlist
                            <br/>
                            PON XX 2021 Papua
                        </td> : 
                        pagenumber == 2 ? 
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                            Rekap Pendaftaran Tahap #1 
                            <br/> Jumlah Cabor/Disiplin, Atlit & Official
                            <br/>
                            PON XX 2021 Papua
                        </td> : 
                        pagenumber == 3 ?
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                            Rekap Pendaftaran Tahap #1 
                            <br/> Jumlah Kontingen, Atlit & Official
                            <br/>
                            PON XX 2021 Papua
                        </td>  :
                        pagenumber == 4 ?
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                            Laporan Pendaftaran Tahap #1 
                            <br/> Cabor/Disiplin & Longlist By Kluster
                            <br/>
                            PON XX 2021 Papua
                        </td> :
                        pagenumber == 7 ? 
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                             Pendaftaran Tahap #1 
                            <br/> Pengecekan Identitas Atlit
                            <br/>
                            PON XX 2021 Papua
                        </td> :
                        pagenumber == 8 ? 
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                             Pendaftaran Tahap #1 
                            <br/> Distribusi Longlist
                            <br/>
                            PON XX 2021 Papua
                        </td> : 
                        <td style={{textAlign:'center', width:700, fontFamily:'Arial', fontSize:24, fontWeight:'bold'}}>
                        
                            Laporan Pendaftaran Tahap #1 
                            <br/> Berdasarkan Nomor Pertandingan
                            <br/>
                            PON XX 2021 Papua
                        </td>

                    }
                    
                    <td style={{textAlign:'left'}}>
                        <img src={maskot1} style={{height:100}}/>
                        <img src={maskot2} style={{height:100}}/>
                    </td>
                </tr>
            </table>
            <hr/>
        </div>
        

        <table  style={{width:'100%'}}>
            <thead><tr><td>
                <div class="header-space">&nbsp;</div>
            </td></tr></thead>
            <tbody><tr><td>
            <div class="content">  
                {
                    pagenumber == 0 ?
                    <table style={{float:'left'}}>
                        <tr>
                            <td style={{padding:10}}>
                                <img src={url+"/repo/contingent/"+choosecontingent.logo} style={{height:80}}/>
                            </td>
                            <td>
                                <span style={{fontSize:20, fontWeight:'bold'}}>{"Provinsi "+choosecontingent.name}</span>
                            </td>
                        </tr>
                    </table> : 
                    pagenumber == 1 || pagenumber == 5 ?
                     <table style={{float:'left'}}>
                        <tr>
                            <td style={{padding:10}}>
                                <img src={url+"/repo/sport/"+choosedisciplines.event_icon} style={{height:80}}/>
                            </td>
                            <td>
                                <span style={{fontSize:20, fontWeight:'bold'}}>{choosedisciplines.event_name}</span>
                            </td>
                        </tr>
                    </table> : 
                    pagenumber == 4 ? 
                    <table style={{float:'left'}}>
                        <tr>
                            {/* <td style={{padding:10}}>
                                <img src={url+"/repo/sport/"+choosedisciplines.event_icon} style={{height:80}}/>
                            </td> */}
                            <td style={{marginTop:20}}>
                                <span style={{fontSize:20, fontWeight:'bold'}}>Kluster {cluster}</span>
                            </td>
                        </tr>
                    </table> : null
                }          
               
               {
                   pagenumber == 0 ?
                   <div style={{float:'right', padding:10, marginRight:10}}>
                        <table>
                            <tr>
                                <td>Jumlah Disiplin</td>
                                <td>:</td>
                                <td>{event.length} Disiplin</td>
                            </tr>
                            <tr>
                                <td>Jumlah Atlit</td>
                                <td>:</td>
                                <td>{participant.filter(x=>x.participant_type === "Athlete").length} orang</td>
                            </tr>
                            <tr>
                                <td>Jumlah Official</td>
                                <td>:</td>
                                <td>{participant.filter(x=>x.participant_type === "Official").length} orang</td>
                            </tr>
                            
                        </table>
                    </div> :
                   pagenumber == 1 || pagenumber == 5?
                    <div style={{float:'right', padding:10, marginRight:10}}>
                        <table>
                            <tr>
                                <td>Jumlah Kontingen</td>
                                <td>:</td>
                                <td>{datacontingent.length} Kontingen</td>
                            </tr>
                            <tr>
                                <td>Jumlah Atlit</td>
                                <td>:</td>
                                <td>{dataparticipant.filter(x=>x.participant_type === "Athlete").length} orang</td>
                            </tr>
                            <tr>
                                <td>Jumlah Official</td>
                                <td>:</td>
                                <td>{dataparticipant.filter(x=>x.participant_type === "Official").length} orang</td>
                            </tr>
                            <tr>
                                <td>Babak Kualifikasi</td>
                                <td>:</td>
                                <td>{choosedisciplines.event_qualifications}</td>
                            </tr>
                        </table>
                    </div>
                   : 
                   pagenumber == 4 ? 
                   <div style={{float:'right', padding:10, marginRight:10}}>
                        <table>
                            <tr>
                                <td>Jumlah Disiplin</td>
                                <td>:</td>
                                <td>{dataevent.length} Disiplin</td>
                            </tr>
                            <tr>
                                <td>Jumlah Atlit</td>
                                <td>:</td>
                                <td>{dataparticipant.filter(x=>x.participant_type === "Athlete").length} orang</td>
                            </tr>
                            <tr>
                                <td>Jumlah Official</td>
                                <td>:</td>
                                <td>{dataparticipant.filter(x=>x.participant_type === "Official").length} orang</td>
                            </tr>
                            
                        </table>
                    </div>
                   : null
                }
                
                <p style={{clear:'both'}}>Update Terakhir : {moment(new Date()).format("DD-MM-YYYY HH:mm:ss")}</p>
                {
                    pagenumber==0 ? 
                        event.map((item,index)=>{
                            var dataparticipant = participant.filter(x=>
                                {
                                    return(
                                        x.event_id === item.event_id && x.participant_type === "Athlete"
                                    )
                                }
                            
                            )
                            var dataparticipantofficial = participant.filter(x=>
                                {
                                    return(
                                        x.event_id === item.event_id && x.participant_type === "Official"
                                    )
                                }
                            
                            )
                            return(
                                <div style={{  marginBottom:20}}>
                                    <table style={{ width:'100%', backgroundColor:'#EFEFEF'}}>
                                        <tr>
                                            <td style={{width:50}}><img src={url+"/repo/sport/"+item.event_icon} style={{width:50}}/></td>
                                            <td style={{fontSize:18, fontWeight:'bold', textAlign:'left'}}>{item.event_name}</td>
                                            <td style={{textAlign:'right'}}>
                                                <h3 style={{marginRight:10}}>Prakualifikasi : {item.event_qualifications}</h3>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style={{padding:10}}>
                                    <table style={{clear:'both'}}>
                                        <tr>
                                            <td style={{verticalAlign:'top'}}>1. Nomor Pertandingan</td>
                                            <td style={{verticalAlign:'top'}}>:</td>
                                            <td>
                                                <ol style={{paddingTop:0, marginTop:0}}>
                                                    {
                                                        _.orderBy(item.category,["category_gender"],["asc"]).map((itemcategory, indexcategory)=>{
                                                            return(
                                                                <li>{itemcategory.category_name}</li>
                                                            )
                                                        }) 
                                                    }
                                                </ol>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <p>2. Atlit</p>
                                    {
                                        dataparticipant.length > 0 ? 
                                        <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                                            <thead style={{background:'#bf272b', color:'white'}}>
                                                <tr>
                                                    <th style={{width:20,padding:8}}>No</th>
                                                    <th style={{textAlign:'left',padding:8}}>Nama Peserta</th>
                                                    <th style={{width:80,padding:8}}>Jenis Kelamin</th>
                                                    <th style={{width:150,padding:8, textAlign:'left'}}>Tanggal Lahir</th>
                                                    <th style={{width:250,padding:8, textAlign:'left'}}>Alamat</th>
                                                    <th style={{width:150,padding:8}}>SKM</th>
                                                    <th style={{width:80,padding:8}}>Sesuai/Tidak</th>
                                                    <th style={{width:100,padding:8}}>Catatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    _.orderBy(dataparticipant, ["gender"], ["asc"]).map((itemparticipant,indexparticipant)=>{
                                                        return(
                                                            <tr>
                                                                <td style={{width:20,padding:8}}>{indexparticipant+1}</td>
                                                                <td style={{textAlign:'left',padding:8}}>
                                                                    <a href={url+'/repo/participant/'+ itemparticipant.photo} style={{float:'left', marginRight:10}} target="_blank">
                                                                        <img
                                                                        style={{ width: 30}}
                                                                        src={url+'/repo/participant/'+ itemparticipant.photo}
                                                                        />
                                                                    </a>
                                                                    <div>
                                                                        <b style={{fontSize:14}}>{itemparticipant.full_name}</b>
                                                                        <br/>
                                                                        <span style={{fontSize:10}}>NIK : {itemparticipant.ID_Number}</span>
                                                                        <br/>
                                                                        <span style={{fontSize:10}}>KK : {itemparticipant.Family_Number}</span>
                                                                    </div>
                                                                </td>
                                                                <td style={{padding:8}}>{itemparticipant.gender === "L" ? "Laki-laki" : "Perempuan"}</td>
                                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.place_of_birth+', '+moment(itemparticipant.date_of_birth).format("DD-MM-YYYY")}</td>
                                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.address}</td>
                                                                <td style={{padding:8}}>
                                                                    {
                                                                        itemparticipant.SKM?.status ? 
                                                                        <a href={url+'/repo/SKM/'+itemparticipant.SKM.document} style={{textDecoration:'none'}} target="_blank">
                                                                        <Typography style={{fontSize:10}}>Asal Provinsi : {itemparticipant.SKM.name_province}</Typography>
                                                                        <Typography style={{fontSize:10}}>Tgl Terbit : {itemparticipant.SKM.date_issued ? moment(itemparticipant.SKM.date_issued).format("DD-MM-YYYY") : ""}</Typography>
                                                                        <Typography style={{fontSize:10}}>Diterbikan oleh : {itemparticipant.SKM.organization_issued}</Typography>
                                                                        </a>
                                                                        : <center>-</center>
                                                                    }
                                                                </td>
                                                                <td style={{padding:8}}></td>
                                                                <td style={{padding:8}}></td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                
                                            </tbody>
                                        </table> : <p>Belum isi Data</p>
                                    }
                                    <p>3. Official</p>
                                    {
                                        dataparticipant.length > 0 ? 
                                        <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                                            <thead style={{background:'#bf272b', color:'white'}}>
                                                <tr>
                                                    <th style={{width:20,padding:8}}>No</th>
                                                    <th style={{textAlign:'left',padding:8}}>Nama Peserta</th>
                                                    <th style={{width:100,padding:8}}>Jenis Kelamin</th>
                                                    <th style={{width:150,padding:8, textAlign:'left'}}>Tanggal Lahir</th>
                                                    <th style={{width:200,padding:8, textAlign:'left'}}>Alamat</th>
                                                    <th style={{width:80,padding:8}}>Sesuai/Tidak</th>
                                                    <th style={{width:100,padding:8}}>Catatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    _.orderBy(dataparticipantofficial, ["gender"], ["asc"]).map((itemparticipant,indexparticipant)=>{
                                                        return(
                                                            <tr>
                                                                <td style={{width:20,padding:8}}>{indexparticipant+1}</td>
                                                                <td style={{textAlign:'left',padding:8}}>
                                                                    <a href={url+'/repo/participant/'+ itemparticipant.photo} style={{float:'left', marginRight:10}} target="_blank">
                                                                        <img
                                                                        style={{ width: 30}}
                                                                        src={url+'/repo/participant/'+ itemparticipant.photo}
                                                                        />
                                                                    </a>
                                                                    <div>
                                                                        <b style={{fontSize:14}}>{itemparticipant.full_name}</b>
                                                                        <br/>
                                                                        <span style={{fontSize:10}}>NIK : {itemparticipant.ID_Number}</span>
                                                                        <br/>
                                                                        <span style={{fontSize:10}}>KK : {itemparticipant.Family_Number}</span>
                                                                    </div>
                                                                </td>
                                                                <td style={{padding:8}}>{itemparticipant.gender === "L" ? "Laki-laki" : "Perempuan"}</td>
                                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.place_of_birth+', '+moment(itemparticipant.date_of_birth).format("DD-MM-YYYY")}</td>
                                                                <td style={{padding:8, textAlign:'left'}}>{itemparticipant.address}</td>
                                                                <td style={{padding:8}}></td>
                                                                <td style={{padding:8}}></td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                
                                            </tbody>
                                        </table> : <p>Belum isi Data</p>
                                    }
                                    </div>
                                    <hr/>
                                </div>
                            )
                        })
                    : 
                    pagenumber==1 ? 
                    <Tahap1BySport datacontingent={datacontingent} dataparticipant={dataparticipant}/>
                    : 
                    pagenumber==2 ? 
                    <Tahap1SummaryContingent datafootercontingent={datafootercontingent} datasummarycontingent={datasummarycontingent} /> : 
                    pagenumber ==3 ?
                    <Tahap1SummaryDiscipline datafooterdiscipline={datafooterdiscipline} datasummarydiscipline={datasummarydiscipline} /> : 
                    pagenumber ==4 ?
                    <Tahap1ByCluster dataevent={dataevent} dataparticipant={dataparticipant} datacontingents = {contingents}/> :
                    pagenumber ==7 ? 
                    <Tahap1CheckDukcapil /> :
                    pagenumber == 8 ?
                    <Tahap1DistributionLonglist datalonglistdistribution={datalonglistdistribution} datacontingents = {contingents} />
                    :
                    <Tahap1SummaryCategory pagenumber={pagenumber} dataentriequalification={dataentriequalification} datacategory={summarycategory} />

                }
             
        </div>
            </td></tr></tbody>
            <tfoot><tr><td>
                <div class="footer-space">&nbsp;</div>
            </td></tr></tfoot>
        </table>

        <div class="footer">
            <hr/>
            <p>Sport Entries PON XX 2021 Papua</p>
        </div>
     </div>
     
      
    </>
  );
}

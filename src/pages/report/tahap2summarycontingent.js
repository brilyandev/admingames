import React, { useState,useEffect } from "react";
import {
  Grid, 
  Paper,
  Avatar, 
  Typography,
  Button,
  TextField,
  InputLabel, 
  Select,
  MenuItem, 
} from "@material-ui/core";
import axios from "axios";
import moment from 'moment';
import _ from 'lodash';
import { CSVLink } from "react-csv";
import ReactExport from "react-export-excel";
import StaticVar from "../../Config/StaticVar"

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function Tahap2SummaryContingent(props) {
  // global
  //var userDispatch = useUserDispatch();  
  const url = StaticVar.Base_Url;//"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token 
  };


  useEffect(()=>{
  },[])
  
  return (
    <>

        {/* <CSVLink
            data={props.datasummarycontingent}
            filename={"rekapitulasitahap1.csv"}
            //className="btn btn-primary"
            className="no-print"
            target="_blank"
            >
            <Button variant="contained" color="primary">Download CSV</Button>
        </CSVLink> */}
        <div className="no-print">
        <ExcelFile className="no-print" element={<Button variant="contained" className="no-print" color="secondary" style={{fontSize:13}}>Download XLS</Button>}>
            <ExcelSheet data={props.datasummarycontingent_byname} name="Rekapitulasi Kontingen Tahap 2">
                {/* <ExcelColumn label="Nama Kontingen" value="name"/>
                <ExcelColumn label="Disiplin Tahap 1" value="event_tahap_1"/>
                <ExcelColumn label="Nomor Pertandingan Tahap 1" value="category_tahap_1"/>
                <ExcelColumn label="Atlit Tahap 1" value="longlist_athlete"/>
                <ExcelColumn label="Official Tahap 1" value="longlist_official"/>
                <ExcelColumn label="Official Kontingen Tahap 1" value="longlist_officialkontingen"/> */}
                <ExcelColumn label="Disiplin" value="event_tahap_2"/>
                <ExcelColumn label="Nomor Pertandingan" value="category_tahap_2"/>
                <ExcelColumn label="Atlit" value="athlete"/>
                <ExcelColumn label="Official" value="official"/>
                <ExcelColumn label="Official Kontingen" value="officialkontingen"/>
            </ExcelSheet>
        </ExcelFile>
        </div>
        <table style={{ width:'100%', fontSize:12, marginBottom:10}}>
            <thead style={{background:'#bf272b', color:'white'}}>
            
                <tr>
                    <th style={{padding:8,width:30,textAlign:'center'}}>No</th>
                    <th style={{padding:8,textAlign:'left'}}>Kontingen</th>
                    <th style={{padding:8,width:80,textAlign:'center'}}>Disiplin</th>
                    <th style={{padding:8,width:80,textAlign:'center'}}>Nomor Pertandingan</th>
                    <th style={{padding:8,width:100,textAlign:'center'}}>Atlit</th>
                    <th style={{padding:8,width:100,textAlign:'center'}}>Official</th>
                    <th style={{padding:8,width:100,textAlign:'center'}}>Official Kontingen</th>
                </tr>
            </thead>
            <tbody  class="table-stripped">
                {props.datasummarycontingent_byname.map((item, index) => {
                return(

                    <tr>
                        <td style={{padding:8,width:30,textAlign:'center'}}>{index+1}</td>
                        <td style={{padding:8 }}>
                            {/* <img style={{ height: 30, width:28, float:'left', marginRight:5}} src={url+'/repo/contingent/'+item.logo} /> */}
                            <h3 style={{marginTop:5}}>{item.name}</h3>
                        </td>
                        <td style={{padding:8,width:100,textAlign:'center'}}>{item.event_tahap_2}</td>
                        <td style={{padding:8,width:100,textAlign:'center'}}>{item.category_tahap_2}</td>
                        <td style={{padding:8,width:100,textAlign:'center'}}>{item.athlete}</td>
                        <td style={{padding:8,width:100,textAlign:'center'}}>{item.official}</td>
                        <td style={{padding:8,width:100,textAlign:'center'}}>{item.officialkontingen}</td>
                    </tr>
                )
                }
                )}
            </tbody>
            <tfoot style={{background:'#bf272b', color:'white'}}>
                {
                    props.datafootercontingent.map((item, index) => {                      
                        return(
                        <tr>
                            <td colSpan={2} style={{padding:8,fontSize:12}}>Total</td>
                            <td style={{padding:8,width:100,textAlign:'center'}}>{item.event_tahap_2}</td>
                            <td style={{padding:8,width:100,textAlign:'center'}}>{item.category_tahap_2}</td>
                            <td style={{padding:8,width:100,textAlign:'center'}}>{item.athlete}</td>
                            <td style={{padding:8,width:100,textAlign:'center'}}>{item.official}</td>
                            <td style={{padding:8,width:100,textAlign:'center'}}>{item.officialkontingen}</td>
                        </tr>
                      )
                    }
                    )   
                }
            </tfoot>
        </table>
    </>
  );
}

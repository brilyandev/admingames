import React, { useState,useEffect } from "react";
import {
  Typography, Button,
  Grid,
  Select,
  MenuItem
} from "@material-ui/core";
import moment from 'moment';
import _ from 'lodash';
import StaticVar from "../../Config/StaticVar"
import Api from '../../Services/Api'
import Dialog from "../Components/Dialog/Dialog"
import axios from "axios"

export default function Tahap1CheckDukcapil() {
    const url = StaticVar.Base_Url;//"http://localhost:300";
    const token = localStorage.getItem("token");
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': token 
    };

    const [openModal, setopenModal] = useState(false)
    const [detailpeserta, setdetailpeserta] = useState([])
    const [lolosqualification, setlolosqualification] = useState([])
    const [categoryname, setcategoryname] = useState("")
    const [dataparticipant, setdataparticipant] = useState([])
    const [filterData, setfilterData] = useState("0")
    const [filterheader, setfilterheader] = useState("kontingen")
    const [contingentlist, setcontingentlist] = useState([])
    const [eventlist, seteventlist] = useState([])
    const [contingent_choose, setcontingent_choose] = useState('0')
    const [event_choose, setevent_choose] = useState('0')
    const [itemcontingent, setitemcontingent] = useState({})
    const [itemevent, setitemevent] = useState({})
    const [chooseparticipant, setchooseparticipant] = useState({})
    const [participantcount, setparticipantcount] = useState(0)
    const [cekdukcapil, setcekdukcapil] = useState({})
    const handleClose = ()=>{
        setopenModal(false)
    }
    useEffect(() => {
        // axios.get(url+"/private/contingents",{headers}).;  
        // const req2 = axios.get(url+"/private/events/get_all_event",{headers});   
        Api.getContingents().then(res=>setcontingentlist(res.data))
        Api.getAllEvents().then(res=>seteventlist(res.data))
    }, [])
    return (
        <div>
            <Dialog
                open={openModal}
                close={handleClose}
                title={chooseparticipant.full_name}
                content={
                <>
                    {/* <p>Data Pengecekan: {JSON.stringify(chooseparticipant)}</p> */}
                    <p>Load Ulang : 
                        {
                            JSON.stringify(cekdukcapil)
                        }
                    </p>
                </>
                }

                cancel={handleClose}
                valueCancel ={"Tutup"}
                // confirm={()=>postData()}
                // valueConfirm={"Simpan"}
                />
            <div class="no-print">
                <p>Filter : 
                <Select value={filterheader} onChange={(event)=>{
                    setfilterheader(event.target.value)
                    setfilterData("0")
                    setdataparticipant([])
                }} style={{marginRight:5}}>
                    <MenuItem value="kontingen">Berdasarkan Kontingen</MenuItem>
                    <MenuItem value="cabor">Berdasarkan Cabor</MenuItem>
                </Select>
                {
                    filterheader === "kontingen" ?
                    <Select value={contingent_choose} style={{marginRight:5}} onChange={(e)=>{
                        if(e.target.value !== "0"){
                            const datachoose = contingentlist.filter(x=>x._id === e.target.value)[0]
                            setitemcontingent(datachoose); 
                        }                    
                        setcontingent_choose(e.target.value)
                        setdataparticipant([])
                    }
                        
                    }>
                        <MenuItem value="0">Pilih Kontingen</MenuItem>
                        {
                            contingentlist.map((item,index)=>{
                                return(
                                    <MenuItem value={item._id}>{item.name}</MenuItem>
                                )
                            })
                        }
                    </Select> :
                    <Select value={event_choose} style={{marginRight:5}} onChange={(e)=>{
                        if(e.target.value !== "0"){
                            const datachoose = eventlist.filter(x=>x._id === e.target.value)[0]
                            setitemevent(datachoose); 
                        }
                        setevent_choose(e.target.value)
                        setdataparticipant([])
                    }}>
                        <MenuItem value="0">Pilih Disiplin</MenuItem>
                        {
                            eventlist.map((item,index)=>{
                                return(
                                    <MenuItem value={item._id}>{item.event_name}</MenuItem>
                                )
                            })
                        }
                    </Select>
                }
                <Select style={{marginRight:5}} value={filterData} onChange={(event)=>{
                    setfilterData(event.target.value)
                    // if(event.target.value !== "0"){
                    //     if(filterheader === "kontingen"){
                    //         Api.getCheckParticipant({filter:event.target.value}).then(res=>{
                    //             console.log('res', res.data)
                    //             const result = res.data.reduce((ac, a) => {
                    //                 let temp = ac.find(x => x.contingent_name === a.contingent_name);
                    //                 if (!temp) ac.push({ ...a,
                    //                     data: [a]
                    //                 })
                    //                 else {
                    //                     temp.data.push(a)
                    //                 }
                    //                 return ac;
                    //             },[]).map(x=>{return({contingent_name:x.contingent_name, contingent_logo:x.contingent_logo, data:x.data})})
                    //             console.log('result', result)
                    //             setdataparticipant(result)
                    //         })
                    //     }
                    //     else{
                    //         Api.getCheckParticipant({filter:event.target.value}).then(res=>{
                    //             console.log('res', res.data)
                    //             const result = res.data.reduce((ac, a) => {
                    //                 let temp = ac.find(x => x.event_name === a.event_name);
                    //                 if (!temp) ac.push({ ...a,
                    //                     data: [a]
                    //                 })
                    //                 else {
                    //                     temp.data.push(a)
                    //                 }
                    //                 return ac;
                    //             },[]).map(x=>{return({event_name:x.event_name, event_icon:x.event_icon, data:x.data})})
                    //             console.log('result', result)
                    //             setdataparticipant(result)
                    //         })
                    //     }
                    // }
                }}
                >
                    <MenuItem value={"0"}>Pilih Filter</MenuItem>
                    {
                        ["Cek Domisili","Data Tidak Ditemukan", "Data belum ditarik"].map((item,index)=>{
                            return(
                                <MenuItem value={item}>{item}</MenuItem>
                            )
                        })
                    }
                </Select>
                {
                    filterData !== "0"?
                    <Button size="small" variant="contained" color="secondary" onClick={()=>{
                        let filterquery = {}
                        if(filterheader === "kontingen"){
                            filterquery = {...filterquery, contingent_id : contingent_choose}
                        }
                        if(filterheader === "cabor"){
                            filterquery = {...filterquery, event_id : event_choose}
                        }
                        filterquery = {...filterquery, filter : filterData}
                        Api.getCheckParticipant(filterquery).then(res=>{
                            console.log('res', res.data)
                            setparticipantcount(res.data.length)
                            if(filterheader === "kontingen")
                            {
                                const result = res.data.reduce((ac, a) => {
                                    let temp = ac.find(x => x.event_name === a.event_name);
                                    if (!temp) ac.push({ ...a,
                                        data: [a]
                                    })
                                    else {
                                        temp.data.push(a)
                                    }
                                    return ac;
                                },[]).map(x=>{return({event_name:x.event_name, event_icon:x.event_icon, data:x.data})})
                                console.log('result', result)
                                setdataparticipant(result)
                            }
                            else{
                                const result = res.data.reduce((ac, a) => {
                                    let temp = ac.find(x => x.contingent_name === a.contingent_name);
                                    if (!temp) ac.push({ ...a,
                                        data: [a]
                                    })
                                    else {
                                        temp.data.push(a)
                                    }
                                    return ac;
                                },[]).map(x=>{return({contingent_name:x.contingent_name, contingent_logo:x.contingent_logo, data:x.data})})
                                console.log('result', result)
                                setdataparticipant(result)
                            }
                            
                        })
                        
                    }}>Load Data</Button> : null
                }
                
                </p>
            </div>
            <center><h2>{filterData === "Cek Domisili"? "Domisili Berbeda" : filterData === "Data Tidak Ditemukan" ? "Data NIK tidak ditemukan" : "Menunggu Sinkronisasi NIK"}</h2></center>
            <hr/>
            <table style={{float:'left'}}>
                <tr>
                    <td style={{padding:10}}>
                        {
                            filterheader === "kontingen"?
                            <img src={url+"/repo/contingent/"+ itemcontingent.logo} style={{height:80}}/> :
                            <img src={url+"/repo/sport/"+ itemevent.event_icon} style={{height:50}}/>
                        }
                        
                    </td>
                    <td>
                        <span style={{fontSize:20, fontWeight:'bold'}}>{filterheader === "kontingen" ? itemcontingent.name : itemevent.event_name}</span>
                    </td>
                </tr>
            </table>
            <div style={{float:'right', padding:10, marginRight:10}}>
                <table>
                    <tr>
                        <td>Jumlah {filterheader === "kontingen"? "Disiplin" : "Kontingen"}</td>
                        <td>:</td>
                        <td>{dataparticipant.length} {filterheader === "kontingen"? "Disiplin" : "Kontingen"}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Ketidaksesuaian</td>
                        <td>:</td>
                        <td>{participantcount} orang</td>
                    </tr>
                    
                </table>
            </div>
            {
                dataparticipant.map((itemheader,indexheader)=>{
                    return(
                        <div style={{marginBottom:20}}>
                            {
                                filterheader === "cabor"? 
                                <table style={{ width:'100%', backgroundColor:'#EFEFEF'}}>
                                    <tr>
                                        <td style={{width:50, padding:8}}><img src={url+"/repo/contingent/"+itemheader.contingent_logo} style={{height:40, marginRight:5}}/></td>
                                        <td style={{fontSize:18, fontWeight:'bold', textAlign:'left'}}>#{indexheader+1}. {itemheader.contingent_name}</td>
                                    </tr>
                                </table> :
                                <table style={{ width:'100%', backgroundColor:'#EFEFEF'}}>
                                    <tr>
                                        <td style={{width:50, padding:8}}><img src={url+"/repo/sport/"+itemheader.event_icon} style={{height:40, marginRight:5}}/></td>
                                        <td style={{fontSize:18, fontWeight:'bold', textAlign:'left'}}>#{indexheader+1}. {itemheader.event_name}</td>
                                    </tr>
                                </table>
                            }
                            
                            <div style={{padding:10}}></div>
                            <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                                <thead style={{background:'#bf272b', color:'white'}}>
                                    <tr>
                                        <th style={{textAlign:'center',width:20,padding:8}}>No</th>
                                        <th style={{textAlign:'left',padding:8}}>Nama Peserta</th>
                                        <th style={{textAlign:'center',width:100,padding:8}}>Jenis Kelamin</th>
                                        <th style={{textAlign:'center',width:150,padding:8, textAlign:'center'}}>Tanggal Lahir</th>
                                        <th style={{width:350,padding:8, textAlign:'left'}}>Alamat</th>
                                        
                                        {/* <th style={{textAlign:'center',width:150,padding:8}}>{filterheader === "kontingen"? "Disiplin": "Kontingen"}</th> */}
                                        <th style={{textAlign:'left',width:300,padding:8}}>Catatan</th>
                                        <th style={{textAlign:'center',width:50,padding:8}}>Lihat</th>
                                    </tr>
                                </thead>
                                <tbody class="table-stripped">
                                    {
                                        _.orderBy(itemheader.data,["event_name", "contingent_name"],["asc", "asc"]).map((item,index)=>{
                                            return(
                                                <tr>
                                                    <td style={{textAlign:'center', padding:8}}>{index+1}</td>
                                                    <td style={{ padding:8}}><b>{item.full_name}</b>
                                                        <br/>
                                                        <span>NIK : {item.ID_Number}</span>
                                                    </td>
                                                    <td style={{textAlign:'center', padding:8}}>{item.gender}</td>
                                                    <td style={{textAlign:'center', padding:8}}>
                                                        {item.place_of_birth}<br/>
                                                        {moment(item.date_of_birth).format("DD-MM-YYYY")}</td>
                                                    <td style={{textAlign:'left', padding:8}}>{item.address}</td>
                                                    {/* <td style={{textAlign:'center', padding:8}}>{filterheader === "cabor"? item.event_name: item.contingent_name}</td> */}
                                                    <td style={{textAlign:'left', padding:8}}>
                                                        {
                                                            filterData === "Cek Domisili" ?
                                                            <>
                                                                Domisili: <b>{item.dukcapil_prop_name}</b>
                                                                <br/>
                                                                Keterangan SKM :<b>{item.SKM ? item.SKM : "-"}</b>
                                                            </>:
                                                            filterData === "Data Tidak Ditemukan" ? 
                                                            item.note: 
                                                            "-"
                                                        }
                                                        
                                                    </td>
                                                    <td style={{textAlign:'center'}}>
                                                        <Button size="small" variant="contained" color="secondary" 
                                                            onClick={()=>{
                                                                setopenModal(true); 
                                                                setchooseparticipant(item);
                                                                axios.get(url+'/private/dukcapils/NIK/'+item.ID_Number, {headers}).then(res=>{
                                                                    console.log('dukcapils', res.data)
                                                                    setcekdukcapil(res.data)
                                                                })
                                                            }}>Lihat</Button>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                            <hr/>
                        </div>
                    )
                })
            }
            
        </div>
    )
}

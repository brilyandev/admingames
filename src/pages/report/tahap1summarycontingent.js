import React, { useState,useEffect } from "react";
import {
  Grid, 
  Paper,
  Avatar, 
  Typography,
  Button,
  TextField,
  InputLabel, 
  Select,
  MenuItem, 
} from "@material-ui/core";
import axios from "axios";
import moment from 'moment';
import _ from 'lodash';
import { CSVLink } from "react-csv";
import ReactExport from "react-export-excel";
import StaticVar from "../../Config/StaticVar"

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function Tahap1SummaryContingent(props) {
  // global
  //var userDispatch = useUserDispatch();  
  const url = StaticVar.Base_Url;//"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token 
  };


  useEffect(()=>{
  },[])
  
  return (
    <>

        {/* <CSVLink
            data={props.datasummarycontingent}
            filename={"rekapitulasitahap1.csv"}
            //className="btn btn-primary"
            className="no-print"
            target="_blank"
            >
            <Button variant="contained" color="primary">Download CSV</Button>
        </CSVLink> */}
        <div className="no-print">
        <ExcelFile className="no-print" element={<Button variant="contained" className="no-print" color="secondary" style={{fontSize:13}}>Download XLS</Button>}>
            <ExcelSheet data={props.datasummarycontingent} name="Statistik Kontingen">
                <ExcelColumn label="Nama Kontingen" value="name"/>
                <ExcelColumn label="Jumlah Disiplin" value="entrie_by_number"/>
                <ExcelColumn label="Jumlah Nomor" value="number_of_event"/>
                <ExcelColumn label="Atlit Putra" value="athlete_count_male"/>
                <ExcelColumn label="Atlit Putri" value="athlete_count_female"/>
                <ExcelColumn label="Atlit" value="athlete_count"/>
                <ExcelColumn label="Official Putra" value="official_count_male"/>
                <ExcelColumn label="Official Putri" value="official_count_female"/>
                <ExcelColumn label="Official" value="official_count"/>
                <ExcelColumn label="Official Kontingen Putra" value="official_kontingen_count_male"/>
                <ExcelColumn label="Official Kontingen Putri" value="official_kontingen_count_female"/>
                <ExcelColumn label="Official Kontingen" value="official_kontingen_count"/>
                <ExcelColumn label="Longlist Putra" value="longlist_male"/>
                <ExcelColumn label="Longlist Putri" value="longlist_female"/>
                <ExcelColumn label="Longlist Atlit dan Official" value="longlist"/>
            </ExcelSheet>
        </ExcelFile>
        </div>
        <table style={{ width:'100%', fontSize:12, marginBottom:10}}>
            <thead style={{background:'#bf272b', color:'white'}}>
            
                <tr>
                    <th rowSpan={2} style={{padding:8,width:30,textAlign:'center'}}>No</th>
                    <th rowSpan={2} style={{padding:8,textAlign:'left'}}>Kontingen</th>
                    <th colSpan={2} style={{padding:8,textAlign:'center'}}>Disiplin</th>
                    <th colSpan={3} style={{padding:8,textAlign:'center'}}>Longlist Atlit</th>
                    <th colSpan={3} style={{padding:8,textAlign:'center'}}>Longlist Official</th>
                    <th colSpan={3} style={{padding:8,textAlign:'center'}}>Longlist Official Kontingen</th>
                    <th colSpan={3} style={{padding:8,textAlign:'center'}}>Longlist (Atlit & Official)</th>
                </tr>
                <tr>
                    <th style={{padding:8,width:80,textAlign:'center'}}>Disiplin</th>
                    <th style={{padding:8,width:80,textAlign:'center'}}>Nomor Pertandingan</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Putra</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Putri</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Total</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Putra</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Putri</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Total</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Putra</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Putri</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Total</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Putra</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Putri</th>
                    <th style={{padding:8,width:60,textAlign:'center'}}>Total</th>
                </tr>
            </thead>
            <tbody  class="table-stripped">
                {props.datasummarycontingent.map((item, index) => {
                return(

                    <tr>
                        <td style={{padding:8,width:30,textAlign:'center'}}>{index+1}</td>
                        <td style={{padding:8 }}>
                            <img style={{ height: 30, width:28, float:'left', marginRight:5}} src={url+'/repo/contingent/'+item.logo} />
                            <h3 style={{marginTop:5}}>{item.name}</h3>
                        </td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.entrie_by_number}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>
                            {
                                item.number_of_event
                            }
                        </td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_male}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_female}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_male+item.athlete_count_female}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_count_male}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_count_female}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_count_male+item.official_count_female}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_kontingen_count_male}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_kontingen_count_female}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_kontingen_count_male+item.official_kontingen_count_female}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_male+item.
                                                                            official_count_male+item.
                                                                            official_kontingen_count_male}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_female+
                                                                            item.official_count_female+
                                                                            item.official_kontingen_count_female}</td>
                        <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_male+
                                                                            item.official_count_male+
                                                                            item.official_kontingen_count_male+
                                                                            item.athlete_count_female+
                                                                            item.official_count_female+
                                                                            item.official_kontingen_count_female}</td>
                    </tr>
                )
                }
                )}
            </tbody>
            <tfoot style={{background:'#bf272b', color:'white'}}>
                {
                    props.datafootercontingent.map((item, index) => {                      
                        return(
                        <tr>
                            <td colSpan={2} style={{padding:8,fontSize:12}}>Total</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.entrie_by_numbers}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.number_of_event}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_male}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_female}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_male+item.athlete_count_female}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_count_male}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_count_female}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_count_male+item.official_count_female}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_kontingen_count_male}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_kontingen_count_female}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.official_kontingen_count_male+item.official_kontingen_count_female}</td>

                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_male+item.official_count_male+item.official_kontingen_count_male}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_female+item.official_count_female+item.official_kontingen_count_female}</td>
                            <td style={{padding:8,width:60,textAlign:'center'}}>{item.athlete_count_male+
                                                                                item.official_count_male+
                                                                                item.official_kontingen_count_male+
                                                                                item.athlete_count_female+
                                                                                item.official_count_female+
                                                                                item.official_kontingen_count_female}</td>
                        </tr>
                      )
                    }
                    )   
                }
            </tfoot>
        </table>
    </>
  );
}

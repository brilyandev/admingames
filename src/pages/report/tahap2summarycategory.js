import React, { useState,useEffect } from "react";
import {
  Typography, Button,
  Grid
} from "@material-ui/core";
import moment from 'moment';
import _ from 'lodash';
import StaticVar from "../../Config/StaticVar"
import Dialog from "../Components/Dialog/Dialog"
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function Tahap2SummaryCategory(props) {
  // global
  //var userDispatch = useUserDispatch();  
  const url = StaticVar.Base_Url;//"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token 
  };

  const [openModal, setopenModal] = useState(false)
  const [detailpeserta, setdetailpeserta] = useState([])
  const [lolosqualification, setlolosqualification] = useState([])
  const [categoryname, setcategoryname] = useState("")
  const [category_team, setcategory_team] = useState(false)
  const handleClose = ()=>{
      setopenModal(false)
  }

  return (
    <>
        <Dialog
        open={openModal}
        close={handleClose}
        title={categoryname}
        
        content={
          <>
            <table style={{width:'100%', fontSize:12, marginBottom:10}}>
                <thead style={{background:'#bf272b', color:'white'}}>
                    <tr>
                        <th style={{width:60,padding:8}}>No</th>
                        <th style={{width:420,textAlign:'left',padding:8}}>Nama Kontingen</th>
                        <th style={{width:220,padding:8, textAlign:'center'}}>Entries</th>
                    </tr>
                </thead>
                <tbody >
                    {
                        detailpeserta.map((item,index)=>{
                            const datacontingent = props.datacontingent.filter(x=>x._id === item.contingent_id)
                            let group_list = _.uniqBy(item.participant, "group_id");
                            return(
                                <>
                                <tr style={{backgroundColor:"#EFEFEF"}}>
                                    <td style={{width:20,padding:8}}>{index+1}</td>
                                    <td style={{textAlign:'left',padding:8}}>{datacontingent[0].name}</td>
                                    <td style={{textAlign:'center'}}>{item.count}</td>
                                </tr>
                                {
                                    category_team ? 
                                    group_list.map((itemgroup, indexgroup)=>{
                                        return(
                                            <>
                                                <tr>
                                                    <td colSpan={3} style={{textAlign:'left', backgroundColor:"#CFCFCF"}}>Tim - {indexgroup+1}</td>
                                                </tr>
                                                {
                                                    item.participant.filter(filter=>filter.group_id === itemgroup.group_id).map((itemparticipant,indexparticipant)=>{
                                                        return(
                                                            <>
                                                                <tr>
                                                                    <td style={{width:20,padding:8}}></td>
                                                                    <td style={{textAlign:'left',padding:8}}>{itemparticipant.full_name}</td>
                                                                    <td></td>
                                                                </tr>
                                                            </>
                                                        )
                                                    })
                                                }
                                            </>
                                        )
                                    })
                                    
                                    :
                                    item.participant ? item.participant.map((itemparticipant,indexparticipant)=>{
                                        return(
                                            <tr>
                                                <td style={{width:20,padding:8}}></td>
                                                <td style={{textAlign:'left',padding:8}}>{itemparticipant.full_name}</td>
                                                <td></td>
                                            </tr>
                                        )
                                    }) : null
                                }
                                 <tr style={{height:20}}>
                                    <td colSpan="3" style={{borderTop:"1px solid #CFCFCF"}}></td>
                                </tr>
                                </>
                            )
                        })
                    }
                </tbody>
            </table>
             
          </>
        }

        cancel={handleClose}
        valueCancel ={"Tutup"}
        // confirm={()=>postData()}
        // valueConfirm={"Simpan"}
        />
        <table style={{width:'100%', fontSize:12, marginBottom:10}}>
            <thead style={{background:'#bf272b', color:'white'}}>
                <tr>
                    <th style={{width:20,padding:8}}>No</th>
                    <th style={{textAlign:'left',padding:8}}>Nomor Pertandingan</th>
                    <th style={{width:180,padding:8}}>Kontingen</th>
                    <th style={{width:180,padding:8}}>Entrie</th>
                    <th style={{width:100,padding:8}}>Detail</th>
                </tr>
            </thead>
            <tbody  class="table-stripped">
                {
                    props.datacategory.map((item,index)=>{
                        var individual_entrie = props.entrie_individual.filter(x=>x.category_id === item._id)
                        var group_entrie = props.entrie_group.filter(x=>x.category_id === item._id)
                        return(
                            <>
                                <tr>
                                    <td style={{width:20,padding:8}}>{index+1}</td>
                                    <td style={{textAlign:'left',padding:8}}>
                                        <b style={{fontSize:14}}>
                                           {item.event_name} - {item.category_name}</b>
                                        <br/>
                                        <small>Babak Kualifikasi : {item.event_qualifications}</small>
                                    </td>
                                    <td style={{padding:8, textAlign:'center'}}>
                                        {
                                            item.category_team ? group_entrie.length : individual_entrie.length
                                        }
                                    </td>
                                    {
                                        item.category_team ? 
                                        <td style={{padding:8, textAlign:'center'}}>{_.sumBy(group_entrie,"count")}</td>:
                                        <td style={{padding:8, textAlign:'center'}}>{_.sumBy(individual_entrie,"count")}</td>
                                    }
                                    <td style={{textAlign:'center'}}>
                                        <Button variant="contained" color="secondary" onClick={()=> {setopenModal(true); setcategoryname(item.event_name+ " - "+ item.category_name); 
                                        if(item.category_team){
                                            setdetailpeserta(group_entrie)
                                            setcategory_team(true)
                                        }
                                        else{
                                            setdetailpeserta(individual_entrie)
                                        }
                                        }}>Lihat</Button>
                                    </td>
                                </tr>
                                {
                                    props.pagenumber == 5 ?
                                    <tr>
                                        <td></td>
                                        <td colSpan="4" style={{padding:3}}>
                                            {
                                                item.category_team ? 
                                                group_entrie.map(itemparticipant=>{
                                                    return props.datacontingent.filter(x=>x._id === itemparticipant.contingent_id).map(contingent_profile=>
                                                    {
                                                        return(
                                                            <>
                                                            <div style={{float:'left', marginRight:10, textAlign:'center'}} >
                                                                <img
                                                                style={{ height: 30}}
                                                                src={url+'/repo/contingent/'+ contingent_profile.logo}
                                                                />
                                                                <br/>
                                                                <small>
                                                                    {contingent_profile.initial}
                                                                </small>
                                                            </div>
                                                            </>
                                                        )
                                                    })
                                                    
                                                }) 
                                                : 
                                                individual_entrie.map(itemparticipant=>{
                                                    return props.datacontingent.filter(x=>x._id === itemparticipant.contingent_id).map(contingent_profile=>
                                                        {
                                                            return(
                                                                <div style={{float:'left', marginRight:10, textAlign:'center'}} >
                                                                    <img
                                                                    style={{ height: 30}}
                                                                    src={url+'/repo/contingent/'+ contingent_profile.logo}
                                                                    />
                                                                    <br/>
                                                                    <small>
                                                                        {contingent_profile.initial}
                                                                    </small>
                                                                </div>
                                                            )
                                                        })
                                                })
                                            }
                                        </td>
                                    </tr> : null
                                }
                                
                            </>    
                        )
                    })
                }
                
            </tbody>
        </table>
        <ExcelFile className="no-print" element={<Button variant="contained" className="no-print" color="secondary" style={{fontSize:13}}>Download XLS</Button>}>
            <ExcelSheet data={props.datacategory.map(x=>{
                let entrie = 0
                let contingent = 0
                if(x.category_team){
                    entrie = _.sumBy(props.entrie_group.filter(y=>y.category_id === x._id),"count")
                    contingent = props.entrie_group.filter(y=>y.category_id === x._id).length
                }
                else{
                    entrie = _.sumBy(props.entrie_individual.filter(y=>y.category_id === x._id),"count")
                    contingent = props.entrie_individual.filter(y=>y.category_id === x._id).length
                }

                return(
                    {
                        event_name:x.event_name,
                        category_name: x.category_name,
                        entrie,
                        contingent,                        
                    }
                )
            })} name="SummaryCategory">
                <ExcelColumn label="Nama Disiplin" value="event_name"/>
                <ExcelColumn label="Nomor Pertandingan" value="category_name"/>
                <ExcelColumn label="Kontingen" value="contingent"/>
                <ExcelColumn label="Entrie" value="entrie"/>
            </ExcelSheet>
            <ExcelSheet data={props.datacategory.map(x=>{
                if(x.category_team){
                    return props.entrie_group.map(y=>{
                        return(
                            {
                                event_name:x.event_name,
                                category_name:x.category_name,
                                contingent:y.contingent
                            }                       
                        )
                    })
                }
                else{
                    return props.entrie_individual.map(y=>{
                        return(
                            {
                                event_name:x.event_name,
                                category_name:x.category_name,
                                contingent:y.contingent
                            }                       
                        )
                    })
                }
            })}  name="Raw Data Category">
                <ExcelColumn label="Nama Disiplin" value="event_name"/>
                <ExcelColumn label="Nomor Pertandingan" value="category_name"/>
                <ExcelColumn label="Nama" value="full_name"/>
                <ExcelColumn label="Kontingen" value="contingent"/>
            </ExcelSheet>
        </ExcelFile>
    </>
  );
}

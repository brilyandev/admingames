import React from "react";
import {
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
} from "@material-ui/core";

// components
import { Button } from "../../../../components/Wrappers";

const states = {
  sent: "success",
  pending: "warning",
  declined: "secondary",
};

export default function TableComponent({ data }) {
  var keys = Object.keys(data[0]).map(i => i.toUpperCase());
  keys.shift(); // delete "id" key

  return (
    <Table className="mb-0">
      <TableHead>
        <TableRow>
          <TableCell style={{width:20}}>No</TableCell>
          <TableCell>Keterangan</TableCell>
          <TableCell style={{width:180}}>Mulai</TableCell>
          <TableCell style={{width:180}}>Selesai</TableCell>
          <TableCell style={{width:180}}>Status</TableCell>
          <TableCell style={{width:120}}>Aksi</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
          <TableRow>
            <TableCell>1</TableCell>
            <TableCell>Entri Tahap I (By Team)</TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell>
              <Button color={"primary"} size="small" className="px-2"variant="contained" >Ubah</Button>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>2</TableCell>
            <TableCell>Entri Tahap II (By Number)</TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell>
              <Button color={"primary"} size="small" className="px-2"variant="contained" >Ubah</Button>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>3</TableCell>
            <TableCell>Entri Tahap III (By Name)</TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell>
              <Button color={"primary"} size="small" className="px-2"variant="contained" >Ubah</Button>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>4</TableCell>
            <TableCell>Keabsahan Atlit</TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell>
              <Button color={"primary"} size="small" className="px-2"variant="contained" >Ubah</Button>
            </TableCell>
          </TableRow>
      </TableBody>
    </Table>
  );
}

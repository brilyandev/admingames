import React, { useState, useEffect } from "react";
import {
  Grid,
  Button, 
  TextField,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell
} from "@material-ui/core";
import { useTheme } from "@material-ui/styles";
import axios from "axios";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
// styles
import useStyles from "./styles";
import moment from 'moment';

// components
import Widget from "../../components/Widget";
import { Typography } from "../../components/Wrappers";
import logo from "../../Assets/Images/logo.png";
import Dialog from "../Components/Dialog/Dialog";

import Api from '../../Services/Api'

export default function Dashboard(props) {
  var classes = useStyles();
  var theme = useTheme();

  var userDispatch = useUserDispatch();  

  const [open, setOpen] = useState(false);

  const [startdate, setstartdate] = useState(moment(new Date).format("DD-MM-YYYY"));
  const [enddate, setenddate] = useState(moment(new Date).format("DD-MM-YYYY"));
  const [form_data, setform_data] = useState("");

  const [contingent, setcontingent] = useState(0);
  const [disciplin, setdisciplin] = useState(0);
  const [venue, setvenue] = useState(0);
  const [user, setuser] = useState(0);

  const [data, setdata] = useState({
    "schedules": {
        "entrie_1": {
            "startdate": "",
            "enddate": ""
        },
        "entrie_2": {
            "startdate": "",
            "enddate": ""
        },
        "entrie_3": {
            "startdate": "",
            "enddate": ""
        },
        "accreditation": {
            "startdate": "",
            "enddate": ""
        }
    },
    "_id": "",
    "event_name": ""
  });

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(()=>{

    Api.getEvents().then(res=>setdisciplin(res.data.length))
    Api.getContingents().then(res=>setcontingent(res.data.length))
    Api.getUsers().then(res=>setuser(res.data.length))
    Api.getVenues().then(res=>setvenue(res.data.length))
    Api.getEntrieSchedule().then(res=>setdata(res.data))
  },[])

  function postData(){
    if(form_data === "Entrie Tahap 1"){
      var datatemp = data.schedules;
      var dataput = {
        schedules : {
          entrie_1 :{
            startdate : moment(startdate,"DD-MM-YYYY").format("YYYY-MM-DD"),
            enddate : moment(enddate,"DD-MM-YYYY").format("YYYY-MM-DD")
          },
          entrie_2 :{
            startdate : datatemp.entrie_2.startdate,
            enddate : datatemp.entrie_2.enddate
          },
          entrie_3 :{
            startdate : datatemp.entrie_3.startdate,
            enddate : datatemp.entrie_3.enddate
          },
          accreditation :{
            startdate : datatemp.accreditation.startdate,
            enddate : datatemp.accreditation.enddate
          },
        }
      }
      console.log(dataput)
      Api.putEntrieSchedule(data._id, dataput).then(res=>{
        setdata(res.data);
        setOpen(false);
      })
      
    }
    if(form_data === "Entrie Tahap 2"){
      var datatemp = data.schedules;
      Api.putEntrieSchedule(data._id, 
        {
          schedules : {
            entrie_2 :{
              startdate : moment(startdate,"DD-MM-YYYY").format("YYYY-MM-DD"),
              enddate : moment(enddate,"DD-MM-YYYY").format("YYYY-MM-DD")
            },
            entrie_1 :{
              startdate : datatemp.entrie_1.startdate,
              enddate : datatemp.entrie_1.enddate
            },
            entrie_3 :{
              startdate : datatemp.entrie_3.startdate,
              enddate : datatemp.entrie_3.enddate
            },
            accreditation :{
              startdate : datatemp.accreditation.startdate,
              enddate : datatemp.accreditation.enddate
            },
          }
        }
        ).then(res=>{
        setdata(res.data);
        setOpen(false);
      })
    }
    if(form_data === "Entrie Tahap 3"){
      var datatemp = data.schedules;
      Api.putEntrieSchedule(data._id, {
        schedules : {
          entrie_3 :{
            startdate : moment(startdate,"DD-MM-YYYY").format("YYYY-MM-DD"),
            enddate : moment(enddate,"DD-MM-YYYY").format("YYYY-MM-DD")
          },
          entrie_2 :{
            startdate : datatemp.entrie_2.startdate,
            enddate : datatemp.entrie_2.enddate
          },
          entrie_1 :{
            startdate : datatemp.entrie_1.startdate,
            enddate : datatemp.entrie_1.enddate
          },
          accreditation :{
            startdate : datatemp.accreditation.startdate,
            enddate : datatemp.accreditation.enddate
          },
        }
      }).then(res=>{
        setdata(res.data);
        setOpen(false);
      })
      
    }
    if(form_data === "Akreditasi"){
      var datatemp = data.schedules;
      Api.putEntrieSchedule(data._id, {
        schedules : {
          entrie_1 :{
            startdate : datatemp.entrie_1.startdate,
            enddate : datatemp.entrie_1.enddate
          },
          entrie_2 :{
            startdate : datatemp.entrie_2.startdate,
            enddate : datatemp.entrie_2.enddate
          },
          entrie_3 :{
            startdate : datatemp.entrie_3.startdate,
            enddate : datatemp.entrie_3.enddate
          },
          accreditation :{
            startdate : moment(startdate,"DD-MM-YYYY").format("YYYY-MM-DD"),
            enddate : moment(enddate,"DD-MM-YYYY").format("YYYY-MM-DD")
          },
        }
      }).then(res=>{
        setdata(res.data);
        setOpen(false);
      })
    }
    
  }

  // const handleDateChange = date => {
  //   setstartdate(date);
  // };
  const today = new Date;
  return (
    <>
      <Dialog
        open={open}
        close={handleClose}
        title={"Ubah Data" + form_data}
        content={
            <Grid container spacing={1}>
              <Grid item   sm={12}>
                <TextField
                  label="Tgl Mulai"
                  className={classes.formContainer}
                  margin="normal"
                  value={startdate}
                  onChange={(event)=> setstartdate(event.target.value)}
                />
              </Grid>
              <Grid item   sm={12}>
                <TextField
                  label="Tgl Selesai"
                  className={classes.formContainer}
                  margin="normal"
                  value={enddate}
                  onChange={(event)=> setenddate(event.target.value)}
                />
              </Grid>
            </Grid>
          
        }

        cancel={handleClose}
        valueCancel ={"Batal"}
        confirm={()=>postData()}
        valueConfirm={"Simpan"}
      />

      <Grid container spacing={4}>
        <Grid item lg={3} md={3} xs={6}>
          <img src={logo} style={{width:'100%'}}/>
        </Grid>
        <Grid item lg={9} md={9} xs={6}>
          <Typography size="xxl" weight="medium">PON XX 2021 Papua</Typography>
          <Typography size="md" weight="medium">Tgl Pembukaan : 20 Oktober 2020</Typography>
          <Typography size="md" weight="medium">Tgl Penutupan : 02 November 2020</Typography>
        </Grid>
      </Grid>
      <Grid container spacing={4}>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <Widget
            title="Kontingen"
            upperTitle
            bodyClass={classes.fullHeightBody}
            className={classes.card}
          >
            <div className={classes.visitsNumberContainer}>
              <Typography size="xl" weight="medium">
                { contingent}
              </Typography>
            </div>
          </Widget>
        </Grid>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <Widget
            title="Disiplin "
            upperTitle
            bodyClass={classes.fullHeightBody}
            className={classes.card}
          >
            <div className={classes.visitsNumberContainer}>
              <Typography size="xl" weight="medium">
              { disciplin}
              </Typography>
            </div>
          </Widget>
        </Grid>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <Widget
            title="Venue"
            upperTitle
            bodyClass={classes.fullHeightBody}
            className={classes.card}
          >
            <div className={classes.visitsNumberContainer}>
              <Typography size="xl" weight="medium">
              { venue}
              </Typography>
            </div>
          </Widget>
        </Grid>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <Widget
            title="User"
            upperTitle
            bodyClass={classes.fullHeightBody}
            className={classes.card}
          >
            <div className={classes.visitsNumberContainer}>
              <Typography size="xl" weight="medium">
              { user}
              </Typography>
            </div>
          </Widget>
        </Grid>
        
        
        <Grid item xs={12}>
          <Widget
            title="Jadwal Pendaftaran dan Akreditasi"
            
          >
            <Table className="mb-0">
              <TableHead>
                <TableRow>
                  <TableCell style={{width:20}}>No</TableCell>
                  <TableCell>Keterangan</TableCell>
                  <TableCell style={{width:180}}>Mulai</TableCell>
                  <TableCell style={{width:180}}>Selesai</TableCell>
                  <TableCell style={{width:180}}>Status</TableCell>
                  {
                    localStorage.user_name === "Admin" ? 
                    <TableCell style={{width:120}}>Aksi</TableCell> : null
                  }
                  
                </TableRow>
              </TableHead>
              <TableBody>
                  <TableRow>
                    <TableCell>1</TableCell>
                    <TableCell>Entri Tahap I (By Team)</TableCell>
                    <TableCell>
                      {
                        moment(data.schedules.entrie_1.startdate).format("DD-MM-YYYY")
                      }
                    </TableCell>
                    <TableCell>
                      {
                        moment(data.schedules.entrie_1.enddate).format("DD-MM-YYYY")
                      }
                    </TableCell>
                    <TableCell>
                      {
                        new Date(data.schedules.entrie_1.startdate) < today && today < new Date(data.schedules.entrie_1.enddate) ?
                        "Aktif" : 
                        new Date(data.schedules.entrie_1.startdate) < today && today > new Date(data.schedules.entrie_1.enddate) ? "Kadaluarsa" : "Belum dijadwalkan"
                      }
                    </TableCell>
                    {
                      localStorage.user_name === "Admin" ? 
                      <TableCell>
                        <Button color={"primary"} size="small" className="px-2"variant="contained" onClick={()=>{setOpen(true);setform_data("Entrie Tahap 1")}}  >Ubah</Button>
                      </TableCell> : null
                    }
                    
                  </TableRow>
                  <TableRow>
                    <TableCell>2</TableCell>
                    <TableCell>Entri Tahap II (By Number)</TableCell>
                    <TableCell>
                      {
                        moment(data.schedules.entrie_2.startdate).format("DD-MM-YYYY")
                      }
                    </TableCell>
                    <TableCell>
                      {
                        moment(data.schedules.entrie_2.enddate).format("DD-MM-YYYY")
                      }
                    </TableCell>
                    <TableCell>
                      {
                        new Date(data.schedules.entrie_2.startdate) < today && today < new Date(data.schedules.entrie_2.enddate) ?
                        "Aktif" : 
                        new Date(data.schedules.entrie_2.startdate) < today && today > new Date(data.schedules.entrie_2.enddate) ? "Kadaluarsa" : "Belum dijadwalkan"
                      }
                    </TableCell>
                    {
                      localStorage.user_name === "Admin" ? 
                      <TableCell>
                        <Button color={"primary"} size="small" className="px-2"variant="contained" onClick={()=>{setOpen(true);setform_data("Entrie Tahap 2")}}  >Ubah</Button>
                      </TableCell> : null
                    }
                    
                  </TableRow>
                  <TableRow>
                    <TableCell>3</TableCell>
                    <TableCell>Entri Tahap III (By Name)</TableCell>
                    <TableCell>
                      {
                        moment(data.schedules.entrie_3.startdate).format("DD-MM-YYYY")
                      }
                    </TableCell>
                    <TableCell>
                      {
                        moment(data.schedules.entrie_3.enddate).format("DD-MM-YYYY")
                      }
                    </TableCell>
                    <TableCell>
                      {
                        new Date(data.schedules.entrie_3.startdate) < today && today < new Date(data.schedules.entrie_3.enddate) ?
                        "Aktif" : 
                        new Date(data.schedules.entrie_3.startdate) < today && today > new Date(data.schedules.entrie_3.enddate) ? "Kadaluarsa" : "Belum dijadwalkan"
                      }
                    </TableCell>
                    {
                      localStorage.user_name === "Admin" ? 
                      <TableCell>
                        <Button color={"primary"} size="small" className="px-2"variant="contained" onClick={()=>{setOpen(true);setform_data("Entrie Tahap 3")}}  >Ubah</Button>
                      </TableCell> : null
                    }
                    
                  </TableRow>
                  <TableRow>
                    <TableCell>4</TableCell>
                    <TableCell>Keabsahan Atlit</TableCell>
                    <TableCell>
                      {
                        moment(data.schedules.accreditation.startdate).format("DD-MM-YYYY")
                      }
                    </TableCell>
                    <TableCell>
                      {
                        moment(data.schedules.accreditation.enddate).format("DD-MM-YYYY")
                      }
                    </TableCell>
                    <TableCell>
                      {
                        new Date(data.schedules.accreditation.startdate) < today && today < new Date(data.schedules.accreditation.enddate) ?
                        "Aktif" : 
                        new Date(data.schedules.accreditation.startdate) < today && today > new Date(data.schedules.accreditation.enddate) ? "Kadaluarsa" : "Belum dijadwalkan"
                      }
                    </TableCell>
                    {
                      localStorage.user_name === "Admin" ? 
                      <TableCell>
                        <Button color={"primary"} size="small" className="px-2"variant="contained" onClick={()=>{setOpen(true);setform_data("Akreditasi")}} >Ubah</Button>
                      </TableCell> : null
                    }
                    
                  </TableRow>
              </TableBody>
            </Table>
          </Widget>
        </Grid>
      </Grid>
    </>
  );
}

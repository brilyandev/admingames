import React, { useState,useEffect } from "react";
import {
  Grid, 
  Paper,
  Avatar, 
  Typography,
  Button,
  TextField,
  InputLabel, 
  Select,
  MenuItem, 
} from "@material-ui/core";
 
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';  

// styles
import useStyles from "./styles";
import MUIDataTable from "mui-datatables";

import PageTitle from "../Components/PageTitle/PageTitle";
import axios from "axios";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import StaticVar from "../../Config/StaticVar"

export default function Users(props) {
  // global
  var userDispatch = useUserDispatch();  
  var classes = useStyles();
  //"http://localhost:300"
  const url = StaticVar.Base_Url;//"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token 
  };
  var loadDataAwal=async() => {
    console.log('awal');
    try{
      const req2 = axios.get(url+"/private/contingents",{headers});
        const req3 = axios.get(url+"/private/events/get_all_event",{headers}); 
        
            axios.all([req2,req3]).then(axios.spread((...responses) => {
              const response1 = responses[0]
              const response2 = responses[1]  
                setcontingents(response1.data)
                setevents(response2.data)
             // }
            })).catch(errors => {
              console.log('error',errors)
              //signExpired(userDispatch,props.history)
            })
           
    }
    
    catch(error){
      signExpired(userDispatch,props.history);
    }
  }
  var getData = async() => {
    
    console.log('data');
    try{ 
        if(localStorage.getItem("user_access")==="admin")
          { 
            const req1 = axios.get(url+"/private/users",{headers});
            axios.all([req1]).then(axios.spread((...responses) => {
              const response1 = responses[0] 
          
                
                setRows(response1.data) 
             // }
            })).catch(errors => {
              console.log('error',errors)
              //signExpired(userDispatch,props.history)
            })
          }
          else if(localStorage.getItem("user_access")==="Sekretariat PB PON")
          {
            const req1 = axios.get(url+"/private/users/getAllUserKontingen",{headers}); 
            axios.all([req1 ]).then(axios.spread((...responses) => {
              const response1 = responses[0] 
          
                setRows(response1.data)  
            })).catch(errors => {
              console.log('error',errors) 
            })
             
          } 
    }
    
    catch(error){
      signExpired(userDispatch,props.history);
    }
  }
  
  const [open, setOpen] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [openModalReset, setOpenModalReset] = useState(false);
 
  const [action, setaction] = useState("")
  const [username, setusername] = useState([])
  var [iduser, setiduser] = useState("");
  var [imgPreview, setImgPreview ] = useState(NoImg);
  // var [imgEditPreview, setImgEditPreview ] = useState();
  var [img, setImg ] = useState("");
  var [isImgValid, setIsImgValid ] = useState("");
  var [imgErrorMsg, setImgErrorMsg ] = useState("");
  const [events, setevents] = useState([])
  const [contingents, setcontingents] = useState([])
  
  useEffect(() => { 
    getData();
    return () => {
      getData();
    } 
  }, []);
   
  useEffect(() => { 
    loadDataAwal(); 
    return () => {
      loadDataAwal();
    } 
  }, []);

  var [reset_password, setreset_password] = useState("ponxx2020papua");

  
  var [form_name, setform_name] = useState("");
  var [form_gender, setform_gender] = useState("");
  var [form_telp, setform_telp] = useState("");
  var [form_address, setform_address] = useState("");
  var [form_photo, setform_photo] = useState("");
  var [form_email, setform_email] = useState("");
  var [form_password, setform_password] = useState("ponxx2020papua");
  var [form_user_access, setform_user_access] = useState("");
  var [form_contingent_id, setform_contingent_id] = useState("0");
  var [form_event_id, setform_event_id] = useState("0");

  const handleClickOpen = () => {
    setaction("add");
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenModalDelete(false);
    setOpenModalReset(false);
    setOpenAlert(false);
  };

  function handleClickOpenDelete(id,name) {
    setiduser(id);
    setusername(name);
    setOpenModalDelete(true);
    
  };
  function handleClickOpenReset(id,name) {
    setiduser(id);
    setusername(name);
    setOpenModalReset(true);
    
  };

  function handleClickOpenEdit(id) {
    setiduser(rows[id]._id);
    setOpen(true);
    setaction("edit");
    setform_name(rows[id].name)
    setform_gender(rows[id].gender)
    setform_telp(rows[id].telp)
    setform_address(rows[id].address)
    setform_photo(rows[id].photo)
    setform_email(rows[id].email)
    setform_user_access(rows[id].user_access);
    setImgPreview(url+"/repo/"+rows[id].photo);
 
  };

  function handleImage(e){
    let reader = new FileReader();
    let file = e.target.files[0],
      pattern = /image-*/;

    if (!file.type.match(pattern)) {
      setIsImgValid(true)
      setImgErrorMsg("Format File tidak sesuai")
      return;
    }

    reader.onloadend = () => {
      setIsImgValid(false);
      setImgErrorMsg("");
      setImg(file);
      setImgPreview(reader.result)
    };

    reader.readAsDataURL(file);
  }

  let sendData = new FormData();
  sendData.append('name', form_name);
  sendData.append('gender', form_gender);
  sendData.append('telp', form_telp);
  sendData.append('address', form_address);
  sendData.append('photo', img);
  sendData.append('email', form_email);
  sendData.append('password', form_password);
  sendData.append('user_access', form_user_access);
  sendData.append('contingent_id', form_contingent_id);
  sendData.append('event_id', form_event_id);

  let sendDataEdit = new FormData();
  sendDataEdit.append('name', form_name);
  sendDataEdit.append('gender', form_gender);
  sendDataEdit.append('telp', form_telp);
  sendDataEdit.append('address', form_address);
  sendDataEdit.append('photo', img);
  sendDataEdit.append('email', form_email);
  sendDataEdit.append('user_access', form_user_access);
  sendDataEdit.append('contingent_id', form_contingent_id);
  sendDataEdit.append('event_id', form_event_id);

  function postData(){
    if(action === "add"){
      axios.post(url+"/private/users/register", sendData, {headers}).then(res=> {
        console.log('res', res.data)
        getData();
        setOpen(false);
      })
    }
    if(action === "edit"){
      axios.put(url+"/private/users/"+iduser,sendDataEdit,{headers}).then(res=> {
        getData();
        setOpen(false);
      })
    }
    
  }

  function resetPassword(id) {
    axios.put(url+"/private/users/"+iduser,{ password:reset_password},{headers}).then(res=> {
      setOpenModalReset(false); 
      getData();
    })
  }
  function deleteData(id) {
    axios.delete(url+"/private/users/"+id,{headers}).then(res=> {
      setOpenModalDelete(false);
      getData();
    })
  }

  /** Table Pagination */ 
  const [rows, setRows] = useState([]);

  const [openAlert,setOpenAlert] = useState(false);
    const [hasError, sethasError] = useState(false);
    var [errorType, seterrorType] = useState("");
  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  
  const columns = [
    {
      name: "No",
      options: { 
        print: false,
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <div>
               <Typography className={classes.txtAction}>{tableMeta.rowIndex+1}</Typography> 
            </div>
          );
        }
      }
    },
    {
      name: "name",
      label:"Name",
      options: {
        filter: true,
      }
    } ,
    {
      name: "email",
      options: {
        filter: true,
      }
    } ,  
    {
      name: "user_access",
      options: {
        filter: true,
        display:false
      }
    } , 
    {
      name: "Action",
      options: { 
        name:"contingent_name",
        print: false,
        filter: true,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <div>
               <Typography className={classes.txtAction}>{rows[tableMeta.rowIndex].user_access}</Typography>
               {
                rows[tableMeta.rowIndex].user_access === "User Cabor" ? 
                rows[tableMeta.rowIndex].event_name :
                rows[tableMeta.rowIndex].user_access === "Operator Kontingen" ?
                rows[tableMeta.rowIndex].contingent_name
                : null
              }
            </div>
          );
        }
      }
    },
    {
      name: "Action",
      options: { 
        print: false,
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <div>
                <Button className={classes.btnAction} style={{marginBottom:3,marginTop:3}} onClick={() => handleClickOpenEdit(tableMeta.rowIndex)} ><Typography className={classes.txtAction}>Ubah</Typography></Button>
                <Button className={classes.btnAction} style={{marginBottom:3,marginTop:3}} onClick={() => handleClickOpenDelete(rows[tableMeta.rowIndex]._id,rows[tableMeta.rowIndex].name)} ><Typography className={classes.txtAction}>Hapus</Typography></Button>
                <Button className={classes.btnAction} style={{marginBottom:3,marginTop:3}} onClick={() => handleClickOpenReset(rows[tableMeta.rowIndex]._id,rows[tableMeta.rowIndex].name)} ><Typography className={classes.txtAction}>Reset Pass</Typography></Button>
            </div>
          );
        }
      }
    }
  ];


  return (
    <>
      <PageTitle title="Users" button="Tambah Users" click={handleClickOpen} />

      <Dialog
        open={open}
        close={handleClose}
        title={"Form "+ action + " Users"}
        content={
          <Grid container spacing={1}>
            <Grid item sm={6}>
              <TextField
                label="Nama"
                className={classes.formContainer}
                margin="normal"
                value={form_name}
                onChange={(event)=> setform_name(event.target.value)}
              />
            </Grid>
            <Grid item sm={6}>
            <InputLabel htmlFor="contributor-helper">Jenis Kelamin</InputLabel>
                <Select
                  className={classes.formContainer}
                  value={form_gender}
                  onChange={(event)=>{
                    setform_gender(event.target.value); 
                  }}
                  inputProps={{
                    name: 'contributor',
                    id: 'contributor-helper',
                  }}
                >
                  <MenuItem value={"Laki-Laki"}>Laki-Laki</MenuItem>
                  <MenuItem value={"Perempuan"}>Perempuan</MenuItem> 
              </Select> 
            </Grid>
            <Grid item sm={6}>
              <TextField
                label="Telp"
                className={classes.formContainer}
                margin="normal"
                value={form_telp}
                onChange={(event)=> setform_telp(event.target.value)}
              />
            </Grid>
            <Grid item sm={6}>
              <TextField
                label="Alamat"
                className={classes.formContainer}
                margin="normal"
                value={form_address}
                onChange={(event)=> setform_address(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <InputLabel htmlFor="contributor-helper">Foto</InputLabel>
              <Avatar src={imgPreview}/>
              <input type="file" 
              accept="image/*"
              onChange={e =>handleImage(e)}/>
            </Grid>
            {
              action==="add"?
              <Grid item sm={6}>
                <TextField
                  label="Email"
                  className={classes.formContainer}
                  margin="normal"
                  value={form_email}
                  onChange={(event)=> setform_email(event.target.value)}
                />
              </Grid> : 
              <Grid item sm={12}>
                <TextField
                  label="Email"
                  className={classes.formContainer}
                  margin="normal"
                  value={form_email}
                  onChange={(event)=> setform_email(event.target.value)}
                />
              </Grid>
            }
            {
              action === "add"?
              <Grid item sm={6}>
                <TextField
                  label="Password"
                  className={classes.formContainer}
                  margin="normal"
                  value={form_password}
                  onChange={(event)=> setform_password(event.target.value)}
                />
              </Grid> : null
            }
            
            <Grid item xs={12}> 
              <InputLabel htmlFor="contributor-helper">User Access</InputLabel>
                <Select
                  className={classes.formContainer}
                  value={form_user_access}
                  onChange={(event)=>{
                    setform_user_access(event.target.value);
                    if(event.target.value === "admin" || event.target.value === "Tim Keabsahan"|| event.target.value === "Sekretariat PB PON"|| event.target.value === "Dukcapil"){
                      setform_contingent_id("0")
                    }
                  }}
                  inputProps={{
                    name: 'contributor',
                    id: 'contributor-helper',
                  }}
                >
                  <MenuItem value={"Operator Kontingen"}>Operator Kontingen</MenuItem>
                  <MenuItem value={"Tim Keabsahan"}>Tim Keabsahan</MenuItem>
                  <MenuItem value={"User Cabor"}>User Cabor</MenuItem>
                  <MenuItem value={"admin"}>admin</MenuItem>
                  <MenuItem value={"Sekretariat PB PON"}>Sekretariat PB PON</MenuItem>
                  <MenuItem value={"Dukcapil"}>Dukcapil</MenuItem>
                  <MenuItem value={"Tim Keabsahan Bidang Humas"}>Tim Keabsahan Bidang Humas</MenuItem>
                  <MenuItem value={"Manager Cabor Kontingen"}>Tim Keabsahan Bidang Humas</MenuItem>
                  <MenuItem value={"Aplikasi Layanan"}>Aplikasi Layanan</MenuItem>
                  <MenuItem value={"Bidang Pertandingan"}>Bidang Pertandingan</MenuItem>
                  <MenuItem value={"Operator Akreditasi"}>Operator Akreditasi</MenuItem>
              </Select>
            </Grid>
            
            {
              form_user_access === "Operator Kontingen" ? 
              <Grid item xs={12}> 
              <InputLabel htmlFor="contributor-helper">Kontingen</InputLabel>
                <Select
                  className={classes.formContainer}
                  value={form_contingent_id}
                  onChange={(event)=>{
                    setform_contingent_id(event.target.value)
                  }}
                >
                  <MenuItem value={"0"}>Pilih</MenuItem>
                  {
                    contingents.map((item,index)=>{
                      return(
                      <MenuItem value={item._id}>{item.name}</MenuItem>
                      )
                    })
                  }
              </Select> </Grid>
              : 
              form_user_access === "User Cabor" ? 
              <Grid item xs={12}> 
              <InputLabel htmlFor="contributor-helper">Disiplin Olahraga</InputLabel>
                <Select
                  className={classes.formContainer}
                  value={form_event_id}
                  onChange={(event)=>{
                    setform_event_id(event.target.value)
                  }}
                >
                  <MenuItem value={"0"}>Pilih</MenuItem>
                  {
                    events.map((item,index)=>{
                      return(
                      <MenuItem value={item._id}>{item.event_name}</MenuItem>
                      )
                    })
                  }
              </Select> </Grid> : null            
            }
            
          </Grid>
        }

        cancel={handleClose}
        valueCancel ={"Batal"}
        confirm={()=>postData()}
        valueConfirm={"Simpan"}
      />

      {/* modal delete */}
      <Dialog
        open={openModalDelete}
        close={handleClose}
        title={"Apakah anda yakin menghapus ??"}
        content={
          <Typography>{username}</Typography>
        }
        cancel={handleClose}
        valueCancel ={"Tidak"}
        confirm={()=>deleteData(iduser)}
        valueConfirm={"Ya, Hapus"}
      />
      {/* modal Reset */}
      <Dialog
        open={openModalReset}
        close={handleClose}
        title={"Apakah anda yakin mereset password user : "}
        content={
          <Grid>
          <Typography>{username}</Typography>
          <br></br>
          <TextField
          label="Default Password"
          className={classes.formContainer}
          margin="normal"
          value={reset_password}
          onChange={(event)=> setreset_password(event.target.value)}
        /></Grid>
        }
        cancel={handleClose}
        valueCancel ={"Tidak"}
        confirm={()=>resetPassword()}
        valueConfirm={"Ya, Reset"}
      />

      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Paper> 
           <MUIDataTable 
            title="Employee List"
            data={rows} 
            columns={columns}   
            options={{
              selectableRows:  'none'  
            }}
          />
          </Paper>
        </Grid>
          <Snackbar  anchorOrigin={{ vertical: 'bottom',  horizontal: 'left',  }} open={openAlert} autoHideDuration={3000} onClose={handleClose}>
            <Alert onClose={handleClose} severity={errorType} >  {hasError}  </Alert>
          </Snackbar>
      </Grid>
    </>
  );
}

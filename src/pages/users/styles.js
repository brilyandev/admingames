import { makeStyles } from "@material-ui/styles";

export default makeStyles(theme => ({
  formContainer: {
    margin:0,
    width: '100%',
    marginBottom:20
  },
  dense: {
    marginTop: 19,
  },
  txtContentTable:{
    fontWeight:200,
    fontSize:15,
  },
  txtAction:{
    textTransform:'none',
    color:'#bf272b',
    fontWeight:200,
    fontSize:15
  },
  btnAction:{
    marginRight:10,
    border:'1px solid #bf272b'
  },
}));

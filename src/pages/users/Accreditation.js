import React, { useState,useEffect } from "react";
import {
  Grid, 
  Paper,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  Typography,
  Button,
  TextField,
  InputLabel,
  Avatar,
  Select,
  MenuItem,
  Checkbox,
  FormControl
} from "@material-ui/core";

import TablePagination from '@material-ui/core/TablePagination';
import TableSortLabel from '@material-ui/core/TableSortLabel';

// styles
import useStyles from "./styles";

import PageTitle from "../Components/PageTitle/PageTitle";
import axios from "axios";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import _ from 'lodash'
import StaticVar from "../../Config/StaticVar"
export default function Accreditation(props) {
  // global
  var userDispatch = useUserDispatch();  
  var classes = useStyles();
  const url = StaticVar.Base_Url;//"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token 
  };
  
  var getData = async() => {
    try{
     

      let res = await axios.get(url+"/private/users",{headers});
      let response = await res.data;
      console.log('response', JSON.ify(response))

      if(response.hasOwnProperty("status")){
        console.log('error')
        signExpired(userDispatch,props.history);
      }else
      {
        setRows(response)
        setData(response)
      }
    }
    
    catch(error){
      signExpired(userDispatch,props.history);
    }
  }
  
  const [open, setOpen] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);

  const [data, setData] = useState([])
  const [action, setaction] = useState("")
  const [username, setusername] = useState([])
  var [iduser, setiduser] = useState("");
  var [imgPreview, setImgPreview ] = useState(NoImg);
  // var [imgEditPreview, setImgEditPreview ] = useState();
  var [img, setImg ] = useState("");
  var [isImgValid, setIsImgValid ] = useState("");
  var [imgErrorMsg, setImgErrorMsg ] = useState("");
  const [events, setevents] = useState([])
  const [contingents, setcontingents] = useState([])
  const [accreditation_participant, setaccreditation_participant] = useState([])
  const [user_accreditation_list, setuser_accreditation_list] = useState([])
  const [users, setusers] = useState([])
  
  useEffect(() => {
    //getData();

    const req1 = axios.get(url+"/private/users",{headers});
    const req2 = axios.get(url+"/private/contingents",{headers});

    axios.all([req1,req2]).then(axios.spread((...responses) => {
      const response1 = responses[0]
      const datacontingents = responses[1].data
      if(response1.data.hasOwnProperty("status")){
        signExpired(userDispatch,props.history)
      }else
      {
        setRows(datacontingents)
        //setData(response1.data)
        var datauser = response1.data.filter(item=>{
          return item.user_access == "Tim Keabsahan"
        })
        setusers(datauser);
        setcontingents(datacontingents)
      }
    })).catch(errors => {
      console.log('error',errors)
      signExpired(userDispatch,props.history)
    })

  }, []);

  function loadcontingent(datacheck){
    var datapush = []
    rows.forEach(item=>{
      var check_data = datacheck.filter(x=>x.contingent_id === item._id).length
      if(check_data==0){
        datapush.push(item)
      }
    })
    setcontingents(datapush)
  }

  var [user_id,setuser_id] = useState("")
  var [register_button,setregister_button] = useState(false)


  const handleClose = () => {
    setOpen(false);
    setOpenModalDelete(false);
  };

  function deleteData(id) {
    axios.delete(url+"/private/users/"+id,{headers}).then(res=> {
      setOpenModalDelete(false);
      getData();
    })
  }
  const [rows, setRows] = useState([]);

  function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  function search_participant(){
    load_data();
  }

  function load_data(){
    const req1 = axios.get(url+'/private/user_accreditations?user_id='+user_id, {headers});

    axios.all([req1]).then(axios.spread((...responses) => {
      const data_keabsahan = responses[0].data
      setuser_accreditation_list(data_keabsahan)
      loadcontingent(data_keabsahan)
    }))
  }

  function register(data){
    var datapost = {
      user_id: user_id,
      contingent_id : data._id,
    }
    axios.post(url+'/private/user_accreditations/create',datapost, {headers}).then((response)=>{
      load_data()
    })
  }

  return (
    <>
      <PageTitle title="User Akreditasi" />

      

      {/* modal delete */}
      <Dialog
        open={openModalDelete}
        close={handleClose}
        title={"Apakah anda yakin menghapus ??"}
        content={
          <Typography>{username}</Typography>
        }
        cancel={handleClose}
        valueCancel ={"Tidak"}
        confirm={()=>deleteData(iduser)}
        valueConfirm={"Ya, Hapus"}
      />

      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Paper style={{padding:20}}>
             <Typography style={{fontSize:20}}>FILTER</Typography>
             <Grid container spacing={4}>
               <Grid item md={12} xs={12}>
                <FormControl style={{width:'100%'}}>
                  <InputLabel id="demo-customized-select-label">User</InputLabel>

                    <Select style={{width:'100%'}}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={user_id}
                      onChange={(event)=> {
                        setuser_id(event.target.value)
                        
                        
                        if(event.target.value !== ""){
                          
                          setregister_button(true)
                        }
                        else{
                          setregister_button(false)
                          setusername("")
                        }
                      }}
                    >
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>
                      {
                        users.map((item,index)=>{
                          return(
                            <MenuItem value={item._id}>{item.name}</MenuItem>
                          )
                        })
                      }
                    </Select>
                </FormControl>
                 
               </Grid>
               
             </Grid>
            <Button variant="contained" color="secondary" style={{marginTop:20}} onClick={()=>{
              if(user_id != ""){
                var datauser = users.filter(x=>x._id ===user_id)[0].name
                setusername(datauser)
                search_participant();
              }
            }} >
              Cari Data
            </Button>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <h2>Lembar Keabsahan</h2>
          <table style={{width:'100%'}}>
            <tr>
              <td style={{width:200}}>Pilih Tim Keabsahan</td>
              <td>
                
              </td>
            </tr>
          </table>
          <Paper>
            <Table>
              <TableHead>
              <TableRow>
                <TableCell style={{width:30, textAlign:'center'}}><Typography className={classes.txtContentTable}>No</Typography></TableCell>
                <TableCell><Typography className={classes.txtContentTable}>Lembar Keabsahan</Typography></TableCell>
                <TableCell style={{width:100}}></TableCell>
              </TableRow>
              </TableHead>
              <TableBody>
                {
                  contingents.map((item,index)=>{
                    return (
                      <TableRow key={item.id}>
                        <TableCell style={{textAlign:'center'}}>
                          {index+1}
                        </TableCell>
                        <TableCell>
                          <strong>{item._id.participant_type}</strong>
                          <Typography className={classes.txtContentTable}>{item.name}</Typography>
                        </TableCell>
                        <TableCell>
                          {
                            register_button ? 
                            <Button variant="contained" color="primary" onClick={()=> register(item)} > Daftarkan </Button>
                            :
                            <Button variant="contained" color="default" > ... </Button>
                          }
                          
                        </TableCell>
                      </TableRow>
                    );
                  })
                }
              </TableBody>
            </Table>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <h2>Lembar Keabsahan - {username}</h2>
          <Paper>
            
              <Table>
                <TableHead>
                <TableRow>
                  <TableCell style={{width:30, textAlign:'center'}}><Typography className={classes.txtContentTable}>No</Typography></TableCell>
                  <TableCell><Typography className={classes.txtContentTable}>Lembar Keabsahan</Typography></TableCell>
                  <TableCell style={{width:100}}></TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {
                  user_accreditation_list.map((item,index)=>{
                    var contingent = rows.filter(x=>x._id.toString()===item.contingent_id)
                    return (
                      <TableRow key={item._id}>
                        <TableCell style={{textAlign:'center'}}>
                          {index+1}
                        </TableCell>
                        <TableCell>
                          <strong>{contingent[0].name}</strong>
                        </TableCell>
                        <TableCell>
                          <Button variant="contained" color="default" onClick={()=>{
                            axios.delete(url+"/private/user_accreditations/"+item._id, {headers}).then((response)=>{
                              console.log(response.data);
                              load_data()
                            })
                          }} >
                            Hapus
                          </Button>
                        </TableCell>
                      </TableRow>
                    );
                })
              }
                
              </TableBody>
            </Table>
            {/* <TablePagination
              rowsPerPageOptions={[25, 50, 75]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                'aria-label': 'previous page',
              }}
              nextIconButtonProps={{
                'aria-label': 'next page',
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            /> */}
          
          </Paper>
        
          
        
        </Grid>
      </Grid>
    </>
  );
}

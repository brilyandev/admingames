import React, { useState,useEffect } from "react";
import {
  Grid, 
  Paper,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  Typography,
  TextField,
  InputLabel,
  Avatar,
  Button
} from "@material-ui/core";
// styles
import useStyles from "./styles";
import PageTitle from "../Components/PageTitle/PageTitle";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import axios from "axios";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
import StaticVar from "../../Config/StaticVar"

export default function Venues(props) {
  // global
  var userDispatch = useUserDispatch();  
  var classes = useStyles();
  const url = StaticVar.Base_Url;//"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token
  };

  var getData = async() => {
    try{
      let res = await axios.get(url+"/private/venues",{headers});
      let response = await res.data;
      console.log('response', JSON.stringify(response))

      if(response.hasOwnProperty("status")){
        console.log('error')
        signExpired(userDispatch,props.history);
      }else
      {
        setData(response)
      }
    }
    catch(error){
      signExpired(userDispatch,props.history);
    }
  }

  useEffect(() => {
    getData();
    return () => {
      getData();
    }
  }, []);
  
  const [data, setData] = useState([])
  const [venueid, setvenueid] = useState("")
  var [form_name, setform_name] = useState("");
  var [form_capasity, setform_capasity] = useState(0);
  var [form_latitude, setform_latitude] = useState(0);
  var [form_longitude, setform_longitude] = useState(0);
  const [open, setOpen] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [action, setaction] = useState("")
  var [imgPreview, setImgPreview ] = useState(NoImg);
  var [img, setImg ] = useState("");
  var [isImgValid, setIsImgValid ] = useState("");
  var [imgErrorMsg, setImgErrorMsg ] = useState("");

  let sendData = new FormData();
  sendData.append('name', form_name);
  sendData.append('capasity', form_capasity);
  sendData.append('latitude', form_latitude);
  sendData.append('longitude', form_longitude);
  sendData.append('picture', img);
  
  function handleImage(e){
    let reader = new FileReader();
    let file = e.target.files[0],
      pattern = /image-*/;

    if (!file.type.match(pattern)) {
      setIsImgValid(true)
      setImgErrorMsg("Format File tidak sesuai")
      return;
    }

    reader.onloadend = () => {
      setIsImgValid(false);
      setImgErrorMsg("");
      setImg(file);
      setImgPreview(reader.result)
    };

    reader.readAsDataURL(file);
  }

  const handleClickOpen = () => {
    setaction("Tambah");
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenModalDelete(false);
  };

  function postData(){
    if(action === "Tambah"){
      axios.post(url+"/private/venues/create",sendData,{headers}).then(res=> {
        console.log(res.data)
        getData();
        setOpen(false);
      })
    }

    if(action === "Ubah"){
      axios.put(url+"/private/venues/"+venueid,sendData,{headers}).then(res=> {
        getData();
        setOpen(false);
      })
    }
    
  }

  function handleClickOpenEdit(id) {
    setvenueid(id);
    setOpen(true);
    setaction("Ubah");
    axios.get(url+"/private/venues/"+id,{headers}).then(res=>{
      console.log("data", JSON.stringify(res.data) );
      setform_name(res.data[0].name)
      setform_capasity(res.data[0].capasity)
      setform_latitude(res.data[0].location.latitude)
      setform_longitude(res.data[0].location.longitude)
      setImgPreview(url+"/repo/venue/"+res.data[0].picture);
    })
  };

  function handleClickOpenDelete(id,name) {
    setvenueid(id);
    setform_name(name);
    setOpenModalDelete(true);
    
  };

  function deleteData(id) {
    axios.delete(url+"/private/venues/"+id,{headers}).then(res=> {
      setOpenModalDelete(false);
      getData();
    })
  }

  return (
    <>
      <PageTitle title="Venue" button="Tambah Venue" click={handleClickOpen} />
      <Dialog
        open={open}
        close={handleClose}
        title={"Form "+ action + " Venue"}
        content={
          <Grid container spacing={1}>
            <Grid item sm={12}>
              <TextField
                label="Nama"
                className={classes.formContainer}
                margin="normal"
                value={form_name}
                onChange={(event)=> setform_name(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <TextField
                label="Kapasitas"
                className={classes.formContainer}
                margin="normal"
                value={form_capasity}
                onChange={(event)=> setform_capasity(event.target.value)}
              />
            </Grid>
            <Grid item sm={6}>
              <TextField
                label="Latitude"
                className={classes.formContainer}
                margin="normal"
                value={form_latitude}
                onChange={(event)=> setform_latitude(event.target.value)}
              />
            </Grid>
            <Grid item sm={6}>
              <TextField
                label="Longitude"
                className={classes.formContainer}
                margin="normal"
                value={form_longitude}
                onChange={(event)=> setform_longitude(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <InputLabel htmlFor="contributor-helper">Gambar</InputLabel>
              <Avatar src={imgPreview}/>
              <input type="file" 
              accept="image/*"
              onChange={e =>handleImage(e)}/>
            </Grid>
          </Grid>
        }

        cancel={handleClose}
        valueCancel ={"Batal"}
        confirm={()=>postData()}
        valueConfirm={"Simpan"}
      />

       {/* modal delete */}
       <Dialog
        open={openModalDelete}
        close={handleClose}
        title={"Apakah anda yakin menghapus ??"}
        content={
          <Typography>{setform_name}</Typography>
        }
        cancel={handleClose}
        valueCancel ={"Tidak"}
        confirm={()=>deleteData(venueid)}
        valueConfirm={"Ya, Hapus"}
      />

      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Paper>
            <Table>
              <TableHead>
              <TableRow>
                <TableCell style={{width:30, textAlign:'center'}}>No</TableCell>
                <TableCell style={{width:100}}>Logo</TableCell>
                <TableCell>Nama Venue</TableCell>
                <TableCell style={{width:150, textAlign:'center'}}>Kapasitas</TableCell>
                <TableCell style={{width:250, textAlign:'center'}}>Lokasi</TableCell>
                 <TableCell style={{width:250, textAlign:'center'}}>Aksi</TableCell>
              </TableRow>
              </TableHead>
              <TableBody>
                {
                  data.map((item,index)=>{
                    return(                      
                      <TableRow>
                        <TableCell>{(index+1)}</TableCell>
                        <TableCell>
                          <Avatar src={url+"/repo/venue/"+item.picture}/>
                        </TableCell>
                        <TableCell>{item.name}</TableCell>
                        <TableCell>{item.capasity} orang</TableCell>
                        <TableCell>Lat : {item.location.latitude} <br/> Long : {item.location.form_longitude}
                        </TableCell>
                        <TableCell style={{textAlign:'center'}}>
                          <Button className={classes.btnAction} onClick={() => handleClickOpenEdit(item._id)} ><Typography className={classes.txtAction}>Ubah</Typography></Button>
                          <Button className={classes.btnAction} onClick={() => handleClickOpenDelete(item._id,item.name)} ><Typography className={classes.txtAction}>Hapus</Typography></Button>
                        </TableCell>
                      </TableRow>
                    )
                  })
                }
              </TableBody>
            </Table>
          
          </Paper>
        </Grid>
      </Grid>
    </>
  );
}

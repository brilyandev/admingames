import React, { useState, useEffect,Fragment } from "react";
import imageCompression from "browser-image-compression";
import { Editor } from "@tinymce/tinymce-react";

import {
  Grid,
  Typography,
  Button,
  TextField,
  Box,
  FormControl,
  FormControlLabel,
  InputLabel,
  Select,
  MenuItem,
} from "@material-ui/core";
import axios from "axios";
import _ from "lodash";
// styles
import useStyles from "./styles";
import PageTitle from "../../Components/PageTitle/PageTitle";
import NoImg from "../../Assets/Images/no-image.png";
import Dialog from "../../Components/Dialog/Dialog";
import StaticVar from "../../Config/StaticVar"

export default function EditorPages(props) {
  var classes = useStyles();

  const url = StaticVar.Base_Url;//"http://localhost:300";

  let token = localStorage.getItem("token");

  const headers = {
    "Content-Type": "application/json",
    "x-access-token": token,
  };

  let [name, setname] = useState([]);
  let [dataParent, setDataParent] = useState([]);
  var [parent, setparent] = useState("");
  var [imgPreview, setImgPreview] = useState(NoImg);
  var [img, setImg] = useState("");
  var [isImgValid, setIsImgValid] = useState("");
  var [imgErrorMsg, setImgErrorMsg] = useState("");
  var [editorState, seteditorState] = useState("");
  var [openAlert, setopenAlert] = useState(false);
  var [fullWidth, setFullWidth] = useState(false);
  var [messageError, setmessageError] = useState("");
  var [status, setstatus] = useState("");
  
  function handleEditorChange(e) {
    var value = e.target.getContent();
    seteditorState(value);
    console.log("Content was updated:", editorState);
  }

  const action = props.match.params.action;
  const id = props.match.params.id;

  async function getData() {
    if(id !== "0"){
      let res = await axios.get(url + "/private/pages/" + id, { headers });
      let response = await res.data[0];
      setname(response.name);
      setstatus(response.page_status);
      setparent(response.parent);
      const datasample = response.content;
      seteditorState(datasample);
    }
  }

  const getDataParent = async () => {
    let res = await axios.get(url + "/private/pages", { headers });
    let response = await res.data;

    var dataparent = _.filter(response, function(item) {
      return item.parent == "Tanpa Parent";
    });
    setDataParent(dataparent);
  };
  
  useEffect(() => {
    getDataParent();
    getData();
    return () => {
      getDataParent();
      getData();
    };
  }, []);


  return (
    <>
      <PageTitle title="Tambah Laman" />
      <Grid container style={{ paddingLeft: 5, paddingRight: 20 }}>
        <Grid item xs={12} style={{ marginBottom: 20 }}>
          <Box>
            <Typography color="textSecondary">Nama Laman : </Typography>
            <TextField
              className={classes.formContainer}
              margin="normal"
              value={name}
              onChange={event => setname(event.target.value)}
            />
          </Box>
        </Grid>
        <Grid item xs={12} style={{ marginBottom: 10 }}>
          <InputLabel htmlFor="contributor-helper">Parent</InputLabel>
          <Select
            className={classes.formContainer}
            value={parent}
            onChange={event => {
              setparent(event.target.value);
            }}
          >
            <MenuItem value={"Tanpa Parent"}>Tanpa Parent</MenuItem>
            {dataParent.map(item => (
              <MenuItem value={item._id}>{item.name}</MenuItem>
            ))}
          </Select>
        </Grid>
        <Grid item xs={12} style={{ marginBottom: 20 }}>
          <Typography>Isi Laman</Typography>
          <input
            accept="image/*"
            className={classes.input}
            id="file-editor"
            multiple
            type="file"
            // onChange={e => handleImage(e)}
          />
          <Box className={classes.editorToolbar}>
            <Editor
              // apiKey="2x3gj3d5upqimvljmbb1xbiir8ghe5v1rhuo4jfot8oikxpk <= api key in tinymce free trial in 30 days"
              initialValue={editorState}
              value={editorState}
              init={{
                selector: "textarea",
                file_picker_callback: function(callback, value, meta) {
                  if (meta.filetype == "image") {
                    var input = document.getElementById("file-editor");
                    input.click();
                    input.onchange = function() {
                      var reader = new FileReader();
                      var file = input.files[0];
                      var options = {
                        maxSizeMB: 0.1,
                        maxWidthOrHeight: 1500,
                        useWebWorker: true,
                      };
                      imageCompression(file, options).then(function(
                        compressedFile,
                      ) {
                        reader.onload = function(e) {
                          callback(e.target.result, {
                            alt: compressedFile.name,
                          });
                        };
                        reader.readAsDataURL(compressedFile);
                      });
                    };
                  }
                },
                paste_data_images: true,
                images_upload_handler: function(blobInfo, success, failure) {
                  success(
                    "data:" +
                      blobInfo.blob().type +
                      ";base64," +
                      blobInfo.base64(),
                  );
                },
                branding: false,
                height: 500,
                menubar: true,
                plugins: [
                  "advlist autolink lists link image charmap print preview anchor hr pagebreak",
                  "searchreplace wordcount visualblocks visualchars code fullscreen",
                  "insertdatetime media table paste code help",
                ],
                toolbar:
                  "undo redo | styleselect fontselect fontsizeselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | imageupload image media | bullist numlist outdent indent | code | removeformat help",
              }}
              // onDragDrop={true}
              onChange={e => handleEditorChange(e)}
            />
          </Box>
        </Grid>
        <Grid item xs={12}>
          <div style={{ float: "right" }}>
            <Button
              style={{
                width: 120,
                border: "2px solid gray",
                marginRight: 10,
              }}
              onClick={() => {
                props.history.push("/app/pages");
              }}
            >
              Batal
            </Button>
            {
              action == "add" ?
            <Button
              style={{
                width: 120,
                border: "2px solid #bf272b",
                color: "#bf272b",
              }}
              onClick={() => {
                let sendData = new FormData();
                sendData.append("name", name);
                sendData.append("parent", parent);
                sendData.append("content", editorState);
                sendData.append("page_status", "Laman Baru");
                if (name === "") {
                  setopenAlert(true);
                  setmessageError("Data Title wajib diisikan");
                }
                else {
                  axios({
                    method: "post",
                    url: url + "/private/pages/create",
                    data: sendData,
                    headers: headers,
                    onUploadProgress: function(progressEvent) {
                      var percentCompleted = Math.round(
                        (progressEvent.loaded * 100) / progressEvent.total,
                      );
                      console.log("test progress upload", percentCompleted);
                    },
                  })
                    .then(() => {
                      props.history.push("/app/pages/list-pages");
                    })
                    .catch(err => {
                      setopenAlert(true);
                      setmessageError(err);
                    });
                }
              }}
            >
              Simpan
            </Button>:
            <>
            <Button
            style={{
              width: 150,
              border: "2px solid #6f9ae8",
              marginRight: 10,
            }}
            onClick={() => {
              let sendData = new FormData();
              sendData.append("name", name);
              sendData.append("parent", parent);
              sendData.append("content", editorState);
              sendData.append("page_status", "Telah diedit");
              axios
                .put(url + "/private/pages/" + id, sendData, { headers })
                .then(() => {
                  props.history.push("/app/pages/list-pages");
                });
              }}
            >
              Simpan Konten
            </Button>
            <Button
              style={{
                width: 150,
                border: "2px solid #bf272b",
                color: "#bf272b",
              }}
              onClick={() => {
                let sendData = new FormData();
                sendData.append("name", name);
                sendData.append("parent", parent);  
                sendData.append("content", editorState);
                sendData.append("page_status", "Publish");
                axios
                  .put(url + "/private/pages/" + id, sendData, { headers })
                  .then(res => {
                    console.log(res);
                    props.history.push("/app/pages/list-pages");
                  });
              }}
            >
              Publish
            </Button>
            </>
            }
          </div>
        </Grid>
        <Dialog
          open={openAlert}
          close={() => setopenAlert(false)}
          title={"Periksa Data isian Anda ??"}
          content={<Typography>{messageError}</Typography>}
          cancel={() => setopenAlert(false)}
          confirm={() => setopenAlert(false)}
          valueConfirm={"Oke"}
          valueCancel={"Tutup"}
        />
      </Grid>
    </>
  );
}

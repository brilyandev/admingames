import React, { useState,useEffect } from "react";
import {
  Grid, 
  Paper,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  Typography,
  TextField,
  InputLabel,
  Avatar,
  Button
} from "@material-ui/core";
// styles
import useStyles from "./styles";
import PageTitle from "../Components/PageTitle/PageTitle";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import axios from "axios";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
import Api from '../../Services/Api'
import StaticVar from '../../Config/StaticVar'
import { AppsOutlined } from "@material-ui/icons";
export default function Disciplines(props) {
  // global
  var userDispatch = useUserDispatch();  
  var classes = useStyles();

  var [datafilter, setdatafilter]=useState([])

  function getData(){
    try{
      const req1 = Api.getAllEvents()// axios.get(url+"/private/events/get_all_event",{headers});
      
      axios.all([req1]).then(
        axios.spread((...responses) => {
          var response1 = responses[0].data;
          if(response1.hasOwnProperty("status")){
            signExpired(userDispatch,props.history);
          }
          else
          {
            setData(response1)
            setdatafilter(response1)
            console.log('dataDisipline',response1)
          }
        }
      ))
    }
    catch(error){
      signExpired(userDispatch,props.history);
    }
    
  }

  useEffect(() => {
    getData();
    return () => {
      getData();
    }
  }, []);
  
  const [data, setData] = useState([])
  const [discipline_id, setdiscipline_id] = useState("")
  var [form_name, setform_name] = useState("");
  const [open, setOpen] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [action, setaction] = useState("")
  var [imgPreview, setImgPreview ] = useState(NoImg);
  var [img, setImg ] = useState("");
  var [isImgValid, setIsImgValid ] = useState("");
  var [imgErrorMsg, setImgErrorMsg ] = useState("");
  var [txtsearch, settxtsearch ] = useState("");

  let sendData = new FormData();
  sendData.append('event_name', form_name);
  sendData.append('event_icon', img);
  
  function handleImage(e){
    let reader = new FileReader();
    let file = e.target.files[0],
      pattern = /image-*/;

    if (!file.type.match(pattern)) {
      setIsImgValid(true)
      setImgErrorMsg("Format File tidak sesuai")
      return;
    }

    reader.onloadend = () => {
      setIsImgValid(false);
      setImgErrorMsg("");
      setImg(file);
      console.log('file image',file)
      setImgPreview(reader.result)
    };

    reader.readAsDataURL(file);
  }

  const handleClickOpen = () => {
    setaction("Tambah");
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenModalDelete(false);
  };

  function postData(){
    if(action === "Tambah"){
      Api.postEvents(sendData).then(res=>{
        console.log(res.data)
        getData();
        setOpen(false);
      })
    }

    if(action === "Ubah"){
      Api.putEvents(discipline_id, sendData).then(res=>{
        getData();
        setOpen(false);
      })
    }    
  }

  function handleClickOpenEdit(id) {
    setdiscipline_id(id);
    setOpen(true);
    setaction("Ubah");
    Api.getEventById(id).then(res=>{
      console.log("data", JSON.stringify(res.data) );
      setform_name(res.data[0].event_name)
      setImgPreview(StaticVar.ImageUrl+"event/"+res.data[0].event_icon);
    })
  };

  function handleClickOpenDelete(id,name) {
    setdiscipline_id(id);
    setform_name(name);
    setOpenModalDelete(true);
    
  };

  function deleteData(id) {
    Api.deleteEvents(id).then(res=>{
      setOpenModalDelete(false);
      getData();
    })
  }

  function SearchFilterFunction(text) {
    var dtBaru = datafilter.filter(item=>{
      const itemData = item.event_name ? item.event_name.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    setData(dtBaru);
    settxtsearch(text)
    if(text===""){
      setData(datafilter);
    }
  }
  
  return (
    <>
      <PageTitle title="Disiplin" button="Tambah Disiplin" click={handleClickOpen} />

      <Dialog
        open={open}
        close={handleClose}
        title={"Form "+ action + " Disiplin"}
        content={
          <Grid container spacing={1}>
            <Grid item sm={12}>
              <TextField
                label="Nama"
                className={classes.formContainer}
                margin="normal"
                value={form_name}
                onChange={(event)=> setform_name(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <InputLabel htmlFor="contributor-helper">Icon</InputLabel>
              <Avatar src={imgPreview}/>
              <input type="file" 
              accept="image/*"
              onChange={e =>handleImage(e)}/>
            </Grid>
          </Grid>
        }

        cancel={handleClose}
        valueCancel ={"Batal"}
        confirm={()=>postData()}
        valueConfirm={"Simpan"}
      />

       {/* modal delete */}
       <Dialog
        open={openModalDelete}
        close={handleClose}
        title={"Apakah anda yakin menghapus ??"}
        content={
          <Typography>{form_name}</Typography>
        }
        cancel={handleClose}
        valueCancel ={"Tidak"}
        confirm={()=>deleteData(discipline_id)}
        valueConfirm={"Ya, Hapus"}
      />

      <Grid container spacing={4}>
        <Grid item xs={12}>
        
          <Paper className={classes.paper}>
            <TextField
                label="Cari Nama Disiplin"
                className={classes.formContainer}
                margin="normal"
                value={txtsearch}
                onChange={(event)=> SearchFilterFunction(event.target.value)}
              />
            <Table className={classes.table} size="small" aria-label="a dense table">
              <TableHead>
              <TableRow>
                <TableCell rowSpan={2} style={{width:30, textAlign:'center'}}>No</TableCell>
                <TableCell rowSpan={2} style={{width:100}}>Logo</TableCell>
                <TableCell rowSpan={2}>Disiplin</TableCell>
                <TableCell rowSpan={2}>Pra PON</TableCell>
                <TableCell colSpan={4} style={{width:150, textAlign:'center'}}>Jumlah Nomor</TableCell>
                <TableCell colSpan={3} style={{width:150, textAlign:'center'}}>Max Entries</TableCell>
                <TableCell rowSpan={2} style={{width:100, textAlign:'center'}}>Aksi</TableCell>
              </TableRow>
              <TableRow>
                <TableCell style={{width:80, textAlign:'center'}}>Putra</TableCell>
                <TableCell style={{width:80, textAlign:'center'}}>Putri</TableCell>
                <TableCell style={{width:80, textAlign:'center'}}>Mix</TableCell>
                <TableCell style={{width:80, textAlign:'center'}}>Open</TableCell>
                <TableCell style={{width:80, textAlign:'center'}}>Number</TableCell>
                <TableCell style={{width:80, textAlign:'center'}}>Atlit</TableCell>
                <TableCell style={{width:80, textAlign:'center'}}>Official</TableCell>                
              </TableRow>
              </TableHead>
              <TableBody>
                {
                  data.map((item,index)=>{
                   
                    return(                      
                      <TableRow>
                        <TableCell>{(index+1)}</TableCell>
                        <TableCell>
                          <Avatar src={StaticVar.ImageUrl+"sport/"+item.event_icon}/>
                        </TableCell>
                        <TableCell>{item.event_name}</TableCell>
                        <TableCell>{item.event_qualifications}</TableCell>
                        <TableCell style={{textAlign:'center'}}>
                          {
                            item.male
                          }
                        </TableCell>
                        <TableCell style={{textAlign:'center'}}>
                          {
                            item.female
                          }
                        </TableCell>
                        <TableCell style={{textAlign:'center'}}>
                          {
                           item.mix
                          }
                        </TableCell>
                        <TableCell style={{textAlign:'center'}}>
                          {
                            item.open
                          }
                        </TableCell>
                        <TableCell style={{textAlign:'center'}}>
                          {
                            item.max_entries_athlete
                          }
                        </TableCell>
                        <TableCell style={{textAlign:'center'}}>
                          {
                            item.max_entries_contingent
                          }
                        </TableCell>
                        <TableCell style={{textAlign:'center'}}>
                          {
                            item.max_entries_official
                          }
                        </TableCell>
                        <TableCell style={{textAlign:'center'}}>
                          <Button className={classes.btnAction}  onClick={() => {props.history.push('ViewDisciplines/'+item.event_id)}} ><Typography className={classes.txtAction}>Lihat</Typography></Button>
                          {/* <Button className={classes.btnAction} onClick={() => handleClickOpenEdit(item.event_id)} ><Typography className={classes.txtAction}>Ubah</Typography></Button> */}
                          {/* <Button className={classes.btnAction} onClick={() => handleClickOpenDelete(item.event_id,item.event_name)} ><Typography className={classes.txtAction}>Hapus</Typography></Button> */}
                        </TableCell>
                      </TableRow>
                    )
                  })
                }
              </TableBody>
            </Table>
          
          </Paper>
        </Grid>
      </Grid>
    </>
  );
}

import React, { useState, useEffect } from "react";
import { 
  TextField,
  InputLabel, 
  Select,
  MenuItem,
  Typography,
  Button,
  Grid,
} from "@material-ui/core";

import TablePagination from '@material-ui/core/TablePagination';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Loader from 'react-loader-spinner';

// styles
import useStyles from "../dashboard/styles";
import MUIDataTable from "mui-datatables";

import PageTitle from "../Components/PageTitle/PageTitle";
import PageTitleSmall from "../Components/PageTitle/PageTitleSmall";
import axios from "axios";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import StaticVar from "../../Config/StaticVar"

function Entries(props) {
  var userDispatch = useUserDispatch();
  var classes = useStyles();

  //"http://localhost:300"
  const url = StaticVar.Base_Url//"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token
  };
  const [rows, setRows] = useState([]);
  const [discipline_name, setDiscipline_name] = useState('');
  const [categoriesName, setCategoriesName] = useState('');
  const [contingentName, setContingentName] = useState('');
  const [event_qualifications, setEvent_qualifications] = useState('');
  const [contingents, setcontingents] = useState([])

  const [action, setaction] = useState("")

  const [byNumber, setByNumber] = useState(true);
  var [loading, setloading] = useState(false); 
  

   const LoadingIndicator = props => {
       return (
        <h3>Loading! </h3>
      );  
     }
   
  var getData = async() => { 
    setloading(true)
    try{ 
      console.log( localStorage.getItem("token"))
        axios.get(url + "/private/events/list/entries_prapon", { headers }).then((response1) => {
            var data = response1.data; 
            var data = response1.data;
            console.log(data);
            setRows(data) 
          setloading(false)
          })
    }
    
    catch(error){
      signExpired(userDispatch,props.history);
    }
  }
  useEffect(() => {
    getData();
    return () => {
      getData();
    } 
  }, []); 


  const columns = [
    {
      name: "participant_id",  label: "participant_id",
      options: {
        filter: true,
        display:false,
        print:false
      }
    }, 
    {
      name: "No",
      options: {  print: false, filter: false,  sort: false, empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <div>
              <Typography className={classes.txtAction}>{tableMeta.rowIndex + 1}</Typography>
            </div>
          );
        }
      }
    },
    {
      name: "event_name",  label: "Cabor",
      options: {
        filter: true,
      }
    },
    {
      name: "event_qualification",  label: "event_qualification",
      options: {
        filter: true 
      }
    },
    {
      name: "jumlahPraPon",  label: "jumlahPraPon",
      options: {
        filter: true,
      }
    },
    {
      name: "jumlah",  label: "jumlah Entries",
      options: {
        filter: true,
      }
    },
    {
      name: "contingentsPraPon",  label: "Prapon",
      options: {
        filter: true, 
        display:false
      }
    },
    {
      name: "contingentsSportEntries",  label: "Entries",
      options: {
        filter: true,
        display:false
      }
    },
    {
      name: "Kontingen PraPon",
      options: { 
        print: false,
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <div>
            </div>
          );
        }
      }
    }
  ]; 
  const [columnsDisplay, setColumns] = useState(columns);
  const options = {
    filterType: "dropdown",
    responsive: "scroll", 
    onTableChange: (action, tableState) => {
      console.dir(tableState);
      //console.dir(tableState);
    },
    onFilterChange: (changedColumn, filterList) => {
      console.log(changedColumn, filterList);
      var newCols = columns.slice();
      for (var ii = 0; ii < newCols.length; ii++) {
        if (newCols[ii].name === changedColumn) {
          newCols[ii].options.filterList = filterList[ii];
        }
      }
      setColumns(newCols);
    }
  };  

 
  return (

    <div>
      <PageTitle title="Laporan Entries"  /> 
      
      {loading ? <LoadingIndicator /> :  <MUIDataTable
        title="List Hasil Pra PON"
        data={rows}
        columns={columnsDisplay}
        options={{options,
          selectableRows: 'none'
        }}
      />}
     
    </div>


  )
};
export default Entries;
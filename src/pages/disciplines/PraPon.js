import React, { useState, useEffect } from "react";
import { 
  TextField,
  InputLabel, 
  Select,
  MenuItem,
  Typography,
  Button,
  Grid,
} from "@material-ui/core";
 
// styles
import useStyles from "./styles";
import MUIDataTable from "mui-datatables";
import { TableFooter, TableCell, TableRow } from "@material-ui/core"; 

import CustomFooter from "../Components/Table/CustomFooter";

import PageTitle from "../Components/PageTitle/PageTitle";
import PageTitleSmall from "../Components/PageTitle/PageTitleSmall";
import axios from "axios";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import Api from "../../Services/Api"
import StaticVar from "../../Config/StaticVar"

function Prapon(props) {
  var userDispatch = useUserDispatch();
  var classes = useStyles();

  //"http://localhost:300"
  // const url = "http://localhost:300";
  // const token = localStorage.getItem("token");
  // const headers = {
  //   'Content-Type': 'application/json',
  //   'x-access-token': token
  // };
  const [rows, setRows] = useState([]);
  const [discipline_name, setDiscipline_name] = useState('');
  const [categoriesName, setCategoriesName] = useState('');
  const [contingentName, setContingentName] = useState('');
  const [event_qualifications, setEvent_qualifications] = useState('');
  const [contingents, setcontingents] = useState([])

  const [action, setaction] = useState("")

  const [byNumber, setByNumber] = useState(true);
  const [open, setOpen] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [dialogByQuota, setDialogByQuota ] = useState(false);
  

   const LoadingIndicator = props => {
       return (
        <h3>Loading! </h3>
      );  
     }
  
  var loadDataAwal=async() => {
    console.log('awal');
    try{
      Api.getContingents().then(res=>setcontingents(res.data)  )
    }
    
    catch(error){
      signExpired(userDispatch,props.history);
    }
  }

  useEffect(() => { 
    loadDataAwal(); 
    return () => {
      loadDataAwal();
    } 
  }, []);
  useEffect(() => {
    
    if( props.match.params.idKelas==='-'  )
    {
      setByNumber(false)
      Api.getEventData(props.match.params.id).then(response1=>{
        var data = response1.data; 
        setDiscipline_name(data[0].event_name)  
        setEvent_qualifications(data[0].event_qualifications)
      })
    }
    else
    {
      setByNumber(true)
      Api.getEventCategoryData(props.match.params.idKelas).then(response1=>{
        var data = response1.data; 
        setDiscipline_name(data[0].event_name) 
        var event_categories = data[0].event_categories; 
        setCategoriesName(event_categories.category_name)
        setEvent_qualifications(data[0].event_qualifications)
      })
    }
  }, []);
  var getData = async() => { 
    setloading(true)
    try{ 
      if( props.match.params.idKelas==='-'  )
      {
        Api.getEventParticipantQuota(props.match.params.id).then(response1=>{
          var data = response1.data;
          console.log(data);
          setRows(data) 
          setloading(false)
        })
      }
      else
      {
        Api.getEventParticipantCategory(props.match.params.id, props.match.params.idKelas).then((response1) => {
          var data = response1.data;
          console.log(data);
          setRows(data) 
          setloading(false)
        })
      }
    }
    
    catch(error){
      signExpired(userDispatch,props.history);
    }
  }
  useEffect(() => {
    getData();
    return () => {
      getData();
    } 
  }, []); 


  const columns = [
    {
      name: "participant_id",  label: "participant_id",
      options: {
        filter: true,
        display:false,
        print:false
      }
    }, 
    {
      name: "No",
      options: {  print: false, filter: false,  sort: false, empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <div>
              <Typography className={classes.txtAction}>{tableMeta.rowIndex + 1}</Typography>
            </div>
          );
        }
      }
    },
    {
      name: "contingent_name",  label: "Name",
      options: {
        filter: true,
      }
    },
    {
      name: "contingent_id",  
      options: {
        filter: true ,
        display:false
      }
    },
    {
      name: "full_name",  label: "Atlit",
      options: {
        filter: true,
      }
    },
    {
      name: "quota",  label: "quota",
      options: {
        filter: true 
      }
    },
    {
      name: "quotaAtlitPa",  label: "quotaAtlitPa",
      options: {
        filter: true,
      }
    },
    {
      name: "quotaAtlitPi",  label: "quotaAtlitPi",
      options: {
        filter: true, 
      }
    },
    {
      name: "quotaTimPa",  label: "quotaTimPa",
      options: {
        filter: true,
      }
    },
    {
      name: "quotaTimPi",  label: "quotaTimPi",
      options: {
        filter: true,
        
      }
    },
    {
      name: "jenisKelamin",  label: "jenisKelamin",
      options: {
        filter: true,
      }
    },
    {
      name: "Action",
      options: {  print: false,  filter: false, sort: false, empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <div>
              <Button className={classes.btnAction} style={{ marginBottom: 3, marginTop: 3 }} onClick={() => handleClickOpenEdit(tableMeta.rowIndex,tableMeta.rowData)} ><Typography className={classes.txtAction}>Ubah</Typography></Button>
              <Button className={classes.btnAction} style={{ marginBottom: 3, marginTop: 3 }} onClick={() => handleClickOpenDelete(tableMeta.rowIndex,tableMeta.rowData)} ><Typography className={classes.txtAction}>Hapus</Typography></Button>
            </div>
          );
        }
      }
    }
  ]; 
  const [columnsDisplay, setColumns] = useState(columns);
  const options = {
    filterType: "dropdown", 
    customFooter: (count, page, rowsPerPage, changeRowsPerPage, changePage, textLabels) => {
      return (  
        <CustomFooter 
          count={count} 
          page={page} 
          rowsPerPage={rowsPerPage} 
          changeRowsPerPage={changeRowsPerPage} 
          changePage={changePage} 
          textLabels={textLabels} />
      );
    } , 
    onTableChange: (action, tableState) => {
      console.dir(tableState);
      //console.dir(tableState);
    },
    onFilterChange: (changedColumn, filterList) => {
      console.log(changedColumn, filterList);
      var newCols = columns.slice();
      for (var ii = 0; ii < newCols.length; ii++) {
        if (newCols[ii].name === changedColumn) {
          newCols[ii].options.filterList = filterList[ii];
        }
      }
      setColumns(newCols);
    }
  };

  const sumSalery = (startIndex, endIndex) => {
    console.log('sum')
    return rows
      .slice(startIndex, endIndex)
      .map(a => a[3])
      .reduce((total, quota) => (total += quota), 0);
  }; 
  const handleClickOpen = () => {
    if( props.match.params.idKelas==='-'  )
    {
      setaction("add");
      setDialogByQuota(true)
    }
    else
    {
      setaction("add");
      setOpen(true)
    }
  };

  function handleClickOpenEdit(id,value) {
    if( props.match.params.idKelas==='-'  )
    {
      setaction("edit");
      setidPraPon(value[0]);  
      setContingentName(value[2]); 
      setform_fullName(value[4]);  
      setform_gender(value[10]); 
      setform_contingent_id(value[3]); 
      setform_quotaAtlitPA(value[6]); 
      setform_quotaAtlitPI(value[7]); 
      setform_quotaTeamPA(value[8]); 
      setform_quotaTeamPI(value[9]);   
      setDialogByQuota(true);
    }
    else
    {
      setaction("edit");
      setidPraPon(value[0]); 
      setContingentName(value[2]); 
      setform_contingent_id(value[3]); 
      setform_fullName(value[4]); 
      setform_quota(value[5]);   
      setOpen(true);
    }
   }
    function handleClickOpenDelete(id,value) {  
      setidPraPon(value[0]); 
    setContingentName(value[2]);
    setOpenModalDelete(true);
    
  };
  const handleClose = () => {
    setDialogByQuota(false);
    setOpen(false);
    setOpenModalDelete(false);
  };



  function postData(){ 
    var contingenName="";
    contingents.forEach(item=>{
      if(item._id===form_contingent_id)
      contingenName=item.name;
    }) 
    var data={event_id:props.match.params.id,category_id:props.match.params.idKelas, 
      contingent_id:form_contingent_id,contingent_name:contingenName,full_name:form_fullName,quota:form_quota};
    if(action === "add")
    {
      Api.putEventAddParticipant(props.match.params.id ,data).then(res=> {
        getData(); 
        setOpen(false);
      })
    }
    if(action === "edit"){
      console.log('setidPraPon', idPraPon)
      Api.putEventUpdateParticipant(props.match.params.idKelas, idPraPon, data).then(res=>{
        getData();
        setOpen(false);
      })
    } 
  }
  function postData1(){ 
    var contingenName="";
    contingents.forEach(item=>{
      if(item._id===form_contingent_id)
      contingenName=item.name;
    }) 
    var data="";
    if(event_qualifications==="Lolos By Name")
    {
      data={event_id:props.match.params.id, jenisKelamin: form_gender,quota:form_quota, 
       contingent_id:form_contingent_id,contingent_name:contingenName,full_name:form_fullName };
    }
    else
    {
      data={event_id:props.match.params.id, quota:form_quota, 
       contingent_id:form_contingent_id,contingent_name:contingenName, quotaTimPa:form_quotaTeamPA
       ,quotaTimPi:form_quotaTeamPI,quotaAtlitPa:form_quotaAtlitPA,quotaAtlitPi:form_quotaAtlitPI};
    }
    if(action === "add")
    {
      Api.putEventAddQuota(props.match.params.id ,data).then(res=> {
        getData(); 
        setDialogByQuota(false);
      })
    }
    if(action === "edit"){
      Api.putEventUpdateQuota(props.match.params.id, idPraPon, data).then(res=> {
        getData();
        setDialogByQuota(false);
      })
    } 
  }
  function deleteData() {
    if( props.match.params.idKelas==='-'  )
    {
      Api.deleteEventQuota(props.match.params.id,idPraPon).then(res=> {
        setOpenModalDelete(false);
        getData();
      })
    }
    else
    {
      Api.deleteEventParticipantQuota(props.match.params.idKelas, idPraPon).then(res=> {
        setOpenModalDelete(false);
        getData();
      })
    }
  }
  var [form_contingent_id, setform_contingent_id] = useState("0"); 
  var [form_fullName, setform_fullName] = useState("");
  var [form_quota, setform_quota] = useState("1"); 
  var [idPraPon, setidPraPon] = useState("");
  var [loading, setloading] = useState(false); 
  var [form_gender, setform_gender] = useState("");
  
  var [form_quotaTeamPA, setform_quotaTeamPA] = useState("0"); 
  var [form_quotaTeamPI, setform_quotaTeamPI] = useState("0"); 
  var [form_quotaAtlitPA, setform_quotaAtlitPA] = useState("0"); 
  var [form_quotaAtlitPI, setform_quotaAtlitPI] = useState("0"); 

  return (

    <div>
      <PageTitle title={discipline_name} button="Tambah Participant" click={handleClickOpen} />
      {byNumber ?<PageTitleSmall title={categoriesName} />:''}
      <PageTitleSmall title={event_qualifications } />
      
      {loading ? <LoadingIndicator /> :  <MUIDataTable
        title="List Hasil Pra PON"
        data={rows}
        columns={columnsDisplay}
        options={{options,
          selectableRows: 'none'
        }}
      />}
     
<Dialog
        open={dialogByQuota}
        close={handleClose}
        title={  action + " Lolos By Quota"}
        content={
          <Grid container spacing={1}>
            <Grid item xs={12}> 
          <InputLabel htmlFor="contributor-helper">Kontingen</InputLabel>
              <Select
                label="Pilih Kontingen"
                className={classes.formContainer}
                value={form_contingent_id}
                onChange={(event) => {
                  setform_contingent_id(event.target.value)
                }}
              >
                <MenuItem value={"0"}>Pilih</MenuItem>
                {
                  contingents.map((item, index) => {
                    return (
                      <MenuItem value={item._id}>{item.name}</MenuItem>
                    )
                  })
                }
              </Select> </Grid>
              {(event_qualifications==='Lolos By Name')?<Grid   xs={12}><Grid item xs={12}> 
                  <TextField
                    label="Name"
                    className={classes.formContainer}
                    margin="normal"
                    value={form_fullName}
                    onChange={(event) => setform_fullName(event.target.value)}
                  /> 
                  </Grid>
            <Grid item sm={12}>
            <InputLabel htmlFor="contributor-helper">Jenis</InputLabel>
                <Select
                  className={classes.formContainer}
                  value={form_gender}
                  onChange={(event)=>{
                    setform_gender(event.target.value); 
                  }}
                  inputProps={{
                    name: 'contributor',
                    id: 'contributor-helper',
                  }}
                >
                  <MenuItem value={"Putra"}>Putra</MenuItem>
                  <MenuItem value={"Putri"}>Putri</MenuItem> 
              </Select> 
            </Grid></Grid>:
            <Grid   xs={12}>
               <Grid item sm={12}>
              <TextField
                label="Team Putra"
                className={classes.formContainer}
                margin="normal"
                value={form_quotaTeamPA}
                onChange={(event) => setform_quotaTeamPA(event.target.value)}
              />
            </Grid>  
            <Grid item sm={12}>
              <TextField
                label="Team Putri"
                className={classes.formContainer}
                margin="normal"
                value={form_quotaTeamPI}
                onChange={(event) => setform_quotaTeamPI(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <TextField
                label="Atlet Putra"
                className={classes.formContainer}
                margin="normal"
                value={form_quotaAtlitPA}
                onChange={(event) => setform_quotaAtlitPA(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <TextField
                label="Atlet Putri"
                className={classes.formContainer}
                margin="normal"
                value={form_quotaAtlitPI}
                onChange={(event) => setform_quotaAtlitPI(event.target.value)}
              />
            </Grid> 
              <Grid item xs={12}> 
                  <TextField
                    label="Quota"
                    className={classes.formContainer}
                    margin="normal"
                    value={form_quota}
                    onChange={(event) => setform_quota(event.target.value)}
                  /> 
                  </Grid>
              </Grid>}
              

           
            </Grid>

        }

        cancel={handleClose}
        valueCancel={"Batal"}
        confirm={() => postData1()}
        valueConfirm={"Simpan"}
      />
     <Dialog
        open={openModalDelete}
        close={handleClose}
        title={"Apakah anda yakin menghapus ??"}
        content={
          <Typography>{contingentName}</Typography>
        }
        cancel={handleClose}
        valueCancel ={"Tidak"}
        confirm={()=>deleteData()}
        valueConfirm={"Ya, Hapus"}
      />

      <Dialog
        open={open}
        close={handleClose}
        title={"Form " + action + " Participant"}
        content={
          <Grid container spacing={1}>
              <Grid item xs={12}> 
              <InputLabel htmlFor="contributor-helper">Kontingen</InputLabel>
                  <Select
                    label="Pilih Kontingen"
                    className={classes.formContainer}
                    value={form_contingent_id}
                    onChange={(event) => {
                      setform_contingent_id(event.target.value)
                    }}
                  >
                    <MenuItem value={"0"}>Pilih</MenuItem>
                    {
                      contingents.map((item, index) => {
                        return (
                          <MenuItem value={item._id}>{item.name}</MenuItem>
                        )
                      })
                    }
                  </Select> </Grid>
              <Grid item xs={12}> 
                  <TextField
                    label="Name"
                    className={classes.formContainer}
                    margin="normal"
                    value={form_fullName}
                    onChange={(event) => setform_fullName(event.target.value)}
                  /> 
                  </Grid>
              <Grid item xs={12}> 
                  <TextField
                    label="Quota"
                    className={classes.formContainer}
                    margin="normal"
                    value={form_quota}
                    onChange={(event) => setform_quota(event.target.value)}
                  /> 
                  </Grid>

          </Grid>
        }

        cancel={handleClose}
        valueCancel={"Batal"}
        confirm={() => postData()}
        valueConfirm={"Simpan"}
      />
    </div>


  )
};
export default Prapon;
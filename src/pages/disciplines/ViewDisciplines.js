import React, { useState, useEffect,useRef } from "react";
import {
  Grid,
  Paper,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  Typography,
  TextField,
  InputLabel,
  Avatar,
  Button,
  Select,
  MenuItem,
  FormGroup,
  FormControl,
  FormControlLabel,
  Checkbox,
  TableFooter,
  ListItemSecondaryAction,
  ListItemText,
  Box
} from "@material-ui/core";
import moment from "moment"


import { withStyles } from '@material-ui/core/styles';
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert'; 

import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FilterListIcon from '@material-ui/icons/FilterList';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import Input from '@material-ui/core/Input';

// styles
import './styles.css';
import useStyles from "./styles";
import PageTitle from "../Components/PageTitle/PageTitle";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import axios from "axios";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
import {DropzoneArea} from 'material-ui-dropzone'
import Api from '../../Services/Api'
import StaticVar from '../../Config/StaticVar'
import { List } from "react-virtualized";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
  
const ExpansionPanel = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    backgroundColor: '#fff',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiExpansionPanelDetails);


// table check

//const docRef = useRef();

function createData(name) {
  return { name };
}
function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const headCells = [
  { id: 'name', numeric: false, disablePadding: true, label: 'Kontingen (34)' },
];

function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all kontingen' }}
          />
        </TableCell>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={order}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1">
          {numSelected} Kontingen Dipilih
        </Typography>
      ) : (
          <Typography className={classes.title} variant="h6" id="tableTitle">
            Pilih Kontingen
        </Typography>
        )}

      {numSelected > 0 ? (
        <Tooltip title="Checked">
          <IconButton aria-label="checked">
            <DoneAllIcon />
          </IconButton>
        </Tooltip>
      ) : (
          <Tooltip title="Filter list">
            <IconButton aria-label="filter list">
              <FilterListIcon />
            </IconButton>
          </Tooltip>
        )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStylese = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(0),
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    width: 500,
    minWidth: 0,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));



export default function Disciplines(props) {
  // table check

  const classese = useStylese();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [dense, setDense] = React.useState(false);
  const [contingents, setcontingents] = React.useState([])
  const [panelqualification, setpanelqualification] = React.useState(false)
  const [dataprepopulated, setdataprepopulated] = React.useState([])

  const isSelected = name => selected.indexOf(name) !== -1;

  // expand panel
  const [expanded, setExpanded] = React.useState('#');
  const handleChange = panel => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  // global
  var userDispatch = useUserDispatch();
  var classes = useStyles(); 

  const [quotaEvent, setquotaEvent] = useState([])

  const [participant, setparticipant] = useState([])

  async function getData() {
    // try {
      
      const response1 = await Api.getEventById(id).then(res=>(res.data))
      const response2 = await Api.getContingents().then(res=>(res.data))
      const response3 = await Api.getEventQualification(id).then(res=>(res.data))
      Api.getQuota(id).then(res=>{console.log('quota',id, res.data); setquotaEvent(res.data? res.data.contingent:[])})
      console.log('data detail event', response1[0])
      // if (response1.hasOwnProperty("status")) {
      //   console.log('error')
      //   signExpired(userDispatch, props.history);
      // } else {
        setdiscipline_id(response1[0]._id);
        setdiscipline_name(response1[0].event_name);
        setdataprepopulated(response1[0].prepopulated_data)
        setmax_entries_athlete(response1[0].max_entries_athlete ? response1[0].max_entries_athlete : 0)
        setmax_entries_contingent(response1[0].max_entries_contingent ? response1[0].max_entries_contingent : 0)
        setmax_entries_official(response1[0].max_entries_official ? response1[0].max_entries_official : 0)
        setdataschedules(response1[0].event_schedules);
        setmatch_schedules(response1[0].event_schedules.length>0 ? response1[0].event_schedules[0].match_schedules:[]);
        setarrival_schedules(response1[0].event_schedules.length>0 ? response1[0].event_schedules[0].arrival_schedules:[]);
        setdeparture_schedules(response1[0].event_schedules.length>0 ? response1[0].event_schedules[0].departure_schedules:[]);
        settechnical_meeting_schedules(response1[0].event_schedules.length>0 ? response1[0].event_schedules[0].technical_meeting_schedules:[]);
        setlolos(response1[0].event_qualifications);
        if(response1[0].event_qualifications === "Tanpa Prakualifikasi" || response1[0].event_qualifications === ""){
          setpanelqualification(false)
        }else{
          setpanelqualification(true)
        }
        response2.forEach(item=>{
          item["check"] = false
        })
        setcontingents(response2)
        var datacategory = response1[0].event_categories
        var dataqualification = response3; 
        datacategory.forEach(item=>{
          item["qualification"] = []
          dataqualification.forEach(item2=>{
            if(item._id == item2.category_id){
              item["qualification"] = item2.participant
            }
          })
        })
        
        setdatacategories(datacategory)
        setdatafilter(datacategory)
    //   }
    // }
    // catch (error) {
    //   console.log('error', error)
    //   //signExpired(userDispatch, props.history);
    // }
  }
  
  var [openModalKelasWajib, setopenModalKelasWajib] = useState(false);
  const [dataparticipants, setdataparticipants] = React.useState([])
  async function getDataParticipant(contingent_id, event_id) {
     await Api.getParticipant(contingent_id,event_id).then(res=>
      {  
        var datacategory_require =[]; 
        setdataparticipants(res.data);
        res.data.forEach(item=>{   
         
          if(item.category_require!==undefined)
          {
            item.category_require.forEach(item2=>{   
              if(item2!==undefined) 
              { let dataItem =[];
                dataItem["full_name"]=item.full_name
                dataItem["_id"]=item._id
                dataItem["category_name"]=item2.category_name
                dataItem["category_id"]=item2.category_id
                console.log('dataItem',dataItem)
                datacategory_require.push(dataItem); 
              }
            }) 
          }
        })  
        console.log('datacategory_require',datacategory_require)
        setdatacategory_require(datacategory_require);
      })  
  }
 
  var [txtsearch, settxtsearch ] = useState("");
  var [datafilter, setdatafilter]=useState([])
  
  function SearchFilterFunction(text) {
    var dtBaru = datafilter.filter(item=>{
      const itemData = item.category_name ? item.category_name.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    setdatacategories(dtBaru);
    settxtsearch(text)
    if(text===""){
      setdatacategories(datafilter)
      //setData(datafilter);
    }
  }
  function addDays(date, days) {
    const copy = new Date(Number(date))
    copy.setDate(date.getDate() + days)
    return copy
  }

  const id = props.match.params.id;

  useEffect(() => {
    getData();
  }, []);

  const [overall_schedules, setoverall_schedule] = useState([{ data: '2020-10-08', check: false }, { data: '2020-10-09', check: false }, { data: '2020-10-10', check: false }, { data: '2020-10-11', check: false }, { data: '2020-10-12', check: false }, { data: '2020-10-13', check: false }, { data: '2020-10-14', check: false }, { data: '2020-10-15', check: false }, { data: '2020-10-16', check: false }, { data: '2020-10-17', check: false },
  { data: '2020-10-18', check: false }, { data: '2020-10-19', check: false }, { data: '2020-10-20', check: false }, { data: '2020-10-21', check: false }, { data: '2020-10-22', check: false }, { data: '2020-10-23', check: false }, { data: '2020-10-24', check: false }, { data: '2020-10-25', check: false }, { data: '2020-10-26', check: false }, { data: '2020-10-27', check: false },
  { data: '2020-10-28', check: false }, { data: '2020-10-29', check: false }, { data: '2020-10-30', check: false }, { data: '2020-10-31', check: false }, { data: '2020-11-01', check: false }, { data: '2020-11-02', check: false }, { data: '2020-11-03', check: false }]);

  const [discipline_name, setdiscipline_name] = useState("")
  const [datacategories, setdatacategories] = useState([])
  const [datastack, setdatastack] = useState([])
  const [dataschedules, setdataschedules] = useState({})
  const [match_schedules, setmatch_schedules] = useState([])
  const [arrival_schedules, setarrival_schedules] = useState([])
  const [departure_schedules, setdeparture_schedules] = useState([])
  const [technical_meeting_schedules, settechnical_meeting_schedules] = useState([])
  const [discipline_id, setdiscipline_id] = useState("")

  const [participant_id, setparticipant_id] = useState("")

  const [category_id, setcategory_id] = useState("")
  const [category_name, setcategory_name] = useState(""); 
  const [datacategory_require, setdatacategory_require] = useState([]); 


  var [category_gender, setcategory_gender] = useState("L");
  var [category_team, setcategory_team] = useState(false);
  var [category_team_number, setcategory_team_number] = useState(0);
  var [lolos, setlolos] = useState("")


  const [open, setOpen ] = useState(false);
  const [openAlert,setOpenAlert] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [openModalSchedule, setopenModalSchedule] = useState(false);
  const [action, setaction] = useState("")
  const [openModalTambah, setOpenModalTambah] = useState(false);
  const [openModalAturSemua, setOpenModalAturSemua] = useState(false);
  const [dialogByQuota, setDialogByQuota ] = useState(false);
  const [contingent_id, setContingent_id] = useState("0"); 
  const [chooseCategory, setchooseCategory] = useState({})
  const [min_age, setmin_age] = useState(0)
  const [max_age, setmax_age] = useState(0)
  const [max_entries, setmax_entries] = useState(0)
  const [date_age, setdate_age] = useState()

  var [dataref, setdataref] = useState([]);
  var [scheduletype, setscheduletype] = useState("");
  var [file_document, setfile_document] = useState([]);
  const [max_entries_athlete, setmax_entries_athlete] = useState(0)
  const [max_entries_contingent, setmax_entries_contingent] = useState(0)
  const [max_entries_official, setmax_entries_official] = useState(0)
  const [dataentry_prepopulated, setdataentry_prepopulated] = useState([])
  const [addQuotaContingent, setaddQuotaContingent] = useState(false)
  const [quotaContingent, setquotaContingent] = useState({})


  const handleClickOpen = () => {
    setaction("Tambah");
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setDialogByQuota(false);
    setOpenAlert(false);
    setOpenModalDelete(false);
    setopenModalSchedule(false);
    setOpenModalTambah(false);
    setOpenModalAturSemua(false);
    setopenModalKelasWajib(false);
  };

  function handleChange_files(files){
    console.log(files[0])
    setfile_document(files)
    // this.setState({
    //   files: files
    // });
  }

  function postData() {
    
    if (action === "Tambah") {
      var datacategory = datacategories;
      datacategory.push({
        category_name: category_name,
        category_gender: category_gender,
        category_team: category_team,
        category_team_number: category_team_number
      });

      Api.putNonEvents(discipline_id, {
        event_categories: datacategory
      }).then(res => {
        console.log(res.data)
        getData();
        setOpen(false);
      })
    }
    if (action === "Ubah") {
      // var datacategory = datacategories;
      // datacategory.forEach(item => {
      //   if (item._id === category_id) {
      //     item.category_name = category_name
      //     item.category_gender = category_gender
      //     item.category_team = category_team
      //     item.category_team_number = category_team_number
      //   }
      // })
      // Api.putNonEvents(discipline_id, {
      //   event_categories: datacategory
      // }).then(res => {
      //   console.log('res', res.data)
      //   getData();
      //   setOpen(false);
      // })
      let postdata = { ...chooseCategory, 
        category_name,
        category_gender,
        category_team,
        category_team_number,
        min_age,
        max_age,
        max_entries,
        date_age
      }
      console.log('postdata',props.match.params.id, postdata)
      Api.updatecategory(props.match.params.id, postdata).then(res=>{
        console.log('res', res.data)
        getData();
        setOpen(false);
      })
    }
  }

  function hapusDataCategoriRequire(participant_id,categoryrequire_id) {   
    console.log('data',participant_id,categoryrequire_id)
       Api.deleteParticipantCategoryRequire(participant_id,categoryrequire_id).then(res => {  
        getDataParticipant(contingent_id,id) 
      }) 
  }
  function simpanDataCategoriRequire() { 
    if (action === "Tambah") {
      var data = {}; 
    var category_name="";
    datacategories.forEach(item=>{
      if(item._id===category_id)
      category_name=item.category_name;
    })  
    data.category_name= category_name
    data.category_id= category_id  
       Api.addParticipantCategoryRequire(participant_id,   data  ).then(res => { 
        setopenModalKelasWajib(false); 
        getDataParticipant(contingent_id,id) 
      })
    }  
  }
  function handleClickOpenEdit(data) {
    setOpen(true);
    setaction("Ubah");
    setchooseCategory(data);
    setcategory_name(data.category_name);
    setcategory_id(data._id);
    setcategory_gender(data.category_gender);
    setcategory_team(data.category_team);
    setcategory_team_number(data.category_team_number);
    setmax_entries(data.max_entries)
    setmin_age(data.min_age)
    setmax_age(data.max_age)
    setdate_age(moment(data.date_age).format("YYYY-MM-DD"))
    
  };

  function handleClickOpenDelete(data) {
    setcategory_name(data.category_name);
    setcategory_id(data._id);
    setOpenModalDelete(true);
  };

  function deleteData() {
    var datacategory = datacategories;
    var newdata = [];
    datacategory.forEach(item => {
      if (item._id !== category_id) {
        newdata.push(item);
      }
    })
    Api.putNonEvents(discipline_id, {
      event_categories: newdata
    }).then(res => {
      console.log('res', res.data)
      getData();
      setOpenModalDelete(false);
    })
  };

  function handleClickOpenTambah(data) {
    setcategory_name(data.category_name);
    setcategory_id(data._id);
    setOpenModalTambah(true);
    setaction("Tambah");
  };

  function handleClickOpenAturSemua() {
    setOpenModalAturSemua(true);
    setaction("AturSemua");
  };

  function dropDocument(files){
    //console.log('files',files)
    setfile_document(files)
  }

  //const [file, setfiles] = useState([])

  function saveDocument(files){
    //console.log('save files',files)
    
  }

  const [uploadfile, setuploadfile] = useState(false)

  return (
    <>
      <PageTitle title={discipline_name} button="Kembali" click={() => props.history.push('/app/disciplines')} />
      <Dialog
        open={openModalKelasWajib}
        close={handleClose}
        title={"Form Kelas Wajib"}
        content={
          <Grid container spacing={1}>
            <Grid item sm={12}>
            <Select
                label="Pilih Participant"
                className={classes.formContainer}
                value={participant_id}
                disabled={false}
                onChange={(event) => { 
                  setparticipant_id(event.target.value);
                 //getDataParticipant(event.target.value,id) 
                }}
              >
                <MenuItem value={"0"}>Pilih</MenuItem>
                {
                  dataparticipants.map((item, index) => {
                    return (
                      <MenuItem value={item._id}   label={item.full_name}>{item.full_name}</MenuItem>
                    )
                  })
                }
              </Select> 
            </Grid>
            <Grid item sm={12}>
            <Select
                label="Pilih Kategori"
                className={classes.formContainer}
                value={category_id}
                disabled={false}
                onChange={(event) => {
                  setcategory_id(event.target.value);
                  //setcategory_name(event.target.text);
                 //getDataParticipant(event.target.value,id) 
                }}
              >
                <MenuItem value={"0"}>Pilih</MenuItem>
                {
                  datacategories.map((item, index) => {
                    return (
                      <MenuItem value={item._id} label={item.category_name}>{item.category_name}</MenuItem>
                    )
                  })
                }
              </Select> 
            </Grid>
             
          </Grid>

        }

        cancel={handleClose}
        valueCancel={"Batal"}
        confirm={() => {
          if(category_id==='')
          {alert('Pilih Kategori');return}
          simpanDataCategoriRequire()}}
        valueConfirm={"Simpan"}
      />

      <Dialog
        open={open}
        close={handleClose}
        title={"Form " + action + " Nomor Pertandingan"}
        content={
          <Grid container spacing={1}>
            <Grid item sm={12}>
              <TextField
                label="Nomor Pertandingan"
                className={classes.formContainer}
                margin="normal"
                value={category_name}
                onChange={(event) => setcategory_name(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <Select className={classes.formContainer}
                name="Kategori"
                value={category_gender}
                onChange={(event) => { setcategory_gender(event.target.value) }}>
                <MenuItem value={"L"}>Putra</MenuItem>
                <MenuItem value={"P"}>Putri</MenuItem>
                <MenuItem value={"Mix"}>Campuran</MenuItem>
                <MenuItem value={"Open"}>Open</MenuItem>
              </Select>
            </Grid>
            <Grid item sm={12}>
              <Select className={classes.formContainer}
                name="Perorangan/Non-Perorangan"
                value={category_team}
                onChange={(event) => { setcategory_team(event.target.value) }}>
                <MenuItem value={false}>Perorangan</MenuItem>
                <MenuItem value={true}>Non-Perorangan</MenuItem>
              </Select>
            </Grid>
            {
              category_team ?
                <Grid item sm={12}>
                  <TextField
                    label="Jumlah Orang"
                    className={classes.formContainer}
                    margin="normal"
                    value={category_team_number}
                    onChange={(event) => setcategory_team_number(event.target.value)}
                  />
                </Grid> : null
            }
            <Grid item sm={12}>
              <TextField
                label="Maksimum Entries"
                className={classes.formContainer}
                margin="normal"
                value={max_entries}
                onChange={(event) => setmax_entries(event.target.value)}
              />
            </Grid>
            <Grid item sm={6}>
              <TextField
                label="Batas Bawah Usia"
                className={classes.formContainer}
                margin="normal"
                value={min_age}
                onChange={(event) => setmin_age(event.target.value)}
              />
            </Grid>
            <Grid item sm={6}>
              <TextField
                label="Batas Atas Usia"
                className={classes.formContainer}
                margin="normal"
                value={max_age}
                onChange={(event) => setmax_age(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <TextField
                label="Basis Perhitungan Usia"
                className={classes.formContainer}
                margin="normal"
                type="Date"
                value={date_age}
                onChange={(event) => setdate_age(event.target.value)}
              />
            </Grid>
          </Grid>

        }

        cancel={handleClose}
        valueCancel={"Batal"}
        confirm={() => postData()}
        valueConfirm={"Simpan"}
      />

      <Dialog
        open={addQuotaContingent}
        close={handleClose}
        title={action + " Quota Peserta"}
        content={
          <Grid container spacing={1}>
            <Grid item xs={12}> 
              <InputLabel htmlFor="contributor-helper">Kontingen</InputLabel>
              <Select
                label="Pilih Kontingen"
                className={classes.formContainer}
                value={quotaContingent.contingent_id}
                disabled={action === "Tambah" ? false:  true}
                onChange={(event) => {
                  let quota = {...quotaContingent, contingent_id: event.target.value, contingent_name:  contingents.filter(x=>x._id === event.target.value)[0].name  }
                  setquotaContingent(quota)
                }}
              >
                <MenuItem value={"0"}>Pilih</MenuItem>
                {
                  contingents.map((item, index) => {
                    return (
                      <MenuItem value={item._id} label={item.name}>{item.name}</MenuItem>
                    )
                  })
                }
              </Select> 
            </Grid>
            <Grid item sm={12}>
              <TextField
                label="Laki-laki"
                className={classes.formContainer}
                margin="normal"
                type="Number"
                value={quotaContingent.quotaPa}
                onChange={(event) => {
                  let quota = {...quotaContingent, quotaPa: event.target.value, quota: Number(quotaContingent.quotaPi)+Number(event.target.value)  }
                  setquotaContingent(quota)
                }}
              />
            </Grid>
            <Grid item sm={12}>
              <TextField
                label="Perempuan"
                className={classes.formContainer}
                margin="normal"
                type="Number"
                value={quotaContingent.quotaPi}
                onChange={(event) => {
                  let quota = {...quotaContingent, quotaPi: event.target.value, quota: Number(quotaContingent.quotaPa)+Number(event.target.value)  }
                  setquotaContingent(quota)
                }}
              />
            </Grid>
          </Grid>

        }

        cancel={handleClose}
        valueCancel={"Batal"}
        confirm={() => { 
          if(action=== "Tambah"){
            if(quotaEvent.length>0){
              var data = {
                event_id: id,
                event_name: discipline_name,
                contingent:quotaContingent
              } 
              Api.addQuota(id, data).then(()=>{
                Api.getQuota(id).then(res=>{
                  setquotaEvent(res.data? res.data.contingent:[])
                  setaddQuotaContingent(false)
                })
              })
            }
            else{
              var data = {
                event_id: id,
                event_name: discipline_name,
                contingent:[quotaContingent]
              }
              Api.createQuota(data).then(()=>{
                Api.getQuota(id).then(res=>{
                  setquotaEvent(res.data? res.data.contingent:[])
                  setaddQuotaContingent(false)
                })
              })
            }
          }
          else{
            if(action=== "Ubah"){
              Api.updateQuota(id, quotaContingent.contingent_id, {
                quota: quotaContingent.quota,
                quotaPa: quotaContingent.quotaPa,
                quotaPi : quotaContingent.quotaPi
              }).then(()=>{
                Api.getQuota(id).then(res=>{
                  setquotaEvent(res.data? res.data.contingent:[])
                  setaddQuotaContingent(false)
                })
              })
            }
          }
          
        }}
        valueConfirm={"Simpan"}
      />

      
      <Dialog
        open={dialogByQuota}
        close={handleClose}
        title={"Lolos By Quota"}
        content={
          <Grid container spacing={1}>
            <Grid item xs={12}> 
          <InputLabel htmlFor="contributor-helper">Kontingen</InputLabel>
              <Select
                label="Pilih Kontingen"
                className={classes.formContainer}
                value={contingent_id}
                onChange={(event) => {
                  setContingent_id(event.target.value)
                }}
              >
                <MenuItem value={"0"}>Pilih</MenuItem>
                {
                  contingents.map((item, index) => {
                    return (
                      <MenuItem value={item._id}>{item.name}</MenuItem>
                    )
                  })
                }
              </Select> </Grid>

            <Grid item sm={12}>
              <TextField
                label="Team Putra"
                className={classes.formContainer}
                margin="normal"
                value={category_name}
                onChange={(event) => setcategory_name(event.target.value)}
              />
            </Grid>  
            <Grid item sm={12}>
              <TextField
                label="Team Putri"
                className={classes.formContainer}
                margin="normal"
                value={category_name}
                onChange={(event) => setcategory_name(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <TextField
                label="Atlet Putra"
                className={classes.formContainer}
                margin="normal"
                value={category_name}
                onChange={(event) => setcategory_name(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <TextField
                label="Atlet Putri"
                className={classes.formContainer}
                margin="normal"
                value={category_name}
                onChange={(event) => setcategory_name(event.target.value)}
              />
            </Grid>
          </Grid>

        }

        cancel={handleClose}
        valueCancel={"Batal"}
        confirm={() => postData()}
        valueConfirm={"Simpan"}
      />

      {/* modal delete */}
      <Dialog
        open={openModalDelete}
        close={handleClose}
        title={"Apakah anda yakin menghapus ??"}
        content={
          <Typography>{category_name}</Typography>
        }
        cancel={handleClose}
        valueCancel={"Tidak"}
        confirm={() => deleteData()}
        valueConfirm={"Ya, Hapus"}
      />

      <Dialog
        open={openModalSchedule}
        close={handleClose}
        title={"Pilih Tanggal " + scheduletype}
        content={
          <FormGroup>
            {
              overall_schedules.map((item, index) => {
                return (
                  <FormControlLabel key={index}
                    control={<Checkbox value={item.check} onChange={() => {
                      overall_schedules.forEach(element => {
                        if (element.data === item.data) {
                          element.check = !element.check
                        }
                      });
                      console.log('data', JSON.stringify(overall_schedules))
                      setoverall_schedule(overall_schedules.slice());
                    }}
                    />}
                    label={item.data}
                  />
                )
              })
            }
          </FormGroup>
        }
        cancel={handleClose}
        valueCancel={"Tidak"}
        confirm={() => {
          var datatemp = dataschedules;
          var datastack = [];
          overall_schedules.forEach(item => {
            if (item.check) {
              datastack.push(item.data);
            }
          })
          if (scheduletype === "Pertandingan") {
            datatemp.match_schedules = datastack;
          }
          if (scheduletype === "Kedatangan") {
            datatemp.arrival_schedules = datastack;
          }
          if (scheduletype === "Kepulangan") {
            datatemp.departure_schedules = datastack;
          }
          if (scheduletype === "TM") {
            datatemp.technical_meeting_schedules = datastack;
          }
          Api.putNonEvents(discipline_id,  {
            event_schedules: datatemp
          }).then(res => {
            getData();
            setopenModalSchedule(false);
            overall_schedules.forEach(element => {
              element.check = false
            });
            setoverall_schedule(overall_schedules.slice())

          })
        }}
        valueConfirm={"Simpan"}
      />
      <Dialog
        open={openModalTambah}
        close={handleClose}
        title={"Hasil - "+ category_name}
        content={
          <div className={classese.root}>
            <Paper className={classese.paper}>
              {/* <EnhancedTableToolbar numSelected={selected.length} /> */}
              <div className={classese.tableWrapper}>
                <Table
                  className={classese.table}
                  aria-labelledby="tableTitle" 
                > 
                  <TableBody>
                    {
                      stableSort(contingents, getSorting(order, orderBy))
                        .map((item, index) => {
                          const isItemSelected = isSelected(item.name);
                          const labelid = `enhanced-table-checkbox-${index}`;

                          return (
                            <TableRow
                              key={item.name}
                              hover
                              onClick={() => {
                                contingents.forEach(element => {
                                  if (element._id === item._id) {
                                    element.check = !element.check
                                  }
                                });
                                setcontingents(contingents.slice());
                              }}
                              role="checkbox"
                              aria-checked={item.check}
                              tabIndex={-1}
                              selected={item.check}>
                              <TableCell padding="checkbox">
                                <Checkbox
                                  value ={item.check}
                                  checked={item.check}
                                  inputProps={{ 'aria-labelledby': labelid }}
                                />
                              </TableCell>
                              <TableCell component="th" id={labelid} scope="row" padding="none">
                                <Typography className={classes.txtContentTable}>{item.name}</Typography>
                              </TableCell>
                              <TableCell  >
                                <TextField 
                                  inputProps={{ 'aria-labelledby': labelid }}
                                />
                              </TableCell>
                            </TableRow>
                          );
                        })
                    }
                  </TableBody>
                </Table>
              </div>
            </Paper>
          </div>
        }
        cancel={handleClose}
        valueCancel={"Batal"}
        confirm={()=>{
          var contingent_check = contingents.filter(item=>{
            return item.check == true
          })
          var datacontingent = [];
          contingent_check.forEach(item=>{
            datacontingent.push({
              contingent_id : item._id,
              contingent_name : item.name
            })
          })
          Api.postEventQualification({
            event_id : id,
            category_id : category_id,
            participant: datacontingent
          }).then((response)=>{
            getData();
            handleClose();            
          })
        }}
        valueConfirm={"Simpan"}
      />

      <Dialog
        open={openModalAturSemua}
        close={handleClose}
        title={"Batas Usia"}
        content={
          <TableBody>
            <TableRow>
              <TableCell>Batas Atas</TableCell>
              <TableCell>:</TableCell>
              <TableCell>
                <Input
                  placeholder="Masukan Usia"
                  className={classes.input}
                  inputProps={{
                    'aria-label': 'description',
                  }}
                  style={{ marginLeft: 20 }}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Batas Bawah</TableCell>
              <TableCell>:</TableCell>
              <TableCell>
                <Input
                  placeholder="Masukan Usia"
                  className={classes.input}
                  inputProps={{
                    'aria-label': 'description',
                  }}
                  style={{ marginLeft: 20 }}
                />
              </TableCell>
            </TableRow>
          </TableBody> 
        }
        cancel={handleClose}
        valueCancel={"Tidak"}
        confirm={handleClose}
        valueConfirm={"Simpan"}
      />

      <Grid container spacing={4}>
        <Grid item xs={12}>
          <div>
          <ExpansionPanel square expanded={expanded === 'panel0'} onChange={handleChange('panel0')}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1d-content"
                id="panel1d-header">
                <Typography>Pengaturan Entries</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                  <table>
                    <tr>
                      <td>Maksimum Entrie Per Atlit</td>
                      <td>:</td>
                      <td><TextField type="Number" value={max_entries_athlete} onChange={(e)=>setmax_entries_athlete(e.target.value)}/></td>
                    </tr>
                    <tr>
                      <td>Maksimum Entrie Atlit Per Kontingen</td>
                      <td>:</td>
                      <td><TextField type="Number" value={max_entries_contingent} onChange={(e)=>setmax_entries_contingent(e.target.value)}/></td>
                    </tr>
                    <tr>
                      <td>Maksimum Entrie Official Per Kontingen</td>
                      <td>:</td>
                      <td><TextField type="Number" value={max_entries_official} onChange={(e)=>setmax_entries_official(e.target.value)}/></td>
                    </tr>                    
                    <tr>
                      <td></td>
                      <td></td>
                      <td>
                        <Button variant="contained" color="primary" onClick={()=>{
                          Api.putNonEvents(props.match.params.id, {max_entries_athlete, max_entries_contingent, max_entries_official}).then(res=>{
                            alert('Data sudah diperbaharui')
                          })
                        }}>Update</Button>
                      </td>
                    </tr>
                  </table>
                  <br/>
                  
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel square expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1d-content"
                id="panel1d-header">
                <Typography>Jadwal</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Paper className={classes.paper}>
                  <Table className={classes.table} size="small" aria-label="a dense table">
                    <TableHead>
                      <TableRow>
                        <TableCell style={{ width: 30, textAlign: 'center' }}>No</TableCell>
                        <TableCell style={{ width: 300 }}>Jadwal</TableCell>
                        <TableCell>Tanggal</TableCell>
                        <TableCell style={{ width: 100, textAlign: 'center' }}>Aksi</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell style={{ width: 30, textAlign: 'center' }}>1</TableCell>
                        <TableCell style={{ width: 300 }}>Jadwal Kedatangan</TableCell>
                        <TableCell>
                          {
                            arrival_schedules.map((item, index) => {
                              return (
                                <span key={index}>| {item} |</span>
                              )
                            })
                          }
                        </TableCell>
                        <TableCell style={{ width: 100, textAlign: 'center' }}>
                          <Button className={classes.btnAction} onClick={() => { setopenModalSchedule(true); setscheduletype("Kedatangan") }} ><Typography className={classes.txtAction}>Ubah</Typography></Button>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell style={{ width: 30, textAlign: 'center' }}>2</TableCell>
                        <TableCell style={{ width: 300 }}>Jadwal Kepulangan</TableCell>
                        <TableCell>
                          {
                            departure_schedules.map((item, index) => {
                              return (
                                <span key={index}>| {item} |</span>
                              )
                            })
                          }
                        </TableCell>
                        <TableCell style={{ width: 100, textAlign: 'center' }}>
                          <Button className={classes.btnAction} onClick={() => { setopenModalSchedule(true); setscheduletype("Kepulangan") }} ><Typography className={classes.txtAction}>Ubah</Typography></Button>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell style={{ width: 30, textAlign: 'center' }}>3</TableCell>
                        <TableCell style={{ width: 300 }}>Jadwal Pertandingan</TableCell>
                        <TableCell>
                          {
                            match_schedules.map((item, index) => {
                              return (
                                <span key={index}>| {item} |</span>
                              )
                            })
                          }
                        </TableCell>
                        <TableCell style={{ width: 100, textAlign: 'center' }}>
                          <Button className={classes.btnAction} onClick={() => { setopenModalSchedule(true); setscheduletype("Pertandingan") }} ><Typography className={classes.txtAction}>Ubah</Typography></Button>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell style={{ width: 30, textAlign: 'center' }}>4</TableCell>
                        <TableCell style={{ width: 300 }}>Jadwal Pertemuan Teknik</TableCell>
                        <TableCell>
                          {
                            technical_meeting_schedules.map((item, index) => {
                              return (
                                <span key={index}>| {item} |</span>
                              )
                            })
                          }
                        </TableCell>
                        <TableCell style={{ width: 100, textAlign: 'center' }}>
                          <Button className={classes.btnAction} onClick={() => { setopenModalSchedule(true); setscheduletype("TM") }}  ><Typography className={classes.txtAction}>Ubah</Typography></Button>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </Paper>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel square expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2d-content"
                id="panel2d-header">
                <Typography>Nomor Pertandingan</Typography>
              </ExpansionPanelSummary> <TextField
                label="Cari Nama "
                className={classes.formContainer}
                margin="normal"
                value={txtsearch}
                onChange={(event)=> SearchFilterFunction(event.target.value)}
              />
              <ExpansionPanelDetails> 
                <Grid container spacing={0}>
                  <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                    <Paper className={classes.paper}>
                      <Table className={classes.table} size="small" aria-label="a dense table">
                        <TableHead>
                          <TableRow>
                            <TableCell rowSpan="2" style={{ width: 30, textAlign: 'center' }}>No</TableCell>
                            <TableCell rowSpan="2">Nomor Pertandingan</TableCell>
                            <TableCell rowSpan="2" style={{ width: 150, textAlign: 'center' }}>Jenis Kelamin</TableCell>
                            <TableCell rowSpan="2" style={{ width: 350, textAlign: 'center' }}>Perorangan/Tim</TableCell>
                            <TableCell rowSpan="2" style={{ width: 150, textAlign: 'center' }}>Max Entries</TableCell>
                            <TableCell colSpan="3" style={{ textAlign: 'center' }}>Batasan Usia</TableCell>   
                            <TableCell rowSpan="2"   style={{ textAlign: 'center' }}>Type</TableCell>                           
                            <TableCell rowSpan="2" style={{ width: 150, textAlign: 'center' }}>Aksi</TableCell>
                          </TableRow>
                          <TableRow> 
                            <TableCell style={{ width: 100, textAlign: 'center' }}>Bawah</TableCell>
                            <TableCell style={{ width: 100, textAlign: 'center' }}>Atas</TableCell>
                            <TableCell style={{ width: 150, textAlign: 'center' }}>Basis Tgl</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {
                            datacategories.map((item, index) => {
                              return (
                                <TableRow>
                                  <TableCell style={{ textAlign: 'center' }}>{index + 1}</TableCell>
                                  <TableCell>{item.category_name}</TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>{item.category_gender}</TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>{
                                    item.category_team ? "Tim " + item.category_team_number + " orang" : "Perorangan"
                                  }</TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>{item.max_entries}</TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>{item.min_age}</TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>{item.max_age}</TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>{moment(item.date_age).format("DD-MM-YYYY")}</TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>{item.category_type}</TableCell>
                                  <TableCell style={{ textAlign: 'center'}}>
                                    <Button className={classes.btnAction} onClick={() => handleClickOpenEdit(item)} ><Typography className={classes.txtAction}>Ubah</Typography></Button>
                                    {/* <Button className={classes.btnAction} onClick={() => handleClickOpenDelete(item)} ><Typography className={classes.txtAction}>Hapus</Typography></Button> */}
                                  </TableCell>
                                </TableRow>
                              )
                            })
                          }
                        </TableBody>
                      </Table>
                    </Paper>
                  </Grid>
                  <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                    <Button className={classes.btnAction} onClick={() => handleClickOpen()} >Tambah Nomor</Button>
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel square expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel3d-content"
                id="panel3d-header">
                <Typography>Prakualifikasi</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Grid container spacing={0}>
                  <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                    <Grid container spacing={0} style={{marginBottom:10}}>
                      <Grid item style={{ display: 'flex' }}>
                        <Typography variant="body1" style={{ display: 'flex',marginTop:20, flexDirection: 'column', justifyContent: 'center' }}>Jenis Prakualifikasi :</Typography>
                        <FormControl style={{ marginLeft:20,width: 300 }}>
                          <InputLabel id="demo-customized-select-label">Pilih Filter</InputLabel>
                          <Select
                            value={lolos}
                            onChange={(event) => {
                              setlolos(event.target.value)}}
                          >
                            <MenuItem value="">
                              <em>None</em>
                            </MenuItem>
                            <MenuItem value={"Lolos By Quota"}>Lolos By Quota</MenuItem>
                            <MenuItem value={"Lolos By Number"}>Lolos By Number</MenuItem>
                            <MenuItem value={"Lolos By Quota & By Number"}>Lolos By Quota & By Number</MenuItem>
                            <MenuItem value={"Lolos By Name"}>Lolos By Name</MenuItem>
                            <MenuItem value={"Lolos By Name & By Number"}>Lolos By Name & By Number</MenuItem>
                            <MenuItem value={"Lolos By Name & By Quota"}>Lolos By Name & By Quota</MenuItem>
                            <MenuItem value={"Tanpa Prakualifikasi"}>Tanpa Prakualifikasi</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <Grid item style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', textAlign: 'center' }}>
                        <Button variant="contained" color="secondary" style={{ marginLeft: 20,marginTop:10, width: 80 }} onClick={()=>{
                          Api.putNonEvents(id, {event_qualifications : lolos}).then(response=>{
                            console.log('update data',response.data)
                            setOpenAlert(true);
                            if(lolos === "Tanpa Prakualifikasi" || lolos === ""){
                              setpanelqualification(false)
                            }else{
                              setpanelqualification(true)
                            }
                            getData();
                          })
                        }}>
                          Simpan
                        </Button>
                        <Button variant="contained" color="secondary" style={{ marginLeft: 20,marginTop:10, width: 120 }} onClick={()=>{ 
                          props.history.push('/app/prapon/'+props.match.params.id+'/-')
                        }}>
                          Input Data
                        </Button>
                        <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
                      <Alert onClose={handleClose} color="success">
                        This is a success message!
                      </Alert>
                    </Snackbar>
   
                      </Grid>
                    </Grid>
                  </Grid>
                  {
                    panelqualification ?
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                      <Paper className={classes.paper}>
                        <Table className={classes.table} size="small" aria-label="a dense table">
                          <TableHead>
                            <TableRow>
                              <TableCell style={{ width: 30, textAlign: 'center' }}>No</TableCell>
                              <TableCell style={{ width: 300 }}>Nomor Pertandingan</TableCell>
                              <TableCell>Hasil Prakualifikasi</TableCell>
                              <TableCell style={{ width: 100, textAlign: 'center' }}>Aksi</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {
                              datacategories.map((item, index) => {                                
                                return (
                                  <TableRow>
                                    <TableCell style={{ width: 30, textAlign: 'center' }}>{index + 1}</TableCell>
                                    <TableCell style={{ width: 30 }}>{item.category_name}</TableCell>
                                    <TableCell style={{ width: 300 }}>
                                      {
                                        item.qualification.map((item2,index2)=>{
                                          return(
                                          <Typography>{index2+1 +'. '+item2.contingent_name} {item2.full_name !=="" && item2.full_name  ? " => atasnama "+item2.full_name : ""}</Typography> 
                                            
                                          )
                                        })
                                      }
                                    </TableCell>
                                    <TableCell style={{ width: 100, textAlign: 'center' }}>
                                      <Button className={classes.btnAction} onClick={() => handleClickOpenTambah(item)} ><Typography className={classes.txtAction}>Tambah</Typography></Button>
                                      <Button className={classes.btnAction} onClick={() => {props.history.push('/app/prapon/'+props.match.params.id+'/'+item._id)}} ><Typography className={classes.txtAction}>Lihat</Typography></Button>
                                       
                                    </TableCell>
                                  </TableRow>
                                )
                              })
                            }
                          </TableBody>
                        </Table>
                      </Paper>
                    </Grid>    : null               
                  }
                  
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel square expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel3d-content"
                id="panel3d-header">
                <Typography>Kuota Peserta</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Grid container spacing={0}>
                  <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                    <Paper className={classes.paper}>
                      <Button className={classes.btnAction} onClick={()=>{setaddQuotaContingent(true); setaction("Tambah")}}>Tambah Kuota</Button>
                      <Table className={classes.table} size="small" aria-label="a dense table">
                        <TableHead style={{backgroundImage:'linear-gradient(to right, #bf272b , #601416 )'}}>
                          <TableRow>
                            <TableCell  style={{ width: 30,color:'white', textAlign: 'center' }}>No</TableCell>
                            <TableCell  style={{ color:'white' }}>Kontingen</TableCell>
                            <TableCell style={{ width: 100, color:'white', textAlign: 'center' }}>Laki-laki</TableCell>
                            <TableCell style={{ width: 100, color:'white', textAlign: 'center' }}>Perempuan</TableCell>
                            <TableCell style={{ width: 100, color:'white', textAlign: 'center' }}>Total</TableCell>
                            <TableCell  style={{ width: 300,color:'white', textAlign: 'center' }}>Aksi</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {
                            quotaEvent.map((item, index) => {                              
                              return (
                                <TableRow>
                                  <TableCell style={{ width: 30, textAlign: 'center' }}>{index + 1}</TableCell>
                                  <TableCell>{item.contingent_name}</TableCell>
                                  <TableCell style={{ width: 100, textAlign:'center' }}>
                                  {item.quotaPa}
                                  </TableCell>
                                  <TableCell style={{ width: 100, textAlign:'center' }}>
                                  {item.quotaPi}
                                  </TableCell>
                                  <TableCell style={{ width: 100, textAlign:'center' }}>
                                  {item.quota}
                                  </TableCell>
                                  <TableCell style={{ width: 300, textAlign:'center' }}>
                                    <Button className={classes.btnAction} onClick={()=>{setquotaContingent(item);setaddQuotaContingent(true); setaction("Ubah")}} ><Typography className={classes.txtAction}>Ubah</Typography></Button>
                                    <Button className={classes.btnAction} onClick={()=>{
                                      Api.deleteQuota(id, item.contingent_id).then(()=>{
                                        if(quotaEvent.length>1){
                                          Api.getQuota(id).then(res=>setquotaEvent(res.data? res.data.contingent:[]))
                                        }
                                        else{
                                          setquotaEvent([])
                                        }
                                      })
                                      
                                    }}><Typography className={classes.txtAction}>Hapus</Typography></Button>
                                  </TableCell>
                                </TableRow>
                              )
                            })
                          }
                        </TableBody>
                      </Table>
                    </Paper>
                  </Grid> 
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel square expanded={expanded === 'panel5'} onChange={handleChange('panel5')}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel4d-content"
                id="panel4d-header">
                <Typography>Batasan Usia</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Grid container spacing={0}>
                  <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                    <Grid container spacing={0}>
                      <Grid item style={{ display: 'flex' }}>
                        <Typography variant="body1" style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>Batas Usia :</Typography>
                        <FormControl style={{ margin: '10px', width: 200 }}>
                          <InputLabel id="demo-customized-select-label">Pilih Filter</InputLabel>
                          <Select
                            labelid="demo-customized-select-label"
                            id="demo-customized-select"
                            value={lolos}
                            onChange={(event) => setlolos(event.target.value)}
                          >
                            <MenuItem value="">
                              <em>None</em>
                            </MenuItem>
                            <MenuItem value={"LolosByNumber"}>Bebas</MenuItem>
                            <MenuItem value={"LolosByName"}>Batas Atas</MenuItem>
                            <MenuItem value={"TanpaPrakualifikasi"}>Batas Bawah</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <Grid item style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', textAlign: 'center' }}>
                        <Button variant="contained" color="secondary" style={{ marginLeft: 20, width: 80 }} onClick={()=>{
                          console.log('')
                        }}>
                          Simpan
                        </Button>
                      </Grid>
                      <Grid item style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', textAlign: 'center' }}>
                        <Button variant="contained" color="primary" style={{ marginLeft: 20, width: 200 }} onClick={() => handleClickOpenAturSemua()}>
                          Set Batasan Usia
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                    <Paper className={classes.paper}>
                      <Table className={classes.table} size="small" aria-label="a dense table">
                        <TableHead>
                          <TableRow>
                            <TableCell style={{ width: 30, textAlign: 'center' }}>No</TableCell>
                            <TableCell>Nomor Pertandingan</TableCell>
                            <TableCell style={{ width: 250, textAlign: 'center' }}>Batas Bawah</TableCell>
                            <TableCell style={{ width: 250, textAlign: 'center' }}>Batas Atas</TableCell>
                            <TableCell style={{ width: 150, textAlign: 'center' }}>Aksi</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {
                            datacategories.map((item, index) => {
                              return (
                                <TableRow>
                                  <TableCell style={{ textAlign: 'center' }}>
                                    <Checkbox
                                      value ={item.check}
                                      checked={item.check}
                                    />
                                  </TableCell>
                                  <TableCell>{item.category_name}</TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>
                                    {
                                      item.start_age
                                    }
                                  </TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>
                                    {
                                      item.end_age
                                    }
                                  </TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>
                                    <Button className={classes.btnAction}><Typography className={classes.txtAction}>Ubah</Typography></Button>
                                   
                                  </TableCell>
                                </TableRow>
                              )
                            })
                          }
                        </TableBody>
                      </Table>
                    </Paper>
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel square expanded={expanded === 'panel6'} onChange={handleChange('panel6')}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel4d-content"
                id="panel4d-header">
                <Typography>Dokumen</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                 <div style={{clear:'both'}}>
                    
                    {
                      uploadfile ? 
                      <>
                        <DropzoneArea 
                            onChange={handleChange_files}
                            acceptedFiles={['application/*']}
                            showPreviews={false}
                            showFileNames={true}
                            showAlerts={false}
                            onDrop={dropDocument.bind(this)}
                            maxFileSize={15000000}
                            filesLimit={1}
                        />
                        <br/> <br/>
                        <Button variant="contained" color="secondary" onClick={()=>{
                          console.log('files', file_document)
                          let send = new FormData();
                          send.append('document', file_document);
                          Api.postUploadEvents(send).then(response=>{
                            console.log('upload', response.data)
                          })
                          }}
                          >
                          Kirim
                        </Button>
                      </> : 
                      <table>
                        <tr>
                          <td>SK Cabor</td>
                          <td><Button variant="contained" color="secondary" onClick={()=>setuploadfile(true)}>Upload</Button></td>
                        </tr>
                        <tr>
                          <td>THB</td>
                          <td><Button variant="contained" color="secondary" onClick={()=>setuploadfile(true)}>Upload</Button></td>
                        </tr>
                      </table>
                    }
                    
                  </div>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel square expanded={expanded === 'panel7'} onChange={handleChange('panel7')}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel4d-content"
                id="panel4d-header">
                <Typography>Pre-populated Data</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Grid container spacing={0}>
                  <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                    <Paper className={classes.paper}>
                      {/* <Table className={classes.table} size="small" aria-label="a dense table">
                        <TableHead>
                          <TableRow>
                            <TableCell style={{ width: 30, textAlign: 'center' }}>No</TableCell>
                            <TableCell style={{ width: 250, textAlign: 'left' }}>Nomor Pertandingan</TableCell> 
                            <TableCell style={{ textAlign: 'left' }}>Field Data</TableCell>      
                          </TableRow>
                        </TableHead>
                        <TableBody> */}
                          {
                            ["Perorangan", "Non Perorangan"].map((item, index) => {
                              let datalist = {}
                              let dropdown_source = false
                              if(item === "Perorangan"){
                                if(dataprepopulated && dataprepopulated.filter(x=>x.category_team == false).length>0){
                                  datalist = dataprepopulated.filter(x=>x.category_team == false)[0]
                                }
                              }
                              else{
                                if(dataprepopulated && dataprepopulated.filter(x=>x.category_team == true).length>0){
                                  datalist = dataprepopulated.filter(x=>x.category_team == true)[0]
                                }
                              }
                              
                              return (
                                <>
                                  <Box style={{lineHeight:3, backgroundColor:'#CFCFCF',textAlign:'center'}}>
                                    {item}
                                  </Box>                                  
                                  <br/>
                                  <Table size="small" >
                                      <TableHead>
                                        <TableRow>
                                          <TableCell style={{ width: 30, textAlign: 'center' }}>No</TableCell>
                                          <TableCell style={{ width: 250, textAlign: 'left' }}>Nama Field</TableCell> 
                                          <TableCell style={{ width: 250, textAlign: 'left' }}>Text Field</TableCell>
                                          <TableCell style={{ width: 250, textAlign: 'left' }}>Jenis Field</TableCell> 
                                          {
                                            item === "Non Perorangan" ? 
                                            <TableCell style={{ textAlign: 'left' }}>Group/Individual Entries</TableCell> 
                                            : null
                                          }
                                          <TableCell style={{ width: 100, textAlign: 'center' }}>Aksi</TableCell>   
                                        </TableRow>
                                      </TableHead>
                                      <TableFooter>
                                        <TableRow>
                                          <TableCell style={{ width: 30, textAlign: 'center' }}></TableCell>
                                          <TableCell style={{ textAlign: 'left' }}>
                                            <TextField label="Nama Field" style={{width:'100%'}} value={datalist.fieldName} onChange={(e)=> { datalist = {...datalist, fieldName : e.target.value}}} />
                                          </TableCell> 
                                          <TableCell style={{ textAlign: 'left' }}>
                                            <TextField label="Text Field" style={{width:'100%'}} value={datalist.fieldText} onChange={(e)=> { datalist = {...datalist, fieldText : e.target.value}}} />
                                          </TableCell>
                                          <TableCell style={{ textAlign: 'left' }}>
                                            <Select style={{width:'100%'}}  value={datalist.fieldType} onChange={(e)=> { 
                                              datalist = {...datalist, fieldType : e.target.value}
                                            }}>
                                              <MenuItem value="Textbox">Textbox</MenuItem>
                                              <MenuItem value="Dropdown">Dropdown</MenuItem>
                                              <MenuItem value="Checkbox">Checkbox</MenuItem>
                                              <MenuItem value="Dropdown Official">Dropdown Official</MenuItem>
                                            </Select>
                                            <br/>
                                            {
                                               <p>fieldType: {datalist.fieldType}</p>
                                            }
                                            {
                                              dropdown_source ?
                                              <div>
                                                <List>
                                                  <ListItemText primary="asdasdfasda" />
                                                  <ListItemSecondaryAction>
                                                    <IconButton edge="end" aria-label="delete"/>
                                                  </ListItemSecondaryAction>
                                                </List>
                                                <TextField />
                                               
                                              </div> : null
                                            }
                                            
                                            
                                          </TableCell>
                                          {
                                            item === "Non Perorangan" ? 
                                            <TableCell style={{ textAlign: 'left' }}>
                                              <FormControlLabel
                                                control={
                                                  <Checkbox
                                                    // checked={state.checkedB}
                                                    // onChange={handleChange}
                                                    name="checkedB"
                                                    color="primary"
                                                  />
                                                }
                                                label="Group Entries"
                                              />
                                            </TableCell> 
                                            : null
                                          }
                                          <TableCell style={{ textAlign: 'center' }}>
                                            <Button variant="contained" size="small" onClick={()=>{
                                              console.log('datalist', datalist)
                                              console.log('dropdown_source', dropdown_source);
                                            }}>Simpan</Button>
                                          </TableCell>  
                                        </TableRow>
                                      
                                      </TableFooter>
                                    </Table>
                                  
                                </>
                              )
                            })
                          }
                    </Paper>
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel square expanded={expanded === 'panel8'} onChange={handleChange('panel8')}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel4d-content"
                id="panel4d-header">
                <Typography>Kelas Wajib</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Grid container spacing={0}>
                <Grid item xl={6} lg={6} md={6} sm={6} xs={6}> 
                  
                <Select
                label="Pilih Kontingen"
                className={classes.formContainer}
                value={contingent_id}
                disabled={false}
                onChange={(event) => {
                  setContingent_id(event.target.value);
                  getDataParticipant(event.target.value,id) 
                }}
              >
                <MenuItem value={"0"}>Pilih</MenuItem>
                {
                  contingents.map((item, index) => {
                    return (
                      <MenuItem value={item._id} label={item.name}>{item.name}</MenuItem>
                    )
                  })
                }
              </Select> 
                    </Grid>
                    
                  <Grid item xl={6} lg={6} md={6} sm={6} xs={6}>
                    <Button className={classes.btnAction}  onClick={()=>{
                                      setaction('Tambah')
                                      setopenModalKelasWajib(true)}}>Tambah Kelas Wajib</Button>
                  </Grid>
                  <Grid item xl={12} lg={12} md={12} sm={12} xs={12}> 
                  
                  <Paper className={classes.paper}>
                      <Table className={classes.table} size="small" aria-label="a dense table">
                        <TableHead>
                          <TableRow>
                            <TableCell style={{ width: 30, textAlign: 'center' }}>No</TableCell> 
                            <TableCell style={{ width: 250, textAlign: 'center' }}>Nama</TableCell>
                            <TableCell style={{ width: 250, textAlign: 'center' }}>Kelas Wajib</TableCell>
                            <TableCell style={{ width: 150, textAlign: 'center' }}>Aksi</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {
                            datacategory_require.map((item, index) => {
                              return (
                                <TableRow> 
                                <TableCell style={{ width: 30, textAlign: 'center' }}>{index + 1}</TableCell>
                                  <TableCell>{item.full_name}</TableCell>
                                 
                                  <TableCell style={{ textAlign: 'center' }}>
                                    {
                                      item.category_name
                                    }
                                  </TableCell>
                                  <TableCell style={{ textAlign: 'center' }}>
                                    <Button className={classes.btnAction} onClick={()=>{
                                      //setaction('Tambah')
                                      hapusDataCategoriRequire(item._id,item.category_id)
                                      }}><Typography className={classes.txtAction}>Hapus</Typography></Button>
                                   
                                  </TableCell>
                                </TableRow>
                              )
                            })
                          }
                        </TableBody>
                      </Table>
                    </Paper>
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
        </Grid>
      </Grid>
    </>
  );
}

import React, { useState, useEffect, useRef } from "react";
import {
  Grid,
  Paper,
  Button,
  Select,
  MenuItem,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Typography,
} from "@material-ui/core";
import Axios from "axios";
import moment from "moment";
import PrintIcon from "@material-ui/icons/Print";
import Dialog from "../Components/Dialog/Dialog";
import PublishIcon from "@material-ui/icons/Publish";

import ReactToPrint, { useReactToPrint } from "react-to-print";

import StaticVar from "../../Config/StaticVar";

// styles
// import useStyles from "./styles";

import PageTitle from "../Components/PageTitle/PageTitle";

import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function Helpdesk2(props) {
  //   var classes = useStyles();
  const componentRef = useRef();

  const url = StaticVar.Base_Url;
  const urlservices = StaticVar.URL_REQUEST;
  const urlrepo = StaticVar.URL_REPO_REQUEST;
  const token = localStorage.getItem("token");
  const headers = {
    "Content-Type": "application/json",
    "x-access-token": token,
  };

  const [loading, setLoading] = useState(false);
  const [openstatus, setOpenStatus] = useState(false);
  const [opencategory, setOpenCategory] = useState(false);
  const [datarequest, setDatarequest] = useState([]);
  const [alldata, setAlldata] = useState([]);
  const [eventlist, setEventList] = useState([]);
  const [contingent, setContingent] = useState([]);
  const [users, setUsers] = useState([]);

  const [selectedcontingent, setSelectedContingent] = useState("0");
  const [selectedevent, setSelectedEvent] = useState("0");
  const [statusselected, setStatusSelected] = useState("0");
  const [selectedcategory, setSelectedCategory] = useState("0");

  const [idupdate, setidupdate] = useState("");
  const [validatedstatus, setValidatedStatus] = useState("");
  const [laststatus, setLastStatus] = useState("");
  const [checkedlaststatus, setCheckedLaststatus] = useState({
    solved: false,
  });
  const { solved, diterima, ditolak, undereview } = checkedlaststatus;
  const [checkedstatus, setCheckedStatus] = useState({
    BU: false,
    DOM: false,
    TAA: false,
    TAC: false,
    TAS: false,
    TAP: false,
    GAA: false,
    GAC: false,
    GAP: false,
    GAI: false,
    TOA: false,
    GOA: false,
    UA: false,
    UAF: false,
  });
  const { BU, DOM, TAA, TAC, TAS, TAP, GAA, GAC, GAP, GAI, TOA, GOA, UA, UAF } =
    checkedstatus;

  const statusitem = [
    {
      _id: "BU",
      name: "BU",
    },
    {
      _id: "DOM",
      name: "DOM",
    },
    {
      _id: "TAA",
      name: "TA-A",
    },
    {
      _id: "TAC",
      name: "TA-C",
    },
    {
      _id: "TAS",
      name: "TA-S",
    },
    {
      _id: "TAP",
      name: "TA-P",
    },
    {
      _id: "GAA",
      name: "GA-A",
    },
    {
      _id: "GAC",
      name: "GA-C",
    },
    {
      _id: "GAP",
      name: "GA-P",
    },
    {
      _id: "GAI",
      name: "GA-I",
    },
    {
      _id: "TOA",
      name: "TO-A",
    },
    {
      _id: "GOA",
      name: "GO-A",
    },
    {
      _id: "UA",
      name: "UA-F",
    },
  ];

  const handleChecklist = (event) => {
    // console.log("event", event.target.name);
    setValidatedStatus(event.target.name);
    setCheckedStatus({ [event.target.name]: event.target.checked });
  };

  const handleLastStatusCheclist = (event) => {
    // console.log("event", event.target.name);
    setLastStatus(event.target.name);
    setCheckedLaststatus({ [event.target.name]: event.target.checked });
  };

  //   const getEvents = () => {
  //     Axios.get(url + "/contingents").then((res) => {
  //     //   console.log("cabor", res.data);
  //     setContingent(res.data);
  //     });
  //   };

  //   const docRef = useRef(null);

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  var getUsers = async () => {
    // console.log('data');
    try {
      if (localStorage.getItem("user_access") === "admin") {
        Axios.get(url + "/private/users/getAllUserKontingen", {
          headers,
        }).then((res) => {
          // console.log("user", res.data);
          setUsers(res.data);
        });
      }
    } catch (error) {
      console.log("err", error);
      //   signExpired(userDispatch, props.history);
    }
  };

  const getContingents = () => {
    Axios.get(url + "/contingents")
      .then((res) => {
        // console.log("setContingent", res.data);
        setContingent(res.data);
      })
      .catch((err) => {
        console.log("err contingnet", err);
      });
  };

  const getEvents = (e) => {
    Axios.get(url + "/events")
      .then((res) => {
        // console.log("cabor", res.data);
        setEventList(res.data);
      })
      .catch((err) => {
        console.log("err events", err);
      });
  };

  const getRequest = () => {
    console.log("get request");
    setLoading(true);
    Axios.get(urlservices + "/request?type=cabor").then((res) => {
      // console.log("tes with params", res.data);
      setDatarequest(res.data);
      setAlldata(res.data);
      setLoading(false);
    });
  };

  const filterData = () => {
    if (selectedcontingent !== "0" && selectedevent !== "0") {
      let fdata = alldata.filter(
        (x) =>
          x.contingent_id === selectedcontingent &&
          x.event_id === selectedevent,
      );
      setDatarequest(fdata);
    } else if (selectedcontingent !== "0" && selectedevent === "0") {
      let fdata = alldata.filter((x) => x.contingent_id === selectedcontingent);
      setDatarequest(fdata);
    } else if (selectedcontingent === "0" && selectedevent !== "0") {
      let fdata = alldata.filter((x) => x.event_id === selectedevent);
      setDatarequest(fdata);
    } else if (selectedcategory !== "0") {
      let fdata = alldata.filter((x) => x.status === selectedcategory);
      setDatarequest(fdata);
    } else {
      setDatarequest(alldata);
    }
    // console.log(fdata);
  };

  useEffect(() => {
    getRequest();
    getEvents();
    getUsers();
    getContingents();
    // getCountRequest();
  }, []);

  return (
    <>
      <PageTitle title="Helpdesk Tahap 2" />

      <Grid container justify="space-between">
        <Grid item sm={12} md={12} lg={10}>
          <p>
            Kontingen :
            <Select
              value={selectedcontingent}
              onChange={(event) => {
                setSelectedContingent(event.target.value);
                // getEvents(event.target.value);
              }}
              //   value={choosecontingent_id}
            >
              <MenuItem value={"0"}>Pilih Kontingen</MenuItem>
              {contingent.map((item, index) => {
                return <MenuItem value={item._id}>{item.name}</MenuItem>;
              })}
            </Select>
            {/* <div style={{ marginRight: "1%" }} /> */}
            Cabor/Disiplin :
            <Select
              style={{ marginRight: 5 }}
              value={selectedevent}
              onChange={(event) => {
                setSelectedEvent(event.target.value);
              }}
            >
              <MenuItem value={"0"}>Pilih Cabor/Disiplin</MenuItem>
              {eventlist.map((item, index) => {
                return (
                  <MenuItem key={index} value={item._id}>
                    {item.event_name}
                  </MenuItem>
                );
              })}
            </Select>
            Status:
            <Select
              style={{ marginRight: 5 }}
              value={selectedcategory}
              onChange={(event) => {
                setSelectedCategory(event.target.value);
              }}
            >
              <MenuItem value={"0"}>Pilih Kategori</MenuItem>
              {statusitem.map((item, index) => {
                return (
                  <MenuItem key={index} value={item._id}>
                    {item.name}
                  </MenuItem>
                );
              })}
            </Select>
            <Button
              onClick={filterData}
              size="small"
              variant="contained"
              color="secondary"
            >
              Load Data
            </Button>
          </p>
        </Grid>
        <Grid
          sm={12}
          md={12}
          lg={2}
          style={{ display: "flex", justifyContent: "flex-end" }}
        >
          {" "}
          <Button
            onClick={handlePrint}
            variant="contained"
            color="primary"
            size="small"
          >
            <PrintIcon />
            <Typography>Cetak</Typography>
            {/* Cetak */}
          </Button>
        </Grid>
        <Grid item xs={12} md={12} lg={12} xl={12}>
          <ExcelFile
            element={
              <Button
                size="small"
                style={{
                  padding: 7,
                  marginRight: "1%",
                }}
                variant="contained"
                color="secondary"
              >
                <PublishIcon style={{ fontSize: 20, marginRight: 5 }} />
                <Typography style={{ fontSize: 14 }}>Export</Typography>
              </Button>
            }
          >
            <ExcelSheet
              data={datarequest.map((item, index) => {
                let eventname = eventlist.filter(
                  (x) => x._id === item.event_id,
                );

                let createdby = users.filter((x) => x._id === item.created_by);

                return {
                  no: index + 1,
                  full_name: item.full_name,
                  nik: item.nik,
                  contingent_name:
                    createdby.length > 0 ? createdby[0].contingent_name : "",
                  participant_type: item.participant_type,
                  event_name:
                    eventname.length > 0 ? eventname[0].event_name : "",
                  problem: item.problem,
                  usulan: item.usulan,
                  category: item.status,
                  status: item.lastStatus,
                  // document: item.document.map((x) => urlrepo + x.filename),
                };
              })}
              name="Daftar List Helpdesk"
            >
              <ExcelColumn label="No" value="no" />
              <ExcelColumn label="Nama Lengkap" value="full_name" />
              <ExcelColumn label="NIK" value="nik" />
              <ExcelColumn label="Kontingen" value="contingent_name" />
              <ExcelColumn label="Cabor/Disiplin" value="event_name" />
              <ExcelColumn label="Partisipan" value="participant_type" />
              <ExcelColumn label="Masalah" value="problem" />
              <ExcelColumn label="Usulan" value="usulan" />
              <ExcelColumn label="Kategori" value="category" />
              <ExcelColumn label="Status" value="status" />
              {/* <ExcelColumn label="Dokumen" value="document" /> */}
            </ExcelSheet>
          </ExcelFile>
        </Grid>
      </Grid>

      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Paper>
            <table
              style={{ width: "100%", fontSize: 12, marginBottom: 10 }}
              ref={componentRef}
            >
              <thead style={{ background: "#bf272b", color: "white" }}>
                <tr>
                  <th style={{ textAlign: "center", width: 20, padding: 8 }}>
                    No
                  </th>
                  <th style={{ textAlign: "left", padding: 8, width: 200 }}>
                    Nama Lengkap
                  </th>

                  <th style={{ width: 100, padding: 8, textAlign: "center" }}>
                    Cabang Olahraga/Disiplin
                  </th>
                  <th style={{ textAlign: "center", width: 200, padding: 8 }}>
                    Masalah
                  </th>
                  <th style={{ textAlign: "left", width: 250, padding: 8 }}>
                    Usulan
                  </th>
                  {/* <th style={{ textAlign: "left", width: 100, padding: 8 }}>
                    Tanggal
                  </th> */}
                  <th style={{ textAlign: "left", width: 150, padding: 8 }}>
                    Updated By
                  </th>
                  <th style={{ textAlign: "left", width: 100, padding: 8 }}>
                    Dokumen
                  </th>
                  <th style={{ textAlign: "center", width: 50, padding: 8 }}>
                    Qualification
                  </th>
                  <th style={{ textAlign: "center", width: 50, padding: 8 }}>
                    Kategori
                  </th>
                  <th style={{ textAlign: "center", width: 50, padding: 8 }}>
                    Aksi
                  </th>
                </tr>
              </thead>
              <tbody class="table-stripped">
                {datarequest.map((item, index) => {
                  let eventname = eventlist.filter(
                    (x) => x._id === item.event_id,
                  );

                  let createdby = users.filter(
                    (x) => x._id === item.created_by,
                  );

                  // console.log("cerate", createdby);

                  let updatedby = users.filter(
                    (x) => x._id === item.updated_by,
                  );
                  //   console.log("tes", createdby);
                  return (
                    <tr>
                      <td style={{ textAlign: "center", padding: 8 }}>
                        {index + 1}
                      </td>
                      <td style={{ padding: 8 }}>
                        <b>{item.full_name}</b>
                        <br />
                        <span>NIK : {item.nik}</span>
                        <br />
                        <span>
                          Kontingen :{" "}
                          {createdby.length > 0
                            ? createdby[0].contingent_name
                            : null}{" "}
                        </span>
                      </td>
                      <td style={{ textAlign: "center", padding: 8 }}>
                        {eventname.length > 0 ? eventname[0].event_name : null}
                      </td>
                      <td style={{ textAlign: "left", padding: 8 }}>
                        {item.problem}
                      </td>
                      <td style={{ textAlign: "left", padding: 8 }}>
                        {item.usulan}
                      </td>
                      {/* <td style={{ textAlign: "center", padding: 8 }}>
                        <p>
                          Dibuat: {moment(item.created_at).format("DD-MM-YYYY")}
                        </p>
                        <p>
                          Update: {moment(item.updated_at).format("DD-MM-YYYY")}
                        </p>
                      </td> */}
                      <td style={{ textAlign: "left", padding: 8 }}>
                        <p>
                          Dibuat:{" "}
                          {createdby.length > 0 ? createdby[0].name : null}
                        </p>
                        <p>{moment(item.created_at).format("DD-MM-YYYY")}</p>
                        <p>
                          Update:{" "}
                          {updatedby.length > 0 ? updatedby[0].name : null}
                        </p>
                        <p>{moment(item.updated_at).format("DD-MM-YYYY")}</p>
                      </td>
                      <td style={{ textAlign: "left", padding: 8 }}>
                        {item.document.map((x) => (
                          <>
                            <p>
                              {x.name} :{" "}
                              <a href={urlrepo + x.filename}>
                                {urlrepo + x.filename}
                              </a>
                            </p>
                          </>
                        ))}
                      </td>
                      <td style={{ textAlign: "left", padding: 8 }}>
                        {eventname.length > 0
                          ? eventname[0].event_qualifications
                          : null}
                      </td>
                      <td style={{ textAlign: "left", padding: 8 }}>
                        <p>kategori: {item.status}</p>
                        <p>status: {item.lastStatus}</p>
                      </td>

                      <td style={{ textAlign: "center" }}>
                        <Button
                          size="small"
                          variant="contained"
                          color="secondary"
                          onClick={() => {
                            setOpenCategory(true);
                            setidupdate(item._id);
                          }}
                        >
                          Kategori
                        </Button>

                        <Button
                          size="small"
                          variant="contained"
                          color="primary"
                          onClick={() => {
                            setOpenStatus(true);
                            setidupdate(item._id);
                            // setLastStatus({ [item.lastStatus]: true });
                          }}
                        >
                          Status
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </Paper>
        </Grid>
      </Grid>

      {/* <Grid container>
        <Grid item>
          
        </Grid>
      </Grid> */}

      <Dialog
        open={opencategory}
        close={() => setOpenCategory(false)}
        title="Ubah Status"
        content={
          <>
            <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox checked={BU} onChange={handleChecklist} name="BU" />
                }
                label="BU"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={DOM}
                    onChange={handleChecklist}
                    name="DOM"
                  />
                }
                label="DOM"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={TAA}
                    onChange={handleChecklist}
                    name="TAA"
                  />
                }
                label="TA-A"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={TAC}
                    onChange={handleChecklist}
                    name="TAC"
                  />
                }
                label="TA-C"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={TAP}
                    onChange={handleChecklist}
                    name="TAP"
                  />
                }
                label="TA-P"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={TAS}
                    onChange={handleChecklist}
                    name="TAS"
                  />
                }
                label="TA-S"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={GAA}
                    onChange={handleChecklist}
                    name="GAA"
                  />
                }
                label="GA-A"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={GAC}
                    onChange={handleChecklist}
                    name="GAC"
                  />
                }
                label="GA-C"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={GAP}
                    onChange={handleChecklist}
                    name="GAP"
                  />
                }
                label="GA-P"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={GAI}
                    onChange={handleChecklist}
                    name="GAI"
                  />
                }
                label="GA-I"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={TOA}
                    onChange={handleChecklist}
                    name="TOA"
                  />
                }
                label="TO-A"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={GOA}
                    onChange={handleChecklist}
                    name="GOA"
                  />
                }
                label="GO-A"
              />
              <FormControlLabel
                control={
                  <Checkbox checked={UA} onChange={handleChecklist} name="UA" />
                }
                label="UA"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={UAF}
                    onChange={handleChecklist}
                    name="UAF"
                  />
                }
                label="UA-F"
              />
            </FormGroup>
          </>
        }
        cancel={() => setOpenCategory(false)}
        valueCancel="Batal"
        confirm={() => {
          let datasend = { status: validatedstatus };

          Axios.put(urlservices + "/request/" + idupdate, datasend).then(
            (res) => {
              // console.log("update true", res.data);]
              setOpenCategory(false);
              // getRequest();
            },
          );
        }}
        valueConfirm="Update"
      />

      <Dialog
        open={openstatus}
        close={() => setOpenStatus(false)}
        title="Ubah Status"
        content={
          <>
            <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={diterima}
                    onChange={handleLastStatusCheclist}
                    name="diterima"
                  />
                }
                label="Diterima"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={ditolak}
                    onChange={handleLastStatusCheclist}
                    name="ditolak"
                  />
                }
                label="Ditolak"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={undereview}
                    onChange={handleLastStatusCheclist}
                    name="undereview"
                  />
                }
                label="Under Review/Klarifikasi"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={solved}
                    onChange={handleLastStatusCheclist}
                    name="solved"
                  />
                }
                label="Selesai"
              />
            </FormGroup>
          </>
        }
        cancel={() => setOpenStatus(false)}
        valueCancel="Batal"
        confirm={() => {
          let datasend = { lastStatus: laststatus };

          Axios.put(urlservices + "/request/" + idupdate, datasend).then(
            (res) => {
              console.log("update true", res.data);
              setOpenStatus(false);
              // getRequest();
            },
          );
        }}
        valueConfirm="Update"
      />
    </>
  );
}

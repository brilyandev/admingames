import React, { useState, useEffect } from "react";
import {
  Grid,
  Paper,
  Avatar,
  Typography,
  Button,
  TextField,
  InputLabel,
  Select,
  MenuItem,
  FormControlLabel,
  Checkbox,
  CircularProgress,
  IconButton
} from "@material-ui/core";

import CloudDownload from '@material-ui/icons/CloudDownload';
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

// styles
import useStyles from "./styles";
import MUIDataTable from "mui-datatables";

import PageTitle from "../Components/PageTitle/PageTitle";
import axios from "axios";
import {
  useUserDispatch,
  signExpired,
  signOut,
  useUserState,
} from "../../context/UserContext";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import _ from "lodash";
import DialogFullScreen from "../../components/Dialog/DialogFullScreen";
import moment from "moment";
import ObjectID from "bson-objectid";
import StaticVar from "../../Config/StaticVar";
import Api from "../../Services/Api";
import ReactExport from "react-export-excel";
import Axios from "axios";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function Participants(props) {
  // global
  var userDispatch = useUserDispatch();
  var classes = useStyles();
  //"http://localhost:300"
  const url = StaticVar.Base_Url; //"http://localhost:300";
  const token = localStorage.getItem("token");
  const headers = {
    "Content-Type": "application/json",
    "x-access-token": token,
  };

  //   catch(error){
  //     signExpired(userDispatch,props.history);
  //   }
  // }
  var getData = async () => {
    try {
      // const req1 = axios.get(url+"/private/participants",{headers});
      axios.get(url + "/private/contingents", { headers }).then((res) => {
        setcontingents(res.data);
      });
      axios.get(url+"/private/events/get_all_event",{headers}).then(res=>setevent(res.data))
      axios.get(url + "/provinces").then((res) => setprovinceName(res.data));
      axios.get(url+'/private/dukcapils/repo', {headers}).then(res=>setrepodukcapil(res.data))
    } catch (error) {
      signExpired(userDispatch, props.history);
    }
  };

  const [allathlete, setallathlete] = useState([]);
  const [allathletetahap2, setallathletetahap2] = useState([]);
  const [allofficialtahap2, setallofficialtahap2] = useState([]);
  const [allofficialkontingentahap2, setallofficialkontingentahap2] = useState([]);
  const [allVIP, setallVIP] = useState([]);
  const [allCdM, setallCdM] = useState([]);
  const [allKONI, setallKONI] = useState([]);
  const [userdata, setuserdata] = useState([]);
  const [event, setevent]= useState([])
  const [repodukcapil, setrepodukcapil]= useState([])  

  const [open, setOpen] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [openModalDukcapil, setopenModalDukcapil] = useState(false);

  const [openModalReset, setOpenModalReset] = useState(false);
  const [dukcapil, setdukcapil] = useState({});
  const [loaddukcapil, setloaddukcapil] = useState(false);
  const [checkparticipants, setcheckparticipants] = useState({});
  const [contingent_choose, setcontingent_choose] = useState("0");
  const [provinceName, setprovinceName] = useState([]);

  const [loadingstatus, setLoadingstatus] = useState(false);
  const [validatedstatus, setValidatedStatus] = useState("");
  const [checkedstatus, setCheckedStatus] = useState({
    Vp: false,
    M: false,
    W: false,
    X: false,
    Q: false,
    N: false,
  });
  const { Vp, M, W, X, Q, N } = checkedstatus;

  const [action, setaction] = useState("");
  const [username, setusername] = useState([]);
  var [iduser, setiduser] = useState("");
  var [NIK, setNIK] = useState("");
  var [contingent_name_popup, setcontingent_name_popup] = useState("");
  var [imgPreview, setImgPreview] = useState(NoImg);
  // var [imgEditPreview, setImgEditPreview ] = useState();
  var [img, setImg] = useState("");
  var [isImgValid, setIsImgValid] = useState("");
  var [imgErrorMsg, setImgErrorMsg] = useState("");
  const [events, setevents] = useState([]);
  const [contingents, setcontingents] = useState([]);

  const handleChecklist = (event) => {
    console.log("eventchecklist", event.target.name);
    setValidatedStatus(event.target.name);
    if (event.target.name === "Vp") {
      setCheckedStatus({
        [event.target.name]: event.target.checked,
        M: false,
        W: false,
        X: false,
        Q: false,
        N: false,
      });
    } else if (event.target.name === "M") {
      setCheckedStatus({
        [event.target.name]: event.target.checked,
        Vp: false,
        W: false,
        X: false,
        Q: false,
        N: false,
      });
    } else if (event.target.name === "W") {
      setCheckedStatus({
        [event.target.name]: event.target.checked,
        Vp: false,
        M: false,
        X: false,
        Q: false,
        N: false,
      });
    } else if (event.target.name === "X") {
      setCheckedStatus({
        [event.target.name]: event.target.checked,
        M: false,
        W: false,
        Vp: false,
        Q: false,
        N: false,
      });
    } else if (event.target.name === "Q") {
      setCheckedStatus({
        [event.target.name]: event.target.checked,
        M: false,
        W: false,
        X: false,
        Vp: false,
        N: false,
      });
    } else {
      setCheckedStatus({
        [event.target.name]: event.target.checked,
        M: false,
        W: false,
        X: false,
        Q: false,
        Vp: false,
      });
    }
  };

  const handleSaveStatus = () => {
    setLoadingstatus(true);
    console.log("checkdukcapil.dukcapil", checkparticipants._id);
    let datasend = { validatedstatus: validatedstatus };
    if (checkparticipants._id) {
      // console.log("edit data");
      axios
        .put(url + "/private/participants/" + checkparticipants._id, datasend, {
          headers,
        })
        .then((res) => {
          setLoadingstatus(false);
          console.log("ok");
          let filterquery = { contingent_id: choosecontingent_id };
          filterquery = { ...filterquery, filter: filterData };
          Api.getCheckParticipant(filterquery).then((res) => {
            console.log("res", res.data);
            setdataparticipant(res.data);
          });
          // setOpen(false);
        });
    }
  };

  useEffect(() => {
    getData();
    return () => {
      getData();
    };
  }, []);

  // useEffect(() => {
  //   loadDataAwal();
  //   return () => {
  //     loadDataAwal();
  //   }
  // }, []);

  var [reset_password, setreset_password] = useState("ponxx2020papua");

  var [form_name, setform_name] = useState("");
  var [form_gender, setform_gender] = useState("");
  var [form_telp, setform_telp] = useState("");
  var [form_address, setform_address] = useState("");
  var [form_photo, setform_photo] = useState("");
  var [form_email, setform_email] = useState("");
  var [form_password, setform_password] = useState("ponxx2020papua");
  var [form_user_access, setform_user_access] = useState("");
  var [form_contingent_id, setform_contingent_id] = useState("0");
  var [form_event_id, setform_event_id] = useState("0");
  var [choosecontingent_id, setchoosecontingent_id] = useState("0");
  var [chooseevent_id, setchooseevent_id] = useState("");

  const handleClickOpen = () => {
    setaction("add");
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenModalDelete(false);
    setOpenModalReset(false);
    setOpenAlert(false);
    setcheckparticipants({});
    setopenModalDukcapil(false)
  };

  function handleClickOpenDelete(id, name) {
    setiduser(id);
    setusername(name);
    setOpenModalDelete(true);
  }
  function handleClickOpenReset(id, name) {
    setiduser(id);
    setusername(name);
    setOpenModalReset(true);
  }

  function handleClickOpenEdit(id) {
    // setiduser(rows[id]._id);
    setcheckparticipants(rows[id]);
    setNIK(rows[id].ID_Number);
    //var contingent_name = contingents.filter(x=>{return x._id === rows[id].contingent_id})
    setcontingent_name_popup(rows[id].contingent_name);
    setOpen(true);
    setaction("edit");
    console.log("link", url + "/private/dukcapils?KTP=" + rows[id].ID_Number);
    setloaddukcapil(false);
    if (rows[id].dukcapil) {
      axios
        .get(url + "/private/dukcapils?KTP=" + rows[id].ID_Number, { headers })
        .then((res) => {
          setdukcapil(res.data[0]);
          setloaddukcapil(true);
        });
    }

    // setform_name(rows[id].name)
    // setform_gender(rows[id].gender)
    // setform_telp(rows[id].telp)
    // setform_address(rows[id].address)
    // setform_photo(rows[id].photo)
    // setform_email(rows[id].email)
    // setform_user_access(rows[id].user_access);
    // setImgPreview(url+"/repo/"+rows[id].photo);
  }

  function handleImage(e) {
    let reader = new FileReader();
    let file = e.target.files[0],
      pattern = /image-*/;

    if (!file.type.match(pattern)) {
      setIsImgValid(true);
      setImgErrorMsg("Format File tidak sesuai");
      return;
    }

    reader.onloadend = () => {
      setIsImgValid(false);
      setImgErrorMsg("");
      setImg(file);
      setImgPreview(reader.result);
    };

    reader.readAsDataURL(file);
  }

  let sendData = new FormData();
  sendData.append("name", form_name);
  sendData.append("gender", form_gender);
  sendData.append("telp", form_telp);
  sendData.append("address", form_address);
  sendData.append("photo", img);
  sendData.append("email", form_email);
  sendData.append("password", form_password);
  sendData.append("user_access", form_user_access);
  sendData.append("contingent_id", form_contingent_id);
  sendData.append("event_id", form_event_id);

  let sendDataEdit = new FormData();
  sendDataEdit.append("name", form_name);
  sendDataEdit.append("gender", form_gender);
  sendDataEdit.append("telp", form_telp);
  sendDataEdit.append("address", form_address);
  sendDataEdit.append("photo", img);
  sendDataEdit.append("email", form_email);
  sendDataEdit.append("user_access", form_user_access);
  sendDataEdit.append("contingent_id", form_contingent_id);
  sendDataEdit.append("event_id", form_event_id);

  function postData() {
    if (action === "add") {
      axios.post(url + "/users/register", sendData).then((res) => {
        getData();
        setOpen(false);
      });
    }
    if (action === "edit") {
      axios
        .put(url + "/private/users/" + iduser, sendDataEdit, { headers })
        .then((res) => {
          getData();
          setOpen(false);
        });
    }
  }

  function resetPassword(id) {
    axios
      .put(
        url + "/private/users/" + iduser,
        { password: reset_password },
        { headers },
      )
      .then((res) => {
        setOpenModalReset(false);
        getData();
      });
  }
  function deleteData(id) {
    axios.delete(url + "/private/users/" + id, { headers }).then((res) => {
      setOpenModalDelete(false);
      getData();
    });
  }

  /** Table Pagination */
  const [rows, setRows] = useState([]);
  const [rowsFilter, setRowsFilter] = useState([]);
  const [filterData, setfilterData] = useState("0");
  const [dataparticipant, setdataparticipant] = useState([]);
  const [openAlert, setOpenAlert] = useState(false);
  const [participant_type, setparticipant_type] = useState("0");
  const [hasError, sethasError] = useState(false);
  var [errorType, seterrorType] = useState("");
  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const [checkdukcapil, setcheckdukcapil] = useState("Belum");
  const [tgl_pindah, settgl_pindah] = useState("");
  const [asal_pindah, setasal_pindah] = useState("0");
  const [tujuan_pindah, settujuan_pindah] = useState("0");

  const columns = [
    {
      name: "No",
      options: {
        print: false,
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <div>
              <Typography className={classes.txtAction}>
                {tableMeta.rowIndex + 1}
              </Typography>
            </div>
          );
        },
      },
    },
    {
      name: "ID_Number",
      label: "NIK",
      options: {
        filter: true,
      },
    },
    {
      name: "full_name",
      label: "Name",
      options: {
        filter: true,
      },
    },
    {
      name: "contingent_name",
      label: "contingent_name",
      options: {
        filter: true,
      },
    },
    {
      name: "event_name",
      label: "event_name",
      options: {
        filter: true,
      },
    },
    {
      name: "participant_type",
      label: "type",
      options: {
        filter: true,
      },
    },
    // {
    //   name: "contingent_id",
    //   label : "contingent",
    //   options: {
    //     filter: true,
    //   }
    // } ,
    // {
    //     name: "event_id",
    //     label : "discipline",
    //     options: {
    //       filter: true,
    //     }
    // } ,
    // {
    //     name: "contingent",
    //     options: {
    //       name:"contingent_name",
    //       print: false,
    //       filter: true,
    //       sort: false,
    //       empty: true,
    //       customBodyRender: (value, tableMeta, updateValue) => {
    //           var contingent_id = rows[tableMeta.rowIndex].contingent_id;
    //           var contingent_name = contingents.filter(x=>{
    //               return x._id === contingent_id
    //           })
    //         return (
    //           <div>
    //              <Typography className={classes.txtAction}>{contingent_name[0].name}</Typography>
    //           </div>
    //         );
    //       }
    //     }
    //   },

    // {
    //   name: "discipline",
    //   options: {
    //     name:"event_name",
    //     print: false,
    //     filter: true,
    //     sort: false,
    //     empty: true,
    //     customBodyRender: (value, tableMeta, updateValue) => {
    //         var event_id = rows[tableMeta.rowIndex].event_id;
    //         var event_data = events.filter(x=>{
    //             return x._id === event_id
    //         })
    //       return (
    //         <div>
    //            <Typography className={classes.txtAction}>{event_data.length>0 ? event_data[0].event_name : ""}</Typography>
    //         </div>
    //       );
    //     }
    //   }
    // },
    {
      name: "Dukcapil",
      options: {
        name: "dukcapil",
        print: false,
        filter: true,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          var dukcapil = rows[tableMeta.rowIndex].dukcapil;
          return (
            <div>
              <Typography className={classes.txtAction}>
                {dukcapil ? "Sudah dicek" : "Belum dicek"}
              </Typography>
            </div>
          );
        },
      },
    },
    {
      name: "Action",
      options: {
        print: false,
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <div>
              <Button
                className={classes.btnAction}
                style={{ marginBottom: 3, marginTop: 3 }}
                onClick={() => handleClickOpenEdit(tableMeta.rowIndex)}
              >
                <Typography className={classes.txtAction}>Lihat</Typography>
              </Button>
            </div>
          );
        },
      },
    },
  ];

  return (
    <>
      <PageTitle title="Participant" />

      <DialogFullScreen
        open={open}
        close={handleClose}
        title={"Pemeriksa Data Dukcapil"}
        content={
          <>
            <Grid container spacing={2}>
              <Grid item md={6}>
                <h2 style={{ marginBottom: 0 }}>Data Entrie Kontingen</h2>

                <table>
                  <tr>
                    <td>NIK</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>
                      {checkparticipants.ID_Number}
                    </td>
                  </tr>
                  <tr>
                    <td>KK</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>
                      {checkparticipants.Family_Number}
                    </td>
                  </tr>
                  <tr>
                    <td>Nomor Passport</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>{checkparticipants.passport}</td>
                  </tr>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>
                      {checkparticipants.full_name}
                    </td>
                  </tr>
                  <tr>
                    <td>Tempat Tanggal Lahir</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>
                      {checkparticipants.place_of_birth +
                        ", " +
                        moment(checkparticipants.date_of_birth).format(
                          "DD-MM-YYYY",
                        )}
                    </td>
                  </tr>
                  <tr>
                    <td>Nama Ibu</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>() </td>
                  </tr>
                  <tr>
                    <td>Nama Ayah</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>()</td>
                  </tr>
                  <tr>
                    <td>Golongan Darah</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>-</td>
                  </tr>
                  <tr>
                    <td>Status Perkawinan</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>-</td>
                  </tr>
                  <tr>
                    <td>Asal Daerah</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>
                      {checkparticipants.province_name}
                    </td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td style={{ height: 30 }}>{checkparticipants.address}</td>
                  </tr>
                  <tr style={{ heigth: 50 }}>
                    <td>Foto</td>
                    <td>:</td>
                    <td>
                      <img
                        style={{ height: 100 }}
                        src={
                          url + "/repo/participant/" + checkparticipants.photo
                        }
                      />
                    </td>
                  </tr>
                  {/* <tr>
                      <td></td>
                      <td></td>
                      <td><Button variant="contained" color="primary" style={{fontSize:10}}>Sinkronisasi Data</Button></td>
                    </tr> */}
                </table>

                <h2>Status Akhir</h2>
                {/* <Button variant="contained" color="primary" onClick={()=>console.log('dukcapil', dukcapil)}>Cek Data</Button> */}
                <table style={{ width: "100%" }}>
                  <tr style={{ backgroundColor: "red", color: "white" }}>
                    <td style={{ width: "20%", textAlign: "center" }}>
                      Sesuai
                    </td>
                    <td style={{ width: "30%", textAlign: "center" }}>
                      Bersyarat
                    </td>
                    <td style={{ width: "30%", textAlign: "center" }}>
                      Tidak Sesuai
                    </td>
                    <td></td>
                  </tr>

                  <tr>
                    <td style={{ width: 350, textAlign: "center" }}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            name="Vp"
                            checked={Vp}
                            onChange={handleChecklist}
                          />
                        }
                        label="Vp"
                      />
                      {/* {checkedstatus.X} */}
                    </td>
                    <td style={{ width: "30%", textAlign: "center" }}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            name="M"
                            checked={M}
                            onChange={handleChecklist}
                          />
                        }
                        label="M"
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            name="W"
                            checked={W}
                            onChange={handleChecklist}
                          />
                        }
                        label="W"
                      />
                    </td>
                    <td style={{ width: "30%", textAlign: "center" }}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            name="X"
                            checked={X}
                            onChange={handleChecklist}
                          />
                        }
                        label="X"
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            name="Q"
                            checked={Q}
                            onChange={handleChecklist}
                          />
                        }
                        label="Q"
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            name="N"
                            checked={N}
                            onChange={handleChecklist}
                          />
                        }
                        label="N"
                      />
                    </td>
                    <td style={{ textAlign: "center" }}>
                      {loadingstatus ? (
                        <CircularProgress />
                      ) : (
                        <Button
                          variant="contained"
                          size="small"
                          color="primary"
                          onClick={handleSaveStatus}
                        >
                          Simpan
                        </Button>
                      )}
                    </td>
                  </tr>
                </table>
              </Grid>

              <Grid item md={6}>
                <h2 style={{ marginBottom: 0 }}>Data Dukcapil</h2>
                {checkparticipants.dukcapil ? null : (
                  <Button
                    size="small"
                    variant="contained"
                    color="secondary"
                    onClick={() => {
                      axios
                        .get(url + "/private/dukcapils/NIK/" + NIK, { headers })
                        .then((res) => {
                          var data_response = res.data;
                          setloaddukcapil(true);
                          setdukcapil({
                            ...data_response.content[0],
                            KTP: NIK,
                          });
                        });
                    }}
                  >
                    Load Data Dukcapil
                  </Button>
                )}
                {
                  // loaddukcapil ?
                  <table style={{ width: "100%" }} style={{ fontSize: 12 }}>
                    <tr>
                      <td>NIK</td>
                      <td>:</td>
                      <td>{NIK}</td>
                    </tr>
                    <tr>
                      <td>NAMA LENGKAP</td>
                      <td>:</td>
                      <td>
                        <TextField
                          style={{ width: "100%", fontSize: 9 }}
                          value={dukcapil.NAMA_LGKP}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              NAMA_LGKP: e.target.value,
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>ALAMAT</td>
                      <td>:</td>
                      <td>
                        {" "}
                        <TextField
                          style={{ width: "100%", fontSize: 9 }}
                          value={dukcapil.ALAMAT}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              ALAMAT: e.target.value,
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>RT / RW</td>
                      <td>:</td>
                      <td>
                        {" "}
                        <TextField
                          style={{ width: "50%" }}
                          value={dukcapil.NO_RT}
                          onChange={(e) => {
                            setdukcapil({ ...dukcapil, NO_RT: e.target.value });
                          }}
                        />
                        <TextField
                          style={{ width: "50%" }}
                          value={dukcapil.NO_RW}
                          onChange={(e) => {
                            setdukcapil({ ...dukcapil, NO_RW: e.target.value });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>KELURAHAN</td>
                      <td>:</td>
                      <td>
                        <TextField
                          style={{ width: "100%", fontSize: 9 }}
                          value={dukcapil.KEL_NAME}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              KEL_NAME: e.target.value,
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>KECAMATAN</td>
                      <td>:</td>
                      <td>
                        <TextField
                          style={{ width: "100%", fontSize: 9 }}
                          value={dukcapil.KEC_NAME}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              KEC_NAME: e.target.value,
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>KAB/KOTA</td>
                      <td>:</td>
                      <td>
                        <TextField
                          style={{ width: "100%", fontSize: 9 }}
                          value={dukcapil.KAB_NAME}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              KAB_NAME: e.target.value,
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>PROVINSI</td>
                      <td>:</td>
                      <td>
                        <Select
                          style={{ width: "100%" }}
                          value={dukcapil.PROP_NAME ? dukcapil.PROP_NAME : "0"}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              PROP_NAME: e.target.value,
                            });
                          }}
                        >
                          <MenuItem value={"0"}>Pilih Provinsi</MenuItem>
                          {provinceName.map((item, index) => {
                            return (
                              <MenuItem value={item.province_name}>
                                {item.province_name}
                              </MenuItem>
                            );
                          })}
                        </Select>
                      </td>
                    </tr>
                    <tr>
                      <td>TEMPAT, TGL LAHIR</td>
                      <td>:</td>
                      <td>
                        <TextField
                          style={{ width: "50%" }}
                          value={dukcapil.TMPT_LHR}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              TMPT_LHR: e.target.value,
                            });
                          }}
                        />
                        <TextField
                          type="Date"
                          style={{ width: "50%" }}
                          value={dukcapil.TGL_LHR}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              TGL_LHR: e.target.value,
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>JENIS KELAMIN</td>
                      <td>:</td>
                      <td>
                        <TextField
                          style={{ width: "100%", fontSize: 9 }}
                          value={dukcapil.JENIS_KLMIN}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              JENIS_KLMIN: e.target.value,
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>AGAMA / GOL DARAH</td>
                      <td>:</td>
                      <td>
                        <TextField
                          style={{ width: "50%", fontSize: 9 }}
                          value={dukcapil.AGAMA}
                          onChange={(e) => {
                            setdukcapil({ ...dukcapil, AGAMA: e.target.value });
                          }}
                        />
                        <TextField
                          style={{ width: "50%", fontSize: 9 }}
                          value={dukcapil.GOL_DARAH}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              GOL_DARAH: e.target.value,
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <td>STATUS KAWIN / PEKERJAAN</td>
                      <td>:</td>
                      <td>
                        <TextField
                          style={{ width: "50%", fontSize: 9 }}
                          value={dukcapil.STATUS_KAWIN}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              STATUS_KAWIN: e.target.value,
                            });
                          }}
                        />
                        <TextField
                          style={{ width: "50%", fontSize: 9 }}
                          value={dukcapil.JENIS_PKRJN}
                          onChange={(e) => {
                            setdukcapil({
                              ...dukcapil,
                              JENIS_PKRJN: e.target.value,
                            });
                          }}
                        />
                      </td>
                    </tr>
                  </table>

                  //   : null
                }

                {checkedstatus.W ? (
                  <>
                    <h2>Riwayat Perpindahan</h2>
                    {/* <Button variant="contained" color="primary" onClick={()=>console.log('dukcapil', dukcapil)}>Cek Data</Button> */}
                    <table style={{ width: "100%" }}>
                      <tr style={{ backgroundColor: "red", color: "white" }}>
                        <td style={{ width: 20, textAlign: "center" }}>No</td>
                        <td style={{ width: 350, textAlign: "center" }}>
                          Tgl Pindah
                        </td>
                        <td style={{ width: "30%", textAlign: "center" }}>
                          Asal Perpindahan
                        </td>
                        <td style={{ width: "30%", textAlign: "center" }}>
                          Tujuan Perpindahan
                        </td>
                        <td></td>
                      </tr>
                      {dukcapil.RIWAYAT_PINDAH
                        ? dukcapil.RIWAYAT_PINDAH.map((data, index) => {
                            return (
                              <tr>
                                <td style={{ width: 20, textAlign: "center" }}>
                                  {index + 1}
                                </td>
                                <td style={{ width: 350, textAlign: "center" }}>
                                  {moment(data.TGL_PINDAH).format("DD-MM-YYYY")}
                                </td>
                                <td
                                  style={{ width: "30%", textAlign: "center" }}
                                >
                                  {data.PROP_ASAL}
                                </td>
                                <td
                                  style={{ width: "30%", textAlign: "center" }}
                                >
                                  {data.PROP_TUJUAN}
                                </td>
                                <td>
                                  <Button
                                    variant="contained"
                                    size="small"
                                    color="secondary"
                                    onClick={() => {
                                      let datariwayat = dukcapil.RIWAYAT_PINDAH
                                        ? dukcapil.RIWAYAT_PINDAH
                                        : [];
                                      let datadelete = datariwayat.filter(
                                        (x) => x._id !== data._id,
                                      );
                                      setdukcapil({
                                        ...dukcapil,
                                        RIWAYAT_PINDAH: datadelete,
                                      });
                                    }}
                                  >
                                    Hapus
                                  </Button>
                                </td>
                              </tr>
                            );
                          })
                        : null}
                      <tr>
                        <td style={{ width: 20, textAlign: "center" }}></td>
                        <td style={{ width: 350, textAlign: "center" }}>
                          <TextField
                            type="Date"
                            value={tgl_pindah}
                            onChange={(e) => settgl_pindah(e.target.value)}
                          />
                        </td>
                        <td style={{ width: "30%", textAlign: "center" }}>
                          <TextField
                            select
                            style={{ width: "100%" }}
                            value={asal_pindah}
                            onChange={(e) => {
                              setasal_pindah(e.target.value);
                            }}
                          >
                            <MenuItem value={"0"}>Asal Provinsi</MenuItem>
                            {provinceName.map((item, index) => {
                              return (
                                <MenuItem value={item.province_name}>
                                  {item.province_name}
                                </MenuItem>
                              );
                            })}
                          </TextField>
                        </td>
                        <td style={{ width: "30%", textAlign: "center" }}>
                          <TextField
                            select
                            value={tujuan_pindah}
                            onChange={(e) => {
                              settujuan_pindah(e.target.value);
                            }}
                            style={{ width: "100%" }}
                          >
                            <MenuItem value={"0"}>
                              Perpindahan Provinsi
                            </MenuItem>
                            {provinceName.map((item, index) => {
                              return (
                                <MenuItem value={item.province_name}>
                                  {item.province_name}
                                </MenuItem>
                              );
                            })}
                          </TextField>
                        </td>
                        <td>
                          <Button
                            variant="contained"
                            size="small"
                            color="primary"
                            onClick={() => {
                              let datariwayat = dukcapil.RIWAYAT_PINDAH
                                ? dukcapil.RIWAYAT_PINDAH
                                : [];
                              datariwayat.push({
                                _id: ObjectID.generate(),
                                TGL_PINDAH: tgl_pindah,
                                PROP_ASAL: asal_pindah,
                                PROP_TUJUAN: tujuan_pindah,
                              });
                              setdukcapil({
                                ...dukcapil,
                                RIWAYAT_PINDAH: datariwayat,
                              });
                            }}
                          >
                            Simpan
                          </Button>
                        </td>
                      </tr>
                    </table>
                  </>
                ) : null}
              </Grid>
              {/* <Grid item md={12}>
                  <Typography>Catatan</Typography>
                  <Typography>Kesesuaian</Typography>
                </Grid> */}
              {
                // !(checkparticipants.dukcapil)?
                // <Button variant="contained" color="secondary" onClick={()=>{
                //   var dukcapil_id = ObjectID.generate()
                //   axios.put(url+'/private/participants/'+checkparticipants._id, {
                //       dukcapil : dukcapil_id
                //   },{headers}).then(res=>{
                //       var data_post = {...dukcapil, _id:dukcapil_id}
                //       axios.post(url+'/private/dukcapils/create', data_post,{headers}).then(res=>{
                //           console.log('')
                //           getData();
                //       })
                //   })
                // }}>Simpan</Button>
                // : null
              }
            </Grid>
          </>
        }
        cancel={handleClose}
        confirm={() => {
          console.log("checkdukcapil.dukcapil", checkparticipants.dukcapil);
          if (checkparticipants.dukcapil) {
            console.log("edit data");
            axios
              .put(
                url + "/private/dukcapils/" + checkparticipants.dukcapil,
                dukcapil,
                { headers },
              )
              .then((res) => {
                let filterquery = { contingent_id: choosecontingent_id };
                filterquery = { ...filterquery, filter: filterData };
                Api.getCheckParticipant(filterquery).then((res) => {
                  console.log("res", res.data);
                  setdataparticipant(res.data);
                });
                setOpen(false);
              });
          } else {
            console.log("create data", checkparticipants._id);
            const dukcapil_id = ObjectID.generate();
            console.log("dukcapil_id", {
              dukcapil: dukcapil_id,
            });
            axios
              .put(
                url + "/private/participants/" + checkparticipants._id,
                {
                  dukcapil: dukcapil_id,
                },
                { headers },
              )
              .then(() => {
                axios
                  .post(
                    url + "/private/dukcapils/create",
                    { ...dukcapil, _id: dukcapil_id },
                    { headers },
                  )
                  .then(() => {
                    let filterquery = { contingent_id: choosecontingent_id };
                    filterquery = { ...filterquery, filter: filterData };
                    Api.getCheckParticipant(filterquery).then((resdukcapil) => {
                      setdataparticipant(resdukcapil.data);
                      setOpen(false);
                    });
                  });
              });
          }
        }}
        valueConfirm={"Simpan"}
        valueCancel={"Batalkan"}
        colorButtonConfirm={"#bf272b"}
      />

      {/* modal delete */}
      <DialogFullScreen
        open={openModalDukcapil}
        close={handleClose}
        title={"Data Snapshop Console Dukcapil " + checkparticipants.full_name}
        content={
          <>
            <Typography>{checkparticipants.full_name}</Typography>
            <Typography>Pilih Nama File : 
              <Select value={checkparticipants.url} onChange={(e)=>{
                setcheckparticipants({...checkparticipants, dataconsole:{
                  name : repodukcapil.filter(x=>x.url === e.target.value)[0].name,
                  url : repodukcapil.filter(x=>x.url === e.target.value)[0].url
                }})
              }}>
                <MenuItem value="0">Pilih File</MenuItem>
                {
                  repodukcapil.map(itemrepo=>{
                    return(
                      <MenuItem value={itemrepo.url}>{itemrepo.name}</MenuItem>
                    )
                  })
                }
              </Select>
            </Typography>
            
            <img src={checkparticipants.dataconsole ? checkparticipants.dataconsole.url : null} style={{width:'70%'}}/>
            

          </>
        }
        cancel={handleClose}
        valueCancel={"Tidak"}
        confirm={() => {
          axios.put(url+'/private/participants/'+ checkparticipants._id, {
            dataconsole : checkparticipants.dataconsole
          }, {headers}).then(res=>{
            console.log('res update', res.data)
            setopenModalDukcapil(false)
            let filterquery = { contingent_id: choosecontingent_id };
            filterquery = { ...filterquery, filter: filterData };
            Api.getCheckParticipant(filterquery).then((resget) => {
              console.log("resget", resget.data);
              setdataparticipant(resget.data);
            });
          })
        }}
        valueConfirm={"Simpan"}
        colorButtonConfirm={"#bf272b"}
      />

      <Dialog
        open={openModalDelete}
        close={handleClose}
        title={"Apakah anda yakin menghapus ??"}
        content={<Typography>{username}</Typography>}
        cancel={handleClose}
        valueCancel={"Tidak"}
        confirm={() => deleteData(iduser)}
        valueConfirm={"Ya, Hapus"}
      />
      {/* modal Reset */}
      <Dialog
        open={openModalReset}
        close={handleClose}
        title={"Apakah anda yakin mereset password user : "}
        content={
          <Grid>
            <Typography>{username}</Typography>
            <br></br>
            <TextField
              label="Default Password"
              className={classes.formContainer}
              margin="normal"
              value={reset_password}
              onChange={(event) => setreset_password(event.target.value)}
            />
          </Grid>
        }
        cancel={handleClose}
        valueCancel={"Tidak"}
        confirm={() => resetPassword()}
        valueConfirm={"Ya, Reset"}
      />

      <p>
        Kontingen :
        <Select
          value={choosecontingent_id}
          onChange={(event) => {
            setchoosecontingent_id(event.target.value);
            if (event.target.value != "0") {
              // var datafilter = rowsFilter.filter(x=>x.contingent_id === event.target.value)
              // setRows(datafilter)

              if (chooseevent_id != "0" && chooseevent_id != "") {
                var datafilter = rowsFilter.filter(
                  (x) =>
                    x.contingent_id === event.target.value &&
                    x.event_id === chooseevent_id,
                );
                setRows(datafilter);
              } else {
                var datafilter = rowsFilter.filter(
                  (x) => x.contingent_id === event.target.value,
                );
                setRows(datafilter);
              }
            } else {
              if (chooseevent_id != "0" && chooseevent_id != "") {
                var datafilter = rowsFilter.filter(
                  (x) => x.event_id === chooseevent_id,
                );
                setRows(datafilter);
              } else {
                setRows(rowsFilter);
              }
            }
          }}
        >
          <MenuItem value={"0"}>Pilih Kontingen</MenuItem>
          {contingents.map((item, index) => {
            return <MenuItem value={item._id}>{item.name}</MenuItem>;
          })}
        </Select>
        Kategori :
        <Select
          style={{ marginRight: 5 }}
          value={filterData}
          onChange={(event) => {
            setfilterData(event.target.value);
          }}
        >
          <MenuItem value={"0"}>Pilih Filter</MenuItem>
          {["Cek Domisili", "Data Tidak Ditemukan", "Data belum ditarik"].map(
            (item, index) => {
              return <MenuItem value={item}>{item}</MenuItem>;
            },
          )}
        </Select>
        {/* Jenis Peserta :  */}
        {/* <Select style={{marginRight:5}} value={participant_type} onChange={(event)=>{
            setparticipant_type(event.target.value)
        }}
        >
            <MenuItem value={"0"}>Pilih Filter</MenuItem>
            {
                ["Athlete", "Official", "OfficialKontingen"].map((item,index)=>{
                  return(
                      <MenuItem value={item}>{item}</MenuItem>
                  )
                })
            }
        </Select> */}
        <Button
          size="small"
          variant="contained"
          color="secondary"
          onClick={() => {
            let filterquery = { contingent_id: choosecontingent_id };
            filterquery = { ...filterquery, filter: filterData };
            Api.getCheckParticipant(filterquery).then((res) => {
              console.log("res", res.data);
              setdataparticipant(res.data);
            });
          }}
        >
          Load Data
        </Button>
        {allathlete.length == 0 ? (
          <Button
            size="small"
            variant="contained"
            color="primary"
            onClick={() => {
              Api.getSyncDukcapil({
                filter: "",
                participant_type: "Athlete",
              })
                .then((res) => {
                  let resdata = res.data
                    .filter((x) => x.participant_type === "Athlete")
                    .map((y) => {
                      return {
                        ...y,
                        date_of_birth: moment(y.date_of_birth).format(
                          "YYYY-MM-DD",
                        ),
                        time_of_birth: moment(y.date_of_birth).format(
                          "HH:mm:ss",
                        )
                        // ID_Number: y.ID_Number,
                        // SKM: y.SKM,
                        // address: y.address,
                        // comparisonResult: y.comparisonResult,
                        // contingent_id: y.contingent_id,
                        // contingent_logo: y.contingent_logo,
                        // contingent_name: y.contingent_name,
                        // date_of_birth: moment(y.date_of_birth).format(
                        //   "YYYY-MM-DD",
                        // ),
                        // dukcapil_date_of_birth: y.dukcapil_date_of_birth,
                        // dukcapil_name: y.dukcapil_name,
                        // dukcapil_place_of_birth: y.dukcapil_place_of_birth,
                        // dukcapil_prop_name: y.dukcapil_prop_name,
                        // event_icon: y.event_icon,
                        // event_id: y.event_id,
                        // event_name: y.event_name,

                        // full_name: y.full_name,
                        // gender: y.gender,
                        // participant_type: y.participant_type,
                        // photo: y.photo,
                        // place_of_birth: y.place_of_birth,
                        // province_name: y.province_name,
                        // _id: y._id,
                      };
                    });
                  setallathlete(resdata);
                  console.log("allathlete", resdata);
                  // setallathlete(
                  //   res.data.filter((x) => x.participant_type === "Athlete"),
                  // );
                })
                .catch((err) => {
                  console.log("err", err);
                });
            }}
          >
            Ambil Semua Atlit
          </Button>
        ) : (
          <ExcelFile
            element={
              <Button size="small" variant="contained" color="primary">
                Download Data
              </Button>
            }
          >
            <ExcelSheet data={allathlete} name="Athlete">
              <ExcelColumn label="_id" value="_id" />
              <ExcelColumn label="NIK" value="ID_Number" />
              <ExcelColumn label="Nama" value="full_name" />
              <ExcelColumn label="Nama Dukcapil" value="dukcapil_name" />
              <ExcelColumn label="Jenis Kelamin" value="gender" />
              <ExcelColumn label="Tempat Lahir" value="place_of_birth" />
              <ExcelColumn
                label="Tempat Lahir Dukcapil"
                value="dukcapil_place_of_birth"
              />
              <ExcelColumn
                label="Tempat Lahir Dukcapil"
                value="dukcapil_place_of_birth"
              />
              <ExcelColumn label="Tgl Lahir" value="date_of_birth" />
              <ExcelColumn label="Waktu Lahir" value="time_of_birth" />
              <ExcelColumn
                label="Tgl Lahir Dukcapil"
                value="dukcapil_date_of_birth"
              />
              <ExcelColumn label="Alamat" value="address" />
              <ExcelColumn label="Tipe" value="participant_type" />
              <ExcelColumn label="Disiplin" value="event_name" />
              <ExcelColumn label="Kontingen" value="contingent_name" />
              <ExcelColumn label="Provinsi" value="province_name" />
              <ExcelColumn
                label="Provinsi Dukcapil"
                value="dukcapil_prop_name"
              />
              <ExcelColumn label="SKM" value="SKM" />
              <ExcelColumn label="Catatan" value="note" />
              <ExcelColumn label="Perbandingan" value="comparisonResult" />
            </ExcelSheet>
          </ExcelFile>
        )}
        &nbsp;&nbsp;
        {allathletetahap2.length == 0 ? (
          <Button
            size="small"
            variant="contained"
            color="secondary"
            onClick={() => {
              Api.getParticipantTahap2("Athlete", choosecontingent_id).then((res) => {
                let resdata = res.data.map((x) => {
                  return {
                    ...x,
                    // date_of_birth: `${x.date_of_birth}`.slice(0, 10),
                    date_of_birth: moment(x.date_of_birth).format("YYYY-MM-DD"),
                    entrie_by_name_date: moment(x.entrie_by_name_date).format(
                      "YYYY-MM-DD",
                    ),
                    contingent_name: contingents.filter(itemcontingent=>itemcontingent._id === x.contingent_id)[0].name,
                    event_name: event.filter(itemevent=>itemevent._id === x.event_id)[0].event_name
                  };
                });
                console.log("allathlete", resdata);
                setallathletetahap2(resdata);
              });
              Api.getParticipantTahap2("Official", choosecontingent_id).then((res) => {
                console.log('Official', res.data)
                let resdata = res.data.map((x) => {
                  
                  return {
                    ...x,
                    date_of_birth: moment(x.date_of_birth).format("YYYY-MM-DD"),
                    entrie_by_name_date: moment(x.entrie_by_name_date).format("YYYY-MM-DD",),
                    contingent_name: contingents.filter(itemcontingent=>itemcontingent._id === x.contingent_id)[0].name,
                    event_name: event.filter(itemevent=>itemevent._id === x.event_id)[0].event_name,
                    function : x.function === ""? "Fo":  x.function
                  };
                });
                console.log('Official Result', resdata)
                setallofficialtahap2(resdata);
              });
              Api.getUsers().then((res) => {
                setuserdata(res.data);
              });
            }}
          >
            Ambil Atlit & Official Tahap 2
          </Button>
        ) : (
          <ExcelFile
            element={
              <Button size="small" variant="contained" color="secondary">
                Download Data Tahap 2
              </Button>
            }
          >
            <ExcelSheet data={allathletetahap2} name="Atlit Tahap 2">
              <ExcelColumn label="_id" value="_id" />
              <ExcelColumn label="NIK" value="ID_Number" />
              <ExcelColumn label="Nama" value="full_name" />
              <ExcelColumn label="Jenis Kelamin" value="gender" />
              <ExcelColumn label="Tempat Lahir" value="place_of_birth" />
              <ExcelColumn label="Tgl Lahir" value="date_of_birth" />
              <ExcelColumn label="Tipe" value="participant_type" />
              <ExcelColumn label="Tgl Daftar" value="entrie_by_name_date" />
              <ExcelColumn label="Kontingen ID" value="contingent_id" />
              <ExcelColumn label="Nama Kontingen" value="contingent_name" />
              <ExcelColumn label="Cabor ID" value="event_id" />
              <ExcelColumn label="Nama Cabor" value="event_name" />
              <ExcelColumn label="Didaftarkan Oleh" value="entrie_by_name" />
            </ExcelSheet>
            <ExcelSheet data={allofficialtahap2} name="Official Tahap 2">
              <ExcelColumn label="_id" value="_id" />
              <ExcelColumn label="NIK" value="ID_Number" />
              <ExcelColumn label="Nama" value="full_name" />
              <ExcelColumn label="Jenis Kelamin" value="gender" />
              <ExcelColumn label="Tempat Lahir" value="place_of_birth" />
              <ExcelColumn label="Tgl Lahir" value="date_of_birth" />
              <ExcelColumn label="Tipe" value="participant_type" />
              <ExcelColumn label="Tgl Daftar" value="entrie_by_name_date" />
              <ExcelColumn label="Kontingen ID" value="contingent_id" />
              <ExcelColumn label="Nama Kontingen" value="contingent_name" />
              <ExcelColumn label="Cabor ID" value="event_id" />
              <ExcelColumn label="Nama Cabor" value="event_name" />
              <ExcelColumn label="Didaftarkan Oleh" value="entrie_by_name" />
              <ExcelColumn label="Posisi" value="jobposition" />
              <ExcelColumn label="Keterangan" value="function" />
              <ExcelColumn label="Kluster" value="cluster" />
            </ExcelSheet>
            <ExcelSheet data={userdata} name="User">
              <ExcelColumn label="userid" value="_id" />
              <ExcelColumn label="Nama User" value="name" />
            </ExcelSheet>
          </ExcelFile>
        )}
        &nbsp;&nbsp;
        {allofficialkontingentahap2.length == 0 ? (
          <Button
            size="small"
            variant="contained"
            color="primary"
            onClick={() => {
              Api.getParticipantTahap2("OfficialKontingen", choosecontingent_id).then((res) => {
                let resdata = res.data.map((x) => {
                  return {
                    ...x,
                    date_of_birth: moment(x.date_of_birth).format("YYYY-MM-DD"),
                    entrie_by_name_date: moment(x.entrie_by_name_date).format(
                      "YYYY-MM-DD",
                    ),
                    contingent_name: contingents.filter(itemcontingent=>itemcontingent._id === x.contingent_id)[0].name,
                    function : x.function === ""? "Fo":  x.function,

                  };
                });
                console.log("officialkontingen", resdata);
                setallofficialkontingentahap2(resdata);
              });
              Api.getParticipantTahap2("VIP", choosecontingent_id).then((res) => {
                let resdata = res.data.map((x) => {
                  
                  return {
                    ...x,
                    date_of_birth: moment(x.date_of_birth).format("YYYY-MM-DD"),
                    contingent_name: contingents.filter(itemcontingent=>itemcontingent._id === x.contingent_id)[0].name
                  };
                });
                console.log("VIP", resdata);
                setallVIP(resdata);
              });
              Api.getParticipantTahap2("CdM", choosecontingent_id).then((res) => {
                let resdata = res.data.map((x) => {
                  
                  return {
                    ...x,
                    date_of_birth: moment(x.date_of_birth).format("YYYY-MM-DD"),
                    contingent_name: contingents.filter(itemcontingent=>itemcontingent._id === x.contingent_id)[0].name
                  };
                });
                console.log("CdM", resdata);
                setallCdM(resdata);
              });
              Api.getParticipantTahap2("KONI", choosecontingent_id).then((res) => {
                let resdata = res.data.map((x) => {
                  
                  return {
                    ...x,
                    date_of_birth: moment(x.date_of_birth).format("YYYY-MM-DD"),
                    contingent_name: contingents.filter(itemcontingent=>itemcontingent._id === x.contingent_id)[0].name
                  };
                });
                console.log("KONI", resdata);
                setallKONI(resdata);
              });
            }}
          >
            Ambil Data OfcKontingen,VIP,CdM&KONI
          </Button>
        ) : (
          <ExcelFile
            element={
              <Button size="small" variant="contained" color="secondary">
                Download Data OfcKontingen,VIP,CdM&KONI
              </Button>
            }
          >
            <ExcelSheet data={allofficialkontingentahap2} name="Official Kontingen Tahap 2">
              <ExcelColumn label="_id" value="_id" />
              <ExcelColumn label="NIK" value="ID_Number" />
              <ExcelColumn label="Nama" value="full_name" />
              <ExcelColumn label="Jenis Kelamin" value="gender" />
              <ExcelColumn label="Tempat Lahir" value="place_of_birth" />
              <ExcelColumn label="Tgl Lahir" value="date_of_birth" />
              <ExcelColumn label="Tipe" value="participant_type" />
              <ExcelColumn label="Tgl Daftar" value="entrie_by_name_date" />
              <ExcelColumn label="Nama Kontingen" value="contingent_name" />
              <ExcelColumn label="Posisi" value="jobposition" />
              <ExcelColumn label="Keterangan" value="function" />
              <ExcelColumn label="Kluster" value="cluster" />
            </ExcelSheet>
            <ExcelSheet data={allVIP} name="VIP">
              <ExcelColumn label="_id" value="_id" />
              <ExcelColumn label="NIK" value="ID_Number" />
              <ExcelColumn label="Nama" value="full_name" />
              <ExcelColumn label="Jenis Kelamin" value="gender" />
              <ExcelColumn label="Tempat Lahir" value="place_of_birth" />
              <ExcelColumn label="Tgl Lahir" value="date_of_birth" />
              <ExcelColumn label="Nama Kontingen" value="contingent_name" />
              <ExcelColumn label="Posisi" value="jobposition" />
              <ExcelColumn label="Kluster" value="cluster" />
            </ExcelSheet>
            <ExcelSheet data={allCdM} name="CdM">
              <ExcelColumn label="_id" value="_id" />
              <ExcelColumn label="NIK" value="ID_Number" />
              <ExcelColumn label="Nama" value="full_name" />
              <ExcelColumn label="Jenis Kelamin" value="gender" />
              <ExcelColumn label="Tempat Lahir" value="place_of_birth" />
              <ExcelColumn label="Tgl Lahir" value="date_of_birth" />
              <ExcelColumn label="Nama Kontingen" value="contingent_name" />
              <ExcelColumn label="Posisi" value="jobposition" />
              <ExcelColumn label="Kluster" value="cluster" />
            </ExcelSheet>
            <ExcelSheet data={allKONI} name="KONI">
              <ExcelColumn label="_id" value="_id" />
              <ExcelColumn label="NIK" value="ID_Number" />
              <ExcelColumn label="Nama" value="full_name" />
              <ExcelColumn label="Jenis Kelamin" value="gender" />
              <ExcelColumn label="Tempat Lahir" value="place_of_birth" />
              <ExcelColumn label="Tgl Lahir" value="date_of_birth" />
              <ExcelColumn label="Nama Kontingen" value="contingent_name" />
              <ExcelColumn label="Posisi" value="jobposition" />
              <ExcelColumn label="Kluster" value="cluster" />
            </ExcelSheet>
            <ExcelSheet data={userdata} name="User">
              <ExcelColumn label="userid" value="_id" />
              <ExcelColumn label="Nama User" value="name" />
            </ExcelSheet>
          </ExcelFile>
        )}
      </p>

      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Paper>
            {/* <MUIDataTable 
            title="Participant List"
            data={rows} 
            columns={columns}   
            options={{
              selectableRows:  'none'  
            }}
          /> */}

            <table style={{ width: "100%", fontSize: 12, marginBottom: 10 }}>
              <thead style={{ background: "#bf272b", color: "white" }}>
                <tr>
                  <th style={{ textAlign: "center", width: 20, padding: 8 }}>
                    No
                  </th>
                  <th style={{ textAlign: "left", padding: 8 }}>
                    Nama Peserta
                  </th>
                  <th style={{ textAlign: "center", width: 100, padding: 8 }}>
                    Jenis Kelamin
                  </th>
                  <th
                    style={{
                      textAlign: "center",
                      width: 150,
                      padding: 8,
                      textAlign: "center",
                    }}
                  >
                    Tanggal Lahir
                  </th>
                  <th style={{ width: 350, padding: 8, textAlign: "left" }}>
                    Alamat
                  </th>
                  <th style={{ textAlign: "center", width: 50, padding: 8 }}>
                    Kepesertaan
                  </th>
                  <th style={{ textAlign: "left", width: 200, padding: 8 }}>
                    Catatan
                  </th>
                  <th style={{ textAlign: "left", width: 200, padding: 8 }}>
                    Status
                  </th>
                  <th style={{ textAlign: "center", width: 80, padding: 8 }}>
                    Snapshot Dukcapil
                  </th>
                  <th style={{ textAlign: "center", width: 250, padding: 8 }}>
                    Data Dukcapil
                  </th>
                </tr>
              </thead>
              <tbody class="table-stripped">
                {dataparticipant.map((item, index) => {
                  return (
                    <tr>
                      <td style={{ textAlign: "center", padding: 8 }}>
                        {index + 1}
                      </td>
                      <td style={{ padding: 8 }}>
                        <b>{item.full_name}</b>
                        <br />
                        <span>NIK : {item.ID_Number}</span>
                      </td>
                      <td style={{ textAlign: "center", padding: 8 }}>
                        {item.gender}
                      </td>
                      <td style={{ textAlign: "center", padding: 8 }}>
                        {item.place_of_birth}
                        <br />
                        {moment(item.date_of_birth).format("DD-MM-YYYY")}
                      </td>
                      <td style={{ textAlign: "left", padding: 8 }}>
                        {item.address}
                      </td>
                      <td style={{ textAlign: "center", padding: 8 }}>
                        {item.participant_type}
                      </td>
                      <td style={{ textAlign: "left", padding: 8 }}>
                        {/* {
                                          item.comparisonResult == 0 ? "Sesuai" : 
                                          item.comparisonResult == 1 ? "Domisili berbeda" : item.note 
                                        }
                                        
                                        */}

                        {filterData === "Cek Domisili" ? (
                          <>
                            Domisili: <b>{item.dukcapil_prop_name}</b>
                            <br />
                            Keterangan SKM :<b>{item.SKM ? item.SKM : "-"}</b>
                          </>
                        ) : filterData === "Data Tidak Ditemukan" ? (
                          item.note
                        ) : (
                          "-"
                        )}
                      </td>
                      <td style={{ textAlign: "left", padding: 8 }}>
                        <p>DW : 
                          <strong>
                          {item.comparisonResult === 0
                            ? "Sesuai"
                            : item.comparisonResult === 1
                            ? "Data Tidak Sesuai"
                            : "Belum Ditarik"}
                          </strong>                          
                        </p>
                        <p>DC : <strong>{item.validatedstatus}</strong></p>
                      </td>
                      <td style={{ textAlign: "center" }}>
                        {
                          item.dataconsole ? 
                          <IconButton>
                            <CloudDownload fontSize="small" />
                          </IconButton> : null
                        }
                      </td>
                      <td style={{ textAlign: "center" }}>
                        <Button
                          size="small"
                          variant="contained"
                          color="secondary"
                          onClick={() => {
                            setOpen(true);
                            setcheckparticipants(item);
                            if (item.validatedstatus === "Vp") {
                              setCheckedStatus({ Vp: true });
                            } else if (item.validatedstatus === "M") {
                              setCheckedStatus({ M: true });
                            } else if (item.validatedstatus === "W") {
                              setCheckedStatus({ W: true });
                            } else if (item.validatedstatus === "X") {
                              // console.log("xtrue");
                              setCheckedStatus({ X: true });
                            } else if (item.validatedstatus === "Q") {
                              setCheckedStatus({ Q: true });
                            } else if (item.validatedstatus === "N") {
                              setCheckedStatus({ N: true });
                            }
                            setNIK(item.ID_Number);
                            //var contingent_name = contingents.filter(x=>{return x._id === rows[id].contingent_id})
                            setcontingent_name_popup(item.contingent_name);
                            setOpen(true);
                            setaction("edit");
                            console.log(
                              "link",
                              url + "/private/dukcapils?KTP=" + item.ID_Number,
                            );
                            setloaddukcapil(false);
                            setdukcapil({});
                            Axios.get(
                              url + "/private/dukcapils?KTP=" + item.ID_Number,
                            ).then((res) => {
                              if (res.data.length > 0) {
                                setdukcapil(res.data[0]);
                              }
                            });
                          }}
                        >
                          Sync
                        </Button>
                        <Button
                          size="small"
                          variant="contained"
                          color="primary"
                          style={{marginLeft:5}}
                          onClick={() => {
                            setopenModalDukcapil(true);
                            setcheckparticipants(item)
                          }}
                        >
                          Console
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </Paper>
        </Grid>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          open={openAlert}
          autoHideDuration={3000}
          onClose={handleClose}
        >
          <Alert onClose={handleClose} severity={errorType}>
            {" "}
            {hasError}{" "}
          </Alert>
        </Snackbar>
      </Grid>
    </>
  );
}

import React, { useState,useEffect } from "react";
import {
  Grid, 
  Paper,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  Typography,
  TextField,
  InputLabel,
  Avatar,
  Button,
  RadioGroup ,
  Radio ,FormControlLabel
} from "@material-ui/core";
// styles
import useStyles from "./styles";
import PageTitle from "../Components/PageTitle/PageTitle";
import Dialog from "../Components/Dialog/Dialog";
import NoImg from "../../Assets/Images/no-image.png";
import { useUserDispatch, signExpired, signOut, useUserState } from "../../context/UserContext";
import Api from "../../Services/Api"
import StaticVar from "../../Config/StaticVar"

export default function Contingents(props) {
  // global
  var userDispatch = useUserDispatch();  
  var classes = useStyles();

  var getData = async() => {
    try{

      let res = await Api.getContingents() //axios.get(url+"/private/contingents",{headers});
      let response = await res.data;
      console.log('response', JSON.stringify(response))

      if(response.hasOwnProperty("status")){
        console.log('error')
        signExpired(userDispatch,props.history);
      }else
      {
        setData(response)
      }
    }
    catch(error){
      signExpired(userDispatch,props.history);
    }
  }

  useEffect(() => {
    getData();
    return () => {
      getData();
    }
  }, []);
  
  const [data, setData] = useState([])
  const [contingentid, setcontingentid] = useState("")
  var [form_name, setform_name] = useState("");
  var [form_initial, setform_initial] = useState("");
  const [open, setOpen] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [action, setaction] = useState("")
  var [imgPreview, setImgPreview ] = useState(NoImg);
  var [img, setImg ] = useState("");
  var [isImgValid, setIsImgValid ] = useState("");
  var [imgErrorMsg, setImgErrorMsg ] = useState("");
  var [isHome, setIsHome ] = useState(false);

  let sendData = new FormData();
  sendData.append('name', form_name);
  sendData.append('initial', form_initial);
  sendData.append('logo', img);
  sendData.append('isHome', isHome);
  
  function handleImage(e){
    let reader = new FileReader();
    let file = e.target.files[0],
      pattern = /image-*/;

    if (!file.type.match(pattern)) {
      setIsImgValid(true)
      setImgErrorMsg("Format File tidak sesuai")
      return;
    }

    reader.onloadend = () => {
      setIsImgValid(false);
      setImgErrorMsg("");
      setImg(file);
      setImgPreview(reader.result)
    };

    reader.readAsDataURL(file);
  }

  const handleClickOpen = () => {
    setaction("Tambah");
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenModalDelete(false);
  };

  function postData(){
    if(action === "Tambah"){
      Api.postContingents(sendData).then(res=>{
        getData();
        setOpen(false);
      })
    }

    if(action === "Ubah"){
      console.log('dta:'+sendData)
      Api.putContingents(contingentid, sendData).then(res=>{
        getData();
        setOpen(false);
      })
    }
    
  }

  function handleClickOpenEdit(id) {
    setcontingentid(id);
    setaction("Ubah");
    Api.putContingents(contingentid, sendData).then(res=>{
      getData();
      setOpen(false);
    })
    Api.getContingentById(id).then(res=>{
      console.log("data", JSON.stringify(res.data) );
      setform_name(res.data[0].name)
      setform_initial(res.data[0].initial)
      setIsHome(res.data[0].ishome)
      setOpen(true);
      setImgPreview(StaticVar.ImageUrl+"contingent/"+res.data[0].logo);
    })
  };

  function handleClickOpenDelete(id,name) {
    setcontingentid(id);
    setform_name(name);
    setOpenModalDelete(true);
    
  };

  function deleteData(id) {
    Api.deleteContingents(id).then(res=>{
      setOpenModalDelete(false);
      getData();
    })
  }
  const handleChange = event => {
    setIsHome(event.target.value);
  };


  return (
    <>
      <PageTitle title="Kontingen" button="Tambah Kontingen" click={handleClickOpen} />
      <Dialog
        open={open}
        close={handleClose}
        title={"Form "+ action + " Kontingen"}
        content={
          <Grid container spacing={1}>
            <Grid item sm={12}>
              <TextField
                label="Nama"
                className={classes.formContainer}
                margin="normal"
                value={form_name}
                onChange={(event)=> setform_name(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <TextField
                label="Inisial"
                className={classes.formContainer}
                margin="normal"
                value={form_initial}
                onChange={(event)=> setform_initial(event.target.value)}
              />
            </Grid>
            <Grid item sm={12}>
              <InputLabel htmlFor="contributor-helper">Foto</InputLabel>
              <Avatar src={imgPreview}/>
              <input type="file" 
              accept="image/*"
              onChange={e =>handleImage(e)}/>
            </Grid>
            <Grid item sm={12}>
              <InputLabel htmlFor="contributor-helper">Is Home</InputLabel>
              <RadioGroup aria-label="position" name="position" value={String(isHome)} onChange={handleChange} row>
        <FormControlLabel
          value="true"
          control={<Radio color="primary" />}
          label="Ya"
          labelPlacement="right"
        />
        <FormControlLabel
          value="false"
          control={<Radio color="primary" />}
          label="Tidak"
          labelPlacement="right"
        /> 
      </RadioGroup>
            </Grid>
          </Grid>
        }

        cancel={handleClose}
        valueCancel ={"Batal"}
        confirm={()=>postData()}
        valueConfirm={"Simpan"}
      />

       {/* modal delete */}
       <Dialog
        open={openModalDelete}
        close={handleClose}
        title={"Apakah anda yakin menghapus ??"}
        content={
          <Typography>{setform_name}</Typography>
        }
        cancel={handleClose}
        valueCancel ={"Tidak"}
        confirm={()=>deleteData(contingentid)}
        valueConfirm={"Ya, Hapus"}
      />

      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Paper>
            <Table>
              <TableHead>
              <TableRow>
                <TableCell style={{width:30, textAlign:'center'}}>No</TableCell>
                <TableCell style={{width:100}}>Logo</TableCell>
                <TableCell>Nama Kontingen</TableCell>
                <TableCell>Tuan Rumah</TableCell>
                 <TableCell style={{width:250, textAlign:'center'}}>Aksi
                </TableCell>
              </TableRow>
              </TableHead>
              <TableBody>
                {
                  data.map((item,index)=>{
                    return(                      
                      <TableRow>
                        <TableCell>{(index+1)}</TableCell>
                        <TableCell>
                          <Avatar src={StaticVar.ImageUrl+"contingent/"+item.logo}/>
                        </TableCell>
                        <TableCell>{item.name} <br/> <small>{item.initial}</small></TableCell>
                        <TableCell>{item.ishome}</TableCell>
                        <TableCell style={{textAlign:'center'}}>
                          <Button className={classes.btnAction} onClick={() => handleClickOpenEdit(item._id)} ><Typography className={classes.txtAction}>Ubah</Typography></Button>
                          <Button className={classes.btnAction} onClick={() => handleClickOpenDelete(item._id,item.name)} ><Typography className={classes.txtAction}>Hapus</Typography></Button>
                        </TableCell>
                      </TableRow>
                    )
                  })
                }
              </TableBody>
            </Table>
          
          </Paper>
        </Grid>
      </Grid>
    </>
  );
}

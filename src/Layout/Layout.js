import React from "react";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import classnames from "classnames";

// styles
import useStyles from "./styles";

// components
import Header from "../Components/Header";
import Sidebar from "../Components/Sidebar";

// pages
import Dashboard from "../Pages/dashboard";
import Maps from "../Pages/maps";
import Charts from "../Pages/charts/Charts";
import Users from "../Pages/users/Users";
import Contingents from "../Pages/contingents/Contingents";
import Disciplines from "../Pages/disciplines/Disciplines";
import ViewDisciplines from "../Pages/disciplines/ViewDisciplines";
import Venues from "../Pages/venues/Venues";
import Pages from "../Pages/Pages/Pages";
import EditorPages from "../Pages/Pages/EditorPages";

// context
import { useLayoutState } from "../Context/LayoutContext";
import Accreditation from "../Pages/users/Accreditation";
import Helpdesk from "../pages/helpdesk/Helpdesk";

function Layout(props) {
  var classes = useStyles();

  // global
  var layoutState = useLayoutState();

  return (
    <div className={classes.root}>
      <>
        <Header history={props.history} />
        <Sidebar />

        <div
          className={classnames(classes.content, {
            [classes.contentShift]: layoutState.isSidebarOpened,
          })}
        >
          <div className={classes.fakeToolbar} />
          <Switch>
            <Route path="/app/dashboard" component={Dashboard} />
            <Route path="/app/users" component={Users} />
            <Route path="/app/contingents" component={Contingents} />
            <Route path="/app/disciplines" component={Disciplines} />
            <Route
              path="/app/viewdisciplines/:id"
              component={ViewDisciplines}
            />
            <Route path="/app/hkvision" component={Hkvision} />
            <Route path="/app/venues" component={Venues} />
            <Route path="/app/accreditation" component={Accreditation} />
            <Route path="/app/pages/list-pages" component={Pages} />
            <Route
              path="/app/pages/editor-pages/:id/:action"
              component={EditorPages}
            />
            <Route
              exact
              path="/app/ui"
              render={() => <Redirect to="/app/ui/icons" />}
            />
            <Route path="/app/ui/maps" component={Maps} />
            <Route path="/app/ui/charts" component={Charts} />
            <Route path="/app/helpdesk/tahap1" component={Helpdesk} />
          </Switch>
        </div>
      </>
    </div>
  );
}

export default withRouter(Layout);
